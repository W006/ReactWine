### 描述

   购酒网React版本

### 开发环境

- nodejs 8+

- 浏览器 最后2个版本 

### 工程搭建

- 下载代码 https://gitee.com/W006/ReactWine.git

```shell
  git clone https://gitee.com/W006/ReactWine.git
```

- 下载依赖
  
```shell
    cd ReactWine
```

```shell
 npm install  // 慢的话用 cnpm install (这个要单独安装cnpm)
```

or mac

```shell
yarn install
```

### 启动

```shell
npm run dev
```

or mac

```shell
yarn run dev
```

### 访问

[http://localhost:6061](http://localhost:6061/)

### 工程学习

#### 前置知识

- nodejs  
    [https://npm.taobao.org/mirrors/node/](https://npm.taobao.org/mirrors/node/)
  
- es6+

- React
  - React-route
  - React-dom
- antd
- webpack

#### 关键点

- Es6

  - let、const
  - promise

  - Array
    - foreach
    - some
    - every
    - filter
  - Object
    - keys 
    - values
    - . . .

