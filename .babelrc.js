const path = require('path');
const webpack = require('webpack');

module.exports = {
    "presets": [
        // "@babel/react",
        [
            "@babel/preset-env",
            {
                "modules": false,
                "targets": {
                    "browsers": [
                    "> 1%",
                    "last 2 versions",
                    "not ie <= 8"
                    ]
                }
            },
        ]
    ],
  plugins: [
    // new webpack.DefinePlugin({
    //   'process.env.NODE_ENV': JSON.stringify('production'),
    // }),
    // [
    //   'module-resolver',
    //   {
    //     alias: {
    //       components: path.join(__dirname, './src/components'),
    //     },
    //   },
    // ],
    // [
    //   'import',
    //   {
    //     libraryName: 'antd',
    //     style: true, // or 'css'
    //   },
    // ],
        // ["import", { libraryName: "antd", "libraryDirectory": "es", style: 'css' }],
        ["@babel/plugin-proposal-decorators",{ "legacy": true }],
        "transform-react-jsx",
        "@babel/plugin-transform-runtime",
        "@babel/plugin-syntax-dynamic-import",
        '@babel/plugin-proposal-class-properties',
  ],
};

// {
//     "presets": [
//         [
//             "@babel/preset-env",
//             {
//                 "modules": false,
//                 "targets": {
//                     "browsers": [
//                     "> 1%",
//                     "last 2 versions",
//                     "not ie <= 8"
//                     ]
//                 }
//             },
//         ]
//     ],
//      "plugins": [
//         ["import", { libraryName: "antd", style: "css" }],
//         ["@babel/plugin-proposal-decorators",{ "legacy": true }],
//         "transform-react-jsx",
//         "@babel/plugin-transform-runtime",
//         "@babel/plugin-syntax-dynamic-import",
//         '@babel/plugin-proposal-class-properties',
//     ]
// }

// {
//     "presets": [
//         // "es2015",
//         "react",
//         "babel-preset-stage-3",
//         [
//             "env",
//             {
//                 "loose": true,  
//                 "modules": false 
//             }
//         ],
//     ],
//     "plugins": [
//         "transform-decorators-legacy",
//     ]
// }
