/* eslint-disable no-console */
const path = require('path');
// export function demo(){
//     console.log('Demo')
// }
// export default  {
//     [`GET /api/login`]: (req, res) => {
//         console.log('---->', req.params);
//         return res.json({
//             id: 1,
//             username: 'kenny',
//             sex: 6
//         });
//     }
// }

// import { getOne } from '../utils/mock.dao';

const { getTableData } = require(path.join(__dirname, '../utils/mock.dao'));
const { writeOk, writeJson, ModuleReturn, writeFail } = require(path.join(__dirname, '../utils/mock.util'));

// console.log('Utils', Utils)
const CUR_SESSION = {};

module.exports = {
  'GET /api/getU':(req, res) => {
    const { host } = req.headers;
    if (CUR_SESSION[host]) {
      return writeOk(res, CUR_SESSION[host]);
    }
    return writeJson(res, { ...ModuleReturn, code: '500', message: 'Session过期' });
  },
  'POST /api/logout': (req, res) => {
    const { host } = req.headers;
    if (CUR_SESSION[host]) {
      delete CUR_SESSION[host];
    }
    writeOk(res, {});
  },
  'POST /api/login': (req, res) => {
    // console.log('---->', req);
    const { host } = req.headers;
    const { params, query, body } = req;
    const { userAccount, password } = { ...params, ...query, ...body };

    if (!userAccount || !password) {
      return writeJson(res, { ...ModuleReturn, code: '500', message: '账号或者密码不能为空' });
    }
    let cUser;
    getTableData('SM_USER').some((u, i) => {
      if (u.userAccount == userAccount) {
        cUser = u;
        CUR_SESSION[host] = cUser;
        return true;
      }
      return false;
    });
    if (cUser) {
      if (cUser.password == password) {
        return writeOk(res, cUser);
      } else {
        return writeJson(res, { ...ModuleReturn, code: '500', message: '密码错误' });
      }
    }
    return writeJson(res, { ...ModuleReturn, code: '500', message: '用户不存在' });
  },
};
