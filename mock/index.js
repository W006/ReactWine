/* eslint-disable global-require */
/* eslint-disable import/no-dynamic-require */
// const fs = require('fs');

const { api } = require('./utils/mock.util');

const mock = {...api};

require('fs').readdirSync(require('path').join(`${__dirname}/module/`)).forEach((file) => {
  // console.log('file.includes(".js")', file.includes(".js"))
  try {
    if (file.includes('.js')) {
      Object.assign(mock, require(`${__dirname}/module/${file}`));
    }
  } catch (error) {
    console.log('error', error);
  }

  // console.log('require(`./${file}`)', require(`./${file}`))
  // Object.assign(mock, require(`./${file}`));
});

module.exports = mock;


// const Result = {
//   code: '200',
//   success: true,
//   message: '',
//   result: {},
// };

// function jFile(filename) {
//   return (req, res) => {
//     const data = fs.readFileSync(`mock/data/${filename}.json`).toString();
//     const json = JSON.parse(data);
//     return res.json(json);
//   };
// }

// module.exports = {
//   'GET /api/user/hello': 1,
//   'GET /api/user/login': jFile('user'),
//   'GET /api/user/getUser': (req, res) => {
//     const { query, body } = req;
//     return res.json({
//       id: 1,
//       username: 'kenny',
//       sex: 6,
//       bir: +new Date(),
//     });
//   },
// };
