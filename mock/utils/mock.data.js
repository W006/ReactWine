/**
 * @desc 当有些数据需要自定义无法模拟生成的时候用这个办法
 */

// 用于登陆的账号
const user = {
  id: 1,
  userAccount: 'admin',
  userName: 'admin',
  password: '1',
  userType: '0',
  updPwdTime: 1520352000000,
  invalidPwdTime: 1837785600000,
  countryId: 10,
  districtId: 10,
  province: '山东省',
  city: '青岛市',
  block: '市南区',
  address: '山东,青岛,市南区',
};


module.exports = {
  user,
};
