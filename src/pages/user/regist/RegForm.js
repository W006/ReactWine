import React from 'react';
import PropTypes from 'prop-types';
import NForm, { NInput } from 'components/NForm';
import './regist.scss';

const TYPE_ENUM = {
  PHONE: 'phone',
  EMAIL: 'email',
};

export default class RegForm extends React.Component {
  formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  };

  tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  };

  static defaultProps = {
    type: TYPE_ENUM.PHONE,
  };

  static propType = {
    type: PropTypes.oneOf(Object.values(TYPE_ENUM)),
  };

  render() {
    const { form, type } = this.props;
    return (
      <NForm action="" className="reg-form" form={form} {...this.formItemLayout}>
        {type == TYPE_ENUM.PHONE ? (
          <NInput id="phone" label="手机号" />
        ) : (
          <NInput id="email" label="邮箱" />
        )}
        <NInput.NPassword id="password" label="设置密码" />
        <NInput.NPassword id="repassword" label="确认密码" />
        <NInput id="yzm" label="手机号" />
        <NInput id="valiCode" label="校验码" />
      </NForm>
    );
  }
}
