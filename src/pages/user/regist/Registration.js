import React from 'react';
import { Image, LinkA } from 'components';
import { Form, Tabs, Button } from 'antd';
import UserLayout from '../UserLayout';
import RegForm from './RegForm';
import './regist.scss';

@Form.create()
class Regisration extends React.Component {
  formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  };

  tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  };

  render() {
    const { form } = this.props;
    return (
      <UserLayout>
        <div className="reg-main">
          <div className="reg-main-box">
            <div className="reg-left left center">
              <Tabs defaultActiveKey="A">
                <Tabs.TabPane tab="手机注册" key="A">
                  <RegForm form={form} type="phone" />
                </Tabs.TabPane>
                <Tabs.TabPane tab="邮箱注册" key="B">
                  <RegForm form={form} type="email" />
                </Tabs.TabPane>
              </Tabs>
              <p className="xieyi">
                <b>点击注册，表示您同意购酒网</b>
                <LinkA id="a1">《服务协议》</LinkA>
              </p>
              <Button type="primary" size="default" className="reg-button">
                {' '}
                注&nbsp;册{' '}
              </Button>
            </div>
            <div className="reg-right right center">
              <LinkA>
                <Image src="images/407.jpg" alt="" />
              </LinkA>
            </div>
          </div>
        </div>
      </UserLayout>
    );
  }
}
export default Regisration;
