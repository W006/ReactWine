import React, { PureComponent, Fragment } from 'react';
import { Image, LinkA } from 'components';
import { Link } from 'react-router-dom';
import './layout.scss';

class UserLayout extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    const { children, login = false } = this.props;
    return (
      <div className="user-layout">
        {/* <!-- header --> */}
        <div className="header">
          <div className="mid">
            <Link to="/home/a" className="logo">
              <Image src="images/399.jpg" alt="购酒网" />
            </Link>
            <div className="login_line">
              {login ? (
                <span className="left">您好，欢迎光临购酒网</span>
              ) : (
                <Fragment>
                  <span className="left">如您已有账户&nbsp;</span>
                  <Link to="/user/login" className="strong-color">
                    请登录
                  </Link>
                </Fragment>
              )}
              <LinkA className="strong-color right">&nbsp; 帮助</LinkA>
              <LinkA className="online-service right">&nbsp;在线客服</LinkA>
            </div>
          </div>
        </div>
        {/* <!-- content 主要内容 --> */}
        <div className="center">{children}</div>
        {/* <!-- footer --> */}
        <div className="footer">
          <div>
            <LinkA title="关于我们">关于我们</LinkA>&nbsp;|&nbsp;<LinkA>联系我们</LinkA>
            &nbsp;|&nbsp;<LinkA title="人才招聘">人才招聘</LinkA>&nbsp;|&nbsp;
            <LinkA title="隐私声明">隐私声明</LinkA>&nbsp;|&nbsp;
            <LinkA title="帮助中心">帮助中心</LinkA>&nbsp;|&nbsp;
            <LinkA title="友情链接">友情链接</LinkA>
          </div>
          <div>
            Copyright©2009-2013 gjw.com, All Rights Reserved 上海购酒网电子商务有限公司 版权所有
          </div>
        </div>
      </div>
    );
  }
}

UserLayout.propTypes = {};

export default UserLayout;
