import React, { PureComponent } from 'react';
import { Image, LinkA } from 'components';
import { Link } from 'react-router-dom';
import { Form, Button, Icon } from 'antd';
import NForm, { NInput, NCheckbox } from 'components/NForm';
import UserLayout from '../UserLayout';
import './login.scss';

@Form.create()
class Login extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  doLogin = () => {
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        alert(JSON.stringify(values));
      }
    });
  };

  render() {
    return (
      <UserLayout login>
        {/* <!-- center --> */}
        <div className="login-main">
          <div className="left">
            <LinkA href="special.html" target="_blank">
              <Image src="images/400.jpg" alt="" />
            </LinkA>
          </div>
          <div className="right login-form">
            <Form>
              <NForm form={this.props.form}>
                <div className="form_top">
                  <LinkA>
                    <Image src="images/401.jpg" alt="" />
                  </LinkA>
                  <b>购酒网会员登录</b>
                </div>
                <div>
                  <NInput
                    id="username"
                    options={{
                      className: 'login-item-input',
                      placeholder: '账号',
                      prefix: <Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />,
                    }}
                  />
                  <NInput.NPassword
                    id="password"
                    options={{
                      className: 'login-item-input',
                      placeholder: '密码',
                      prefix: <Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />,
                    }}
                  />
                  <Button type="primary" onClick={this.doLogin} className="login-form-button">
                    登陆
                  </Button>
                </div>
                <div className="login-btn-tool">
                  <div className="auto-login left">
                    {/* <Checkbox /> 自动登录 */}
                    <NCheckbox id="rememberMe">自动登录</NCheckbox>
                    {/* <input type="checkbox" />自动登录 */}
                  </div>
                  <div className="right ant-form-item-control">
                    <LinkA>忘记密码</LinkA>
                  </div>
                </div>
              </NForm>
            </Form>
            <div className="login-footer">
              <p>使用合作账号登录</p>
              <LinkA>
                <Image src="images/402.jpg" alt="" />
              </LinkA>
              <LinkA>
                <Image src="images/403.jpg" alt="" />
              </LinkA>
              <LinkA>
                <Image src="images/404.jpg" alt="" />
              </LinkA>
              <LinkA>
                <Image src="images/405.jpg" alt="" />
              </LinkA>
              <LinkA>
                <Image src="images/406.jpg" alt="" />
              </LinkA>
            </div>
            <div className="login-reg">
              <Link to="/user/registr">注册新帐号</Link>
            </div>
          </div>
        </div>
      </UserLayout>
    );
  }
}

Login.propTypes = {};

export default Login;
