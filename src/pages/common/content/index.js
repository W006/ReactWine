import React, { PureComponent } from 'react';
import { LinkA, Bread, BrandCard } from 'components';

class Content extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    return (
      <div className="list_fox">
        <Bread />
        <div className="list_main_sec">
          <div className="div1">
            <div className="div1_left">
              <span>品牌</span>
              <br />
              <input type="text" placeholder="输入品牌内容" />
            </div>
            <div className="div1_right">
              <div className="div1_right_top">
                <span>
                  <LinkA>所有品牌</LinkA>
                </span>
                <span>
                  <LinkA>ABCD</LinkA>
                </span>
                <span>
                  <LinkA>EFG</LinkA>
                </span>
                <span>
                  <LinkA>HIJK</LinkA>
                </span>
                <span>
                  <LinkA>LMN</LinkA>
                </span>
                <span>
                  <LinkA>OPQ</LinkA>
                </span>
                <span>
                  <LinkA>RST</LinkA>
                </span>
                <span>
                  <LinkA>UVW</LinkA>
                </span>
                <span className="span1">
                  <LinkA>XYZ</LinkA>
                </span>
                <span style={{ marginRight: '0' }}>
                  <LinkA>更多</LinkA>
                </span>
              </div>
              <div className="div1_right_bottom">
                <ul id="brand_list">
                  <li className="brand_lmn">
                    <LinkA id="putaojiu-lafeixilie" rel="nofollow">
                      拉菲系列
                    </LinkA>
                  </li>

                  <li className="brand_abcd">
                    <LinkA id="putaojiu-benfu" rel="nofollow">
                      奔富
                    </LinkA>
                  </li>

                  <li className="brand_hijk">
                    <LinkA id="putaojiu-huangweidaishu" rel="nofollow">
                      黄尾袋鼠
                    </LinkA>
                  </li>

                  <li className="brand_efg">
                    <LinkA id="putaojiu-fabina" rel="nofollow">
                      法比纳
                    </LinkA>
                  </li>

                  <li className="brand_hijk">
                    <LinkA id="putaojiu-hongmogui" rel="nofollow">
                      红魔鬼
                    </LinkA>
                  </li>

                  <li className="brand_xyz">
                    <LinkA id="putaojiu-zhangyu" rel="nofollow">
                      张裕
                    </LinkA>
                  </li>

                  <li className="brand_abcd">
                    <LinkA id="putaojiu-changcheng" rel="nofollow">
                      长城
                    </LinkA>
                  </li>

                  <li className="brand_hijk">
                    <LinkA id="putaojiu-jiazhouleshi" rel="nofollow">
                      加州乐事
                    </LinkA>
                  </li>

                  <li className="brand_hijk">
                    <LinkA
                      href="javascript:void(0);javascript:void(0);"
                      id="putaojiu-huangxuan"
                      rel="nofollow"
                    >
                      皇轩
                    </LinkA>
                  </li>

                  <li className="brand_hijk">
                    <LinkA id="putaojiu-jiachengzhuangyuan" rel="nofollow">
                      嘉诚庄园
                    </LinkA>
                  </li>

                  <li className="brand_hijk">
                    <LinkA id="putaojiu-kasite" rel="nofollow">
                      卡斯特
                    </LinkA>
                  </li>

                  <li className="brand_efg">
                    <LinkA id="putaojiu-faguoluyilafei" rel="nofollow">
                      法国路易拉菲
                    </LinkA>
                  </li>

                  <li className="brand_abcd">
                    <LinkA id="putaojiu-aizhiwan" rel="nofollow">
                      爱之湾
                    </LinkA>
                  </li>

                  <li className="brand_uvw">
                    <LinkA id="putaojiu-wangchao" rel="nofollow">
                      王朝
                    </LinkA>
                  </li>

                  <li className="brand_abcd">
                    <LinkA id="putaojiu-beileisi" rel="nofollow">
                      贝雷斯
                    </LinkA>
                  </li>

                  <li className="brand_lmn">
                    <LinkA id="putaojiu-niya" rel="nofollow">
                      尼雅
                    </LinkA>
                  </li>

                  <li className="brand_xyz">
                    <LinkA id="putaojiu-xianggelila" rel="nofollow">
                      香格里拉
                    </LinkA>
                  </li>

                  <li className="brand_rst">
                    <LinkA id="putaojiu-shenshu" rel="nofollow">
                      神树
                    </LinkA>
                  </li>

                  <li className="brand_abcd">
                    <LinkA id="putaojiu-chenzuozhixing" rel="nofollow">
                      晨曦之星
                    </LinkA>
                  </li>

                  <li className="brand_lmn">
                    <LinkA id="putaojiu-luyisibojue" rel="nofollow">
                      路易斯伯爵
                    </LinkA>
                  </li>

                  <li className="brand_rst">
                    <LinkA href="hjavascript:void(0);" id="putaojiu-ruigebao" rel="nofollow">
                      瑞格堡
                    </LinkA>
                  </li>

                  <li className="brand_hijk">
                    <LinkA id="putaojiu-hongshanxi" rel="nofollow">
                      红山溪
                    </LinkA>
                  </li>

                  <li className="brand_xyz">
                    <LinkA id="putaojiu-zhili ladingshenhua" rel="nofollow">
                      智利 拉丁神话
                    </LinkA>
                  </li>

                  <li className="brand_efg">
                    <LinkA id="putaojiu-feibo" rel="nofollow">
                      菲波
                    </LinkA>
                  </li>

                  <li className="brand_xyz">
                    <LinkA id="putaojiu-xibanyatangluka" rel="nofollow">
                      西班牙唐卢卡
                    </LinkA>
                  </li>

                  <li className="brand_abcd">
                    <LinkA id="putaojiu-alongsuo" rel="nofollow">
                      阿隆索
                    </LinkA>
                  </li>

                  <li className="brand_lmn">
                    <LinkA id="putaojiu-malidimai" rel="nofollow">
                      玛莉迪麦
                    </LinkA>
                  </li>

                  <li className="brand_hijk">
                    <LinkA id="putaojiu-jialipasuo" rel="nofollow">
                      加利帕索
                    </LinkA>
                  </li>

                  <li className="brand_xyz">
                    <LinkA id="putaojiu-yidalimeiguixiaowu" rel="nofollow">
                      意大利玫瑰小屋
                    </LinkA>
                  </li>

                  <li className="brand_efg">
                    <LinkA id="putaojiu-faguomutongxiajiachengbao" rel="nofollow">
                      法国木桐夏嘉城堡
                    </LinkA>
                  </li>

                  <li className="brand_xyz">
                    <LinkA id="putaojiu-yidalichunzhiwuyu" rel="nofollow">
                      意大利春之物语
                    </LinkA>
                  </li>

                  <li className="brand_hijk">
                    <LinkA id="putaojiu-jiekasi" rel="nofollow">
                      杰卡斯
                    </LinkA>
                  </li>

                  <li className="brand_xyz">
                    <LinkA id="putaojiu-zuoyue" rel="nofollow">
                      酩悦
                    </LinkA>
                  </li>

                  <li className="brand_lmn">
                    <LinkA id="putaojiu-lafeizhuangyuan" rel="nofollow">
                      拉菲庄园
                    </LinkA>
                  </li>

                  <li className="brand_abcd">
                    <LinkA id="putaojiu-ailabeila" rel="nofollow">
                      艾拉贝拉
                    </LinkA>
                  </li>

                  <li className="brand_hijk">
                    <LinkA id="putaojiu-huangzuxiongshi" rel="nofollow">
                      皇族雄狮
                    </LinkA>
                  </li>

                  <li className="brand_lmn">
                    <LinkA id="putaojiu-luoshidunlaifu" rel="nofollow">
                      洛诗敦莱福
                    </LinkA>
                  </li>

                  <li className="brand_hijk">
                    <LinkA id="putaojiu-huolinghun" rel="nofollow">
                      活灵魂
                    </LinkA>
                  </li>

                  <li className="brand_hijk">
                    <LinkA id="putaojiu-kamosha" rel="nofollow">
                      卡莫莎
                    </LinkA>
                  </li>

                  <li className="brand_xyz">
                    <LinkA id="putaojiu-yunnanhong" rel="nofollow">
                      云南红
                    </LinkA>
                  </li>

                  <li className="brand_rst">
                    <LinkA id="putaojiu-shengmeidu" rel="nofollow">
                      圣美都
                    </LinkA>
                  </li>

                  <li className="brand_uvw">
                    <LinkA id="putaojiu-weilong" rel="nofollow">
                      威龙
                    </LinkA>
                  </li>

                  <li className="brand_lmn">
                    <LinkA id="putaojiu-lvwu" rel="nofollow">
                      绿雾
                    </LinkA>
                  </li>

                  <li className="brand_lmn">
                    <LinkA id="putaojiu-meiguishidai" rel="nofollow">
                      玫瑰时代
                    </LinkA>
                  </li>

                  <li className="brand_xyz">
                    <LinkA id="putaojiu-xiaoqie" rel="nofollow">
                      小企鹅
                    </LinkA>
                  </li>

                  <li className="brand_hijk">
                    <LinkA id="putaojiu-kaisile" rel="nofollow">
                      凯斯勒
                    </LinkA>
                  </li>

                  <li className="brand_hijk">
                    <LinkA id="putaojiu-hongmanzhuangyuan" rel="nofollow">
                      红蔓庄园
                    </LinkA>
                  </li>

                  <li className="brand_opq">
                    <LinkA id="putaojiu-pulimi" rel="nofollow">
                      普里米
                    </LinkA>
                  </li>

                  <li className="brand_efg">
                    <LinkA id="putaojiu-falaiyajiubao" rel="nofollow">
                      法莱雅酒堡
                    </LinkA>
                  </li>

                  <li className="brand_lmn">
                    <LinkA id="putaojiu-mutongzhuangyuan" rel="nofollow">
                      木桐庄园
                    </LinkA>
                  </li>

                  <li className="brand_hijk">
                    <LinkA id="putaojiu-hongshangu" rel="nofollow">
                      红山谷
                    </LinkA>
                  </li>

                  <li className="brand_hijk">
                    <LinkA id="putaojiu-jiqingfeiyang" rel="nofollow">
                      激情飞扬
                    </LinkA>
                  </li>

                  <li className="brand_lmn">
                    <LinkA id="putaojiu-mudangongjue" rel="nofollow">
                      牡丹公爵
                    </LinkA>
                  </li>

                  <li className="brand_uvw">
                    <LinkA id="putaojiu-weituchengbao" rel="nofollow">
                      维图城堡
                    </LinkA>
                  </li>

                  <li className="brand_hijk">
                    <LinkA id="putaojiu-kangdi" rel="nofollow">
                      康堤
                    </LinkA>
                  </li>

                  <li className="brand_hijk">
                    <LinkA id="putaojiu-kaerde" rel="nofollow">
                      卡尔德
                    </LinkA>
                  </li>

                  <li className="brand_xyz">
                    <LinkA id="putaojiu-xilai" rel="nofollow">
                      喜来
                    </LinkA>
                  </li>

                  <li className="brand_hijk">
                    <LinkA id="putaojiu-kasimoni" rel="nofollow">
                      卡斯莫尼
                    </LinkA>
                  </li>

                  <li className="brand_lmn">
                    <LinkA id="putaojiu-malinikangdi" rel="nofollow">
                      玛利妮康帝
                    </LinkA>
                  </li>

                  <li className="brand_abcd">
                    <LinkA id="putaojiu-danhe" rel="nofollow">
                      丹赫
                    </LinkA>
                  </li>

                  <li className="brand_rst">
                    <LinkA id="putaojiu-suoweina" rel="nofollow">
                      索维纳
                    </LinkA>
                  </li>

                  <li className="brand_lmn">
                    <LinkA id="putaojiu-mabiao" rel="nofollow">
                      马标
                    </LinkA>
                  </li>

                  <li className="brand_rst">
                    <LinkA id="putaojiu-simaite" rel="nofollow">
                      斯麦特
                    </LinkA>
                  </li>

                  <li className="brand_lmn">
                    <LinkA id="putaojiu-latuzhuangyuan" rel="nofollow">
                      拉图庄园
                    </LinkA>
                  </li>

                  <li className="brand_abcd">
                    <LinkA id="putaojiu-aobiangzhuangyuan" rel="nofollow">
                      奥比昂庄园
                    </LinkA>
                  </li>

                  <li className="brand_abcd">
                    <LinkA id="putaojiu-baimazhuangyuan" rel="nofollow">
                      白马庄园
                    </LinkA>
                  </li>

                  <li className="brand_lmn">
                    <LinkA id="putaojiu-magezhuangyuan" rel="nofollow">
                      玛歌庄园
                    </LinkA>
                  </li>

                  <li className="brand_abcd">
                    <LinkA id="putaojiu-baomazhuangyuan" rel="nofollow">
                      宝马庄园
                    </LinkA>
                  </li>

                  <li className="brand_rst">
                    <LinkA id="putaojiu-tianpushi" rel="nofollow">
                      添普士
                    </LinkA>
                  </li>

                  <li className="brand_xyz">
                    <LinkA id="putaojiu-xiaodixi" rel="nofollow">
                      小笛溪
                    </LinkA>
                  </li>

                  <li className="brand_hijk">
                    <LinkA id="putaojiu-jienishi" rel="nofollow">
                      杰尼诗
                    </LinkA>
                  </li>

                  <li className="brand_hijk">
                    <LinkA id="putaojiu-kelakejieke" rel="nofollow">
                      克拉克杰克
                    </LinkA>
                  </li>

                  <li className="brand_uvw">
                    <LinkA id="putaojiu-wanxuanshi" rel="nofollow">
                      万轩士
                    </LinkA>
                  </li>

                  <li className="brand_rst">
                    <LinkA id="putaojiu-suoluomen" rel="nofollow">
                      所罗门
                    </LinkA>
                  </li>

                  <li className="brand_xyz">
                    <LinkA id="putaojiu-xiongshizhuangyuan" rel="nofollow">
                      雄狮庄园
                    </LinkA>
                  </li>

                  <li className="brand_abcd">
                    <LinkA id="putaojiu-baitusizhuangyuan" rel="nofollow">
                      柏图斯庄园
                    </LinkA>
                  </li>

                  <li className="brand_xyz">
                    <LinkA id="putaojiu-yuxiangxilie" rel="nofollow">
                      驭香系列
                    </LinkA>
                  </li>

                  <li className="brand_rst">
                    <LinkA id="putaojiu-taitezhuangyuan" rel="nofollow">
                      泰特庄园
                    </LinkA>
                  </li>

                  <li className="brand_abcd">
                    <LinkA id="putaojiu-baideshige" rel="nofollow">
                      百德诗歌
                    </LinkA>
                  </li>

                  <li className="brand_abcd">
                    <LinkA id="putaojiu-bishangnvjue" rel="nofollow">
                      碧尚女爵
                    </LinkA>
                  </li>

                  <li className="brand_lmn">
                    <LinkA id="putaojiu-meiguizhuangyuan" rel="nofollow">
                      玫瑰庄园
                    </LinkA>
                  </li>

                  <li className="brand_xyz">
                    <LinkA id="putaojiu-zuocibozhuangyuan" rel="nofollow">
                      靓茨伯庄园
                    </LinkA>
                  </li>

                  <li className="brand_lmn">
                    <LinkA id="putaojiu-leiweibadun" rel="nofollow">
                      雷威巴顿
                    </LinkA>
                  </li>

                  <li className="brand_hijk">
                    <LinkA id="putaojiu-jinmeiguizhuangyuan" rel="nofollow">
                      金玫瑰庄园
                    </LinkA>
                  </li>

                  <li className="brand_abcd">
                    <LinkA id="putaojiu-duxiameilong" rel="nofollow">
                      都夏美隆
                    </LinkA>
                  </li>

                  <li className="brand_abcd">
                    <LinkA id="putaojiu-demihaye" rel="nofollow">
                      德米哈耶
                    </LinkA>
                  </li>

                  <li className="brand_lmn">
                    <LinkA id="putaojiu-longchuanzhuangyuan" rel="nofollow">
                      龙船庄园
                    </LinkA>
                  </li>

                  <li className="brand_opq">
                    <LinkA id="putaojiu-patusizhihua" rel="nofollow">
                      帕图斯之花
                    </LinkA>
                  </li>

                  <li className="brand_hijk">
                    <LinkA id="putaojiu-kanbidefubao" rel="nofollow">
                      坎彼德福堡
                    </LinkA>
                  </li>

                  <li className="brand_hijk">
                    <LinkA id="putaojiu-kangzuogubao" rel="nofollow">
                      康茗古堡
                    </LinkA>
                  </li>

                  <li className="brand_abcd">
                    <LinkA id="putaojiu-aoruika" rel="nofollow">
                      奥瑞卡
                    </LinkA>
                  </li>

                  <li className="brand_efg">
                    <LinkA id="putaojiu-gelaimeigubao" rel="nofollow">
                      格莱美古堡
                    </LinkA>
                  </li>

                  <li className="brand_lmn">
                    <LinkA id="putaojiu-mengpeiqigubao" rel="nofollow">
                      蒙佩奇古堡
                    </LinkA>
                  </li>

                  <li className="brand_xyz">
                    <LinkA id="putaojiu-xiangsongjiazu" rel="nofollow">
                      香颂家族
                    </LinkA>
                  </li>

                  <li className="brand_abcd">
                    <LinkA id="putaojiu-aerfadeqing" rel="nofollow">
                      阿尔法的情
                    </LinkA>
                  </li>

                  <li className="brand_hijk">
                    <LinkA id="putaojiu-hongxinhuanghou" rel="nofollow">
                      红心皇后
                    </LinkA>
                  </li>

                  <li className="brand_rst">
                    <LinkA id="putaojiu-ruidezhuangyuan" rel="nofollow">
                      瑞德庄园
                    </LinkA>
                  </li>

                  <li className="brand_xyz">
                    <LinkA id="putaojiu-zhanmushichuanchang" rel="nofollow">
                      詹姆士船长
                    </LinkA>
                  </li>

                  <li className="brand_xyz">
                    <LinkA id="putaojiu-xintian" rel="nofollow">
                      新天
                    </LinkA>
                  </li>

                  <li className="brand_lmn">
                    <LinkA id="putaojiu-lingzhi" rel="nofollow">
                      凌致
                    </LinkA>
                  </li>

                  <li className="brand_abcd">
                    <LinkA id="putaojiu-baoyi" rel="nofollow">
                      保伊
                    </LinkA>
                  </li>

                  <li className="brand_abcd">
                    <LinkA id="putaojiu-binlujiuzhuang">宾露酒庄</LinkA>
                  </li>

                  <li className="brand_abcd">
                    <LinkA id="putaojiu-aijisixixili" rel="nofollow">
                      艾吉斯西西里
                    </LinkA>
                  </li>

                  <li className="brand_hijk">
                    <LinkA id="putaojiu-huangjiaxiangshu" rel="nofollow">
                      皇家橡树
                    </LinkA>
                  </li>

                  <li className="brand_hijk">
                    <LinkA id="putaojiu-haluode" rel="nofollow">
                      哈罗德
                    </LinkA>
                  </li>

                  <li className="brand_uvw">
                    <LinkA id="putaojiu-walawala" rel="nofollow">
                      哇啦哇啦
                    </LinkA>
                  </li>

                  <li className="brand_hijk">
                    <LinkA id="putaojiu-huaerzi" rel="nofollow">
                      华尔兹
                    </LinkA>
                  </li>

                  <li className="brand_efg">
                    <LinkA id="putaojiu-feijue" rel="nofollow">
                      飞爵
                    </LinkA>
                  </li>

                  <li className="brand_hijk">
                    <LinkA id="putaojiu-huangjiahuayuan" rel="nofollow">
                      皇家花园
                    </LinkA>
                  </li>

                  <li className="brand_lmn">
                    <LinkA id="putaojiu-lanseyaoji" rel="nofollow">
                      蓝色妖姬
                    </LinkA>
                  </li>

                  <li className="brand_lmn">
                    <LinkA id="putaojiu-meishengshijia" rel="nofollow">
                      美圣世家
                    </LinkA>
                  </li>

                  <li className="brand_xyz">
                    <LinkA id="putaojiu-yunong" rel="nofollow">
                      欲浓
                    </LinkA>
                  </li>

                  <li className="brand_xyz">
                    <LinkA id="putaojiu-zuhezhuang" rel="nofollow">
                      组合装
                    </LinkA>
                  </li>

                  <li className="brand_abcd">
                    <LinkA id="putaojiu-babeila" rel="nofollow">
                      巴贝拉
                    </LinkA>
                  </li>

                  <li className="brand_xyz">
                    <LinkA id="putaojiu-zuofennianhua" rel="nofollow">
                      缤纷年华
                    </LinkA>
                  </li>

                  <li className="brand_lmn">
                    <LinkA id="putaojiu-lofic" rel="nofollow">
                      lofic
                    </LinkA>
                  </li>

                  <li className="brand_lmn">
                    <LinkA id="putaojiu-misaiao" rel="nofollow">
                      迷赛奥
                    </LinkA>
                  </li>

                  <li className="brand_rst">
                    <LinkA id="putaojiu-teniu" rel="nofollow">
                      特牛
                    </LinkA>
                  </li>

                  <li className="brand_rst">
                    <LinkA id="putaojiu-ruitaibojue" rel="nofollow">
                      瑞泰伯爵
                    </LinkA>
                  </li>

                  <li className="brand_lmn">
                    <LinkA id="putaojiu-lazuo" rel="nofollow">
                      拉斐
                    </LinkA>
                  </li>

                  <li className="brand_efg">
                    <LinkA id="putaojiu-feisinaite" rel="nofollow">
                      菲斯奈特
                    </LinkA>
                  </li>

                  <li className="brand_opq">
                    <LinkA id="putaojiu-pagenini" rel="nofollow">
                      帕格尼尼
                    </LinkA>
                  </li>

                  <li className="brand_abcd">
                    <LinkA id="putaojiu-anweika" rel="nofollow">
                      安伟卡
                    </LinkA>
                  </li>

                  <li className="brand_hijk">
                    <LinkA id="putaojiu-heita" rel="nofollow">
                      黑塔
                    </LinkA>
                  </li>

                  <li className="brand_abcd">
                    <LinkA id="putaojiu-dalasi" rel="nofollow">
                      达拉斯
                    </LinkA>
                  </li>

                  <li className="brand_uvw">
                    <LinkA id="putaojiu-wuge" rel="nofollow">
                      乌戈
                    </LinkA>
                  </li>

                  <li className="brand_abcd">
                    <LinkA id="putaojiu-dijiazhuangyuan" rel="nofollow">
                      迪嘉庄园
                    </LinkA>
                  </li>

                  <li className="brand_efg">
                    <LinkA id="putaojiu-faguolafeishangpin" rel="nofollow">
                      法国拉菲尚品
                    </LinkA>
                  </li>

                  <li className="brand_efg">
                    <LinkA id="putaojiu-faguogaopengjiuyuan" rel="nofollow">
                      法国高鹏酒园
                    </LinkA>
                  </li>

                  <li className="brand_efg">
                    <LinkA id="putaojiu-faguolafeite" rel="nofollow">
                      法国拉菲特
                    </LinkA>
                  </li>

                  <li className="brand_xyz">
                    <LinkA id="putaojiu-xibanyaliaozhuangyuan" rel="nofollow">
                      西班牙里奥庄园
                    </LinkA>
                  </li>

                  <li className="brand_xyz">
                    <LinkA id="putaojiu-xibanyamalidimai" rel="nofollow">
                      西班牙玛莉迪麦
                    </LinkA>
                  </li>

                  <li className="brand_xyz">
                    <LinkA id="putaojiu-xibanyatumeilong" rel="nofollow">
                      西班牙图美隆
                    </LinkA>
                  </li>

                  <li className="brand_rst">
                    <LinkA id="putaojiu-sweet encouten" rel="nofollow">
                      sweet encouten
                    </LinkA>
                  </li>

                  <li className="brand_lmn">
                    <LinkA id="putaojiu-masaite" rel="nofollow">
                      马塞特
                    </LinkA>
                  </li>

                  <li className="brand_xyz">
                    <LinkA id="putaojiu-zhen2" rel="nofollow">
                      真2
                    </LinkA>
                  </li>

                  <li className="brand_uvw">
                    <LinkA id="putaojiu-weila" rel="nofollow">
                      维拉
                    </LinkA>
                  </li>

                  <li className="brand_abcd">
                    <LinkA id="putaojiu-baiwanjinyang" rel="nofollow">
                      百万金羊
                    </LinkA>
                  </li>

                  <li className="brand_efg">
                    <LinkA id="putaojiu-feilanteli" rel="nofollow">
                      菲兰特利
                    </LinkA>
                  </li>

                  <li className="brand_abcd">
                    <LinkA id="putaojiu-changtuidaishu" rel="nofollow">
                      长腿袋鼠
                    </LinkA>
                  </li>

                  <li className="brand_lmn">
                    <LinkA
                      href="http://www.gjw.com/putaojiu-mingshibinggu/"
                      id="putaojiu-mingshibinggu"
                      rel="nofollow"
                    >
                      名仕冰谷
                    </LinkA>
                  </li>

                  <li className="brand_efg">
                    <LinkA id="putaojiu-fenfu" rel="nofollow">
                      纷赋
                    </LinkA>
                  </li>

                  <li className="brand_xyz">
                    <LinkA id="putaojiu-xiangnai" rel="nofollow">
                      香奈
                    </LinkA>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="div2">
            <div className="div2_top">
              <span>选购热点</span>
            </div>
            <div className="div2_bottom">
              <ul>
                <li>
                  <LinkA>整箱装</LinkA>
                </li>
                <li>
                  <LinkA>限量版</LinkA>
                </li>
                <li>
                  <LinkA>小产区</LinkA>
                </li>
                <li>
                  <LinkA>获奖</LinkA>
                </li>
                <li>
                  <LinkA>高评分</LinkA>
                </li>
                <li>
                  <LinkA>城堡</LinkA>
                </li>
              </ul>
            </div>
          </div>
          <div className="div2">
            <div className="div2_top">
              <span>价格</span>
            </div>
            <div className="div2_bottom">
              <ul>
                <li>
                  <LinkA>1-99</LinkA>
                </li>
                <li>
                  <LinkA>100-199</LinkA>
                </li>
                <li>
                  <LinkA>200-599</LinkA>
                </li>
                <li>
                  <LinkA>600-999</LinkA>
                </li>
                <li>
                  <LinkA>1000-1999</LinkA>
                </li>
                <li>
                  <LinkA>2000-5000</LinkA>
                </li>
                <li>
                  <LinkA>6000以上</LinkA>
                </li>
                <li>
                  <input type="text" />-<input type="text" />
                  <input type="button" value="确定" />
                </li>
              </ul>
            </div>
          </div>
          <div className="div2">
            <div className="div2_top">
              <span>产国</span>
            </div>
            <div className="div2_bottom">
              <ul>
                <li>
                  <LinkA>法国</LinkA>
                </li>
                <li>
                  <LinkA>澳大利亚</LinkA>
                </li>
                <li>
                  <LinkA>中国</LinkA>
                </li>
                <li>
                  <LinkA>智利</LinkA>
                </li>
                <li>
                  <LinkA>西班牙</LinkA>
                </li>
                <li>
                  <LinkA>南非</LinkA>
                </li>
                <li>
                  <LinkA>意大利</LinkA>
                </li>
                <li>
                  <LinkA>加拿大</LinkA>
                </li>
                <li>
                  <LinkA>美国</LinkA>
                </li>
                <li>
                  <LinkA>德国</LinkA>
                </li>
                <li>
                  <LinkA>葡萄牙</LinkA>
                </li>
              </ul>
            </div>
          </div>
          <div className="div2">
            <div className="div2_top">
              <span>品种</span>
            </div>
            <div className="div2_bottom">
              <ul>
                <li>
                  <LinkA>巴贝拉</LinkA>
                </li>
                <li>
                  <LinkA>波尔多</LinkA>
                </li>
                <li>
                  <LinkA>长相思</LinkA>
                </li>
                <li>
                  <LinkA>赤霞珠</LinkA>
                </li>
                <li>
                  <LinkA>解百纳</LinkA>
                </li>
                <li>
                  <LinkA>卡曼尼</LinkA>
                </li>
                <li>
                  <LinkA>梅洛</LinkA>
                </li>
                <li>
                  <LinkA>美露</LinkA>
                </li>
                <li>
                  <LinkA>莎当妮</LinkA>
                </li>
                <li>
                  <LinkA>苏维浓</LinkA>
                </li>
                <li>
                  <LinkA>西拉子</LinkA>
                </li>
              </ul>
            </div>
          </div>
          <div className="div2">
            <div className="div2_top">
              <span>商品规格</span>
            </div>
            <div className="div2_bottom">
              <ul>
                <li>
                  <LinkA>单支装</LinkA>
                </li>
                <li>
                  <LinkA>双支装</LinkA>
                </li>
                <li>
                  <LinkA>整箱装</LinkA>
                </li>
                <li>
                  <LinkA>组合装</LinkA>
                </li>
              </ul>
            </div>
          </div>
          <div className="div2">
            <div className="div2_top">
              <span>使用场合</span>
            </div>
            <div className="div2_bottom">
              <ul>
                <li>
                  <LinkA>家庭团聚</LinkA>
                </li>
                <li>
                  <LinkA>自饮独品</LinkA>
                </li>
                <li>
                  <LinkA>同事聚餐</LinkA>
                </li>
                <li>
                  <LinkA>三五好友</LinkA>
                </li>
                <li>
                  <LinkA>商务宴请</LinkA>
                </li>
                <li>
                  <LinkA>欢乐时刻</LinkA>
                </li>
              </ul>
            </div>
          </div>
          <div className="div3">
            <span>收起</span>
          </div>
        </div>
        <div className="img_top">
          <div className="img_top1">
            <LinkA style={{ width: '55px', background: '#c40000', color: '#fff' }}>销量</LinkA>
            <LinkA style={{ width: '50px' }}>价格</LinkA>
            <LinkA style={{ width: '62px' }}>评论数</LinkA>
            <LinkA style={{ width: '74px' }}>上架时间</LinkA>
            <LinkA style={{ width: '109px' }}>
              <b />
              仅显示有货
            </LinkA>
          </div>
          <div className="img_top2">
            <span>
              <strong style={{ color: '#c40000' }}>1</strong>/10
            </span>
            <LinkA />
            <LinkA />
          </div>
        </div>
        <div className="main_wrap">
          <BrandCard />
        </div>
      </div>
    );
  }
}

Content.propTypes = {};

export default Content;
