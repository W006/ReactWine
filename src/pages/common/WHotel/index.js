import React, { PureComponent, Fragment } from 'react';
import { Image, LinkA } from 'components';

class WHotel extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    return (
      <Fragment>
        {/* <!--一楼白酒馆s-->      */}
        <div className="floor1 gjw_tab floor" id="floor1">
          <div className="floor_t">
            <h2 className="fl">
              <i>1F</i>白酒馆
            </h2>
            <ul className="gjw_tabItemBox fr">
              <li className="gjw_tabItem sb active">
                <LinkA>大牌白酒</LinkA>
                <span />
              </li>
              <li className="gjw_tabItem sb">
                <LinkA>茅台系列</LinkA>
                <span />
              </li>
              <li className="gjw_tabItem sb">
                <LinkA>五粮液系列</LinkA>
                <span />
              </li>
              <li className="gjw_tabItem sb">
                <LinkA>精装礼盒</LinkA>
                <span />
              </li>
              <li className="gjw_tabItem sb">
                <LinkA>整箱优惠</LinkA>
                <span />
              </li>
              <li className="gjw_tabItem sb">
                <LinkA>小酒版</LinkA>
                <span />
              </li>
              <li className="gjw_tabItem sb">
                <LinkA>年份老酒</LinkA>
                <span />
              </li>
              <li className="gjw_tabItem">
                <LinkA>更多</LinkA>
              </li>
            </ul>
          </div>
          <div className="floor_container">
            <div className="gjw_tabSide fl">
              <LinkA className="gjw_tabSideBanner shine">
                <Image src="images/63.jpg" style={{ display: 'block' }} />
              </LinkA>
              <ul className="gjw_tabSideNav">
                <li>
                  <LinkA>五粮液</LinkA>
                </li>
                <li>
                  <LinkA>高端收藏</LinkA>
                </li>
                <li>
                  <LinkA>茅台专场</LinkA>
                </li>
                <li>
                  <LinkA>小酒迷情</LinkA>
                </li>
                <li>
                  <LinkA>陈年老酒</LinkA>
                </li>
                <li>
                  <LinkA>每周特卖</LinkA>
                </li>
                <li>
                  <LinkA>郎酒专场</LinkA>
                </li>
                <li>
                  <LinkA>清仓专区</LinkA>
                </li>
              </ul>
              <LinkA className="tab_sideAdLink shine">
                <Image src="images/64.jpg" style={{ display: 'inline' }} className="tab_sideAd" />
              </LinkA>
              <LinkA className="tab_sideAdLink2 shine">
                <Image src="images/65.jpg" style={{ display: 'inline' }} className="tab_sideAd" />
              </LinkA>
            </div>
            {/* <!--第一屏s--> */}
            <div className="gjw_tabBigBox fl">
              <div className="gjw_tabBox fl active qq" style={{ display: 'block' }}>
                <div className="gjw_slideBox fl">
                  <div
                    className="tempWrap"
                    style={{ overflow: 'hidden', ' position': 'relative', ' width': '440px' }}
                  >
                    <ul
                      style={{
                        width: ' 2200px',
                        position: ' relative',
                        overflow: ' hidden',
                        padding: '0px',
                        margin: '0px',
                        left: '0',
                      }}
                      className="gjw_slideBody"
                      id="gjw2"
                    >
                      <li style={{ float: 'left', width: '440px' }} className="clone">
                        <LinkA>
                          <Image
                            style={{ width: '440px', height: ' 237px' }}
                            src="images/66.jpg"
                            className="notloadlazy"
                            height="237px"
                            width="435px"
                          />
                        </LinkA>
                      </li>
                      <li style={{ float: 'left', width: '440px' }}>
                        <LinkA>
                          <Image
                            style={{ width: '440px', height: ' 237px' }}
                            src="images/67.jpg"
                            className="notloadlazy"
                            alt=""
                            height="237px"
                            width="435px"
                          />
                        </LinkA>
                      </li>
                      <li style={{ float: 'left', width: '440px' }}>
                        <LinkA>
                          <Image
                            style={{ width: '440px', height: ' 237px' }}
                            src="images/68.jpg"
                            className="notloadlazy"
                            alt=""
                            height="237px"
                            width="435px"
                          />
                        </LinkA>
                      </li>
                    </ul>
                  </div>
                  <div className="gjw_slideBtn">
                    <LinkA className="gjw_slidePrev" id="prevBtn2">
                      &lt","
                    </LinkA>
                    <LinkA className="gjw_slideNext" id="nextBtn2">
                      &gt;
                    </LinkA>
                  </div>
                  <div className="gjw_slideNav" id="gjwList2">
                    <LinkA className="active" />
                    <LinkA className="" />
                    <LinkA className="" />
                  </div>
                </div>
                <div className="fr gjw_adBox_xs">
                  <LinkA>
                    <Image src="images/71.jpg" style={{ display: 'inline' }} />
                  </LinkA>
                  <LinkA>
                    <Image src="images/72.jpg" style={{ display: 'inline' }} />
                  </LinkA>
                </div>
                <ul className="gjw_adBox_big clear">
                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/73.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>
                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/74.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>
                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/75.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>
                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/76.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>
                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/77.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>
                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/78.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>
                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/79.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>
                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/80.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>
                </ul>
              </div>
              {/* <!--第一屏e--> */}
              <div className="gjw_tabBox fl qq">
                <ul className="gjw_tabBox_proList">
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="38度 茅台 五星（2011年） 500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/81.jpg" />
                      </div>
                      <p>38度 茅台 五星（2011年） 500ml</p>
                      <div className="gjw_tabBox_price">￥520</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="53度 茅台 王子酒（酱门经典） 500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/82.jpg" title="53度 茅台 王子（酱门经典） 500ml" />
                      </div>
                      <p>53度 茅台 王子酒（酱门经典） 500ml</p>
                      <div className="gjw_tabBox_price">￥129</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="53度 水立方酒 500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/83.jpg" />
                      </div>
                      <p>53度 水立方酒 500ml</p>
                      <div className="gjw_tabBox_price">￥300</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="53度 茅台 飞天带杯（2016年产） 500ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/84.jpg" title="53度 茅台 飞天带杯 500ml" />
                      </div>
                      <p>53度 茅台 飞天带杯（2016年产） 500ml</p>
                      <div className="gjw_tabBox_price">￥926</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="53度 茅台 仁酒 500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/85.jpg" title="53度 茅台 仁酒 500ml" />
                      </div>
                      <p>53度 茅台 仁酒 500ml</p>
                      <div className="gjw_tabBox_price">￥119</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="51度  汉酱酒 500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/86.jpg"
                          title="51度 茅台 汉酱酒 500ml"
                          alt="51度 茅台 汉酱酒 500ml"
                        />
                      </div>
                      <p>51度 汉酱酒 500ml</p>
                      <div className="gjw_tabBox_price">￥239</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="53度 茅台 贵宾酒 （2010年产）500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/87.jpg" title="53度 茅台 贵宾酒 （2010年产）500ml" />
                      </div>
                      <p>53度 茅台 贵宾酒 （2010年产）500ml</p>
                      <div className="gjw_tabBox_price">￥2279</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="53度 茅台 迎宾酒嘉宾级 （2012年产）500ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/88.jpg" />
                      </div>
                      <p>53度 茅台 迎宾酒嘉宾级 （2012年产）500ml</p>
                      <div className="gjw_tabBox_price">￥98</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="53度 成龙酒 金典版 500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/89.jpg"
                          title="53度 成龙酒 金典版 500ml"
                          alt="53度 成龙酒 金典版 500ml"
                        />
                      </div>
                      <p>53度 成龙酒 金典版 500ml</p>
                      <div className="gjw_tabBox_price">￥178</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="53度 茅台 迎宾酒 500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/90.jpg" />
                      </div>
                      <p>53度 茅台 迎宾酒 500ml</p>
                      <div className="gjw_tabBox_price">￥59</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="53度 名将酒 2004年酱瓶 500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/91.jpg" />
                      </div>
                      <p>53度 名将酒 2004年酱瓶 500ml</p>
                      <div className="gjw_tabBox_price">￥1599</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="52度 茅台集团经典原浆V15 1000ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/92.jpg" title="52度 茅台集团经典原浆V15 1000ml" />
                      </div>
                      <p>52度 茅台集团经典原浆V15 1000ml</p>
                      <div className="gjw_tabBox_price">￥119</div>
                    </LinkA>
                  </li>
                </ul>
              </div>
              <div className="gjw_tabBox fl qq">
                <ul className="gjw_tabBox_proList">
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="52度 五粮液股份公司控股 如意结 珍品级 500ml*6"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/93.jpg" title="." alt="." />
                      </div>
                      <p>52度 五粮液股份公司控股 如意结 珍品级 500ml*6</p>
                      <div className="gjw_tabBox_price">￥399</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="52度 五粮液股份 千家福 2003年产 250ml*2"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/94.jpg" />
                      </div>
                      <p>52度 五粮液股份 千家福 2003年产 250ml*2</p>
                      <div className="gjw_tabBox_price">￥139</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="52度 五粮迎宾酒 精品 500ml*6">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/95.jpg" />
                      </div>
                      <p>52度 五粮迎宾酒 精品 500ml*6</p>
                      <div className="gjw_tabBox_price">￥348</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="52度 五粮液股份 六和液 盛典装 500ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/96.jpg" title="." alt="." />
                      </div>
                      <p>52度 五粮液股份 六和液 盛典装 500ml</p>
                      <div className="gjw_tabBox_price">￥158</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="52度 五粮液 1995专卖店酒（新） 500ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/97.jpg" />
                      </div>
                      <p>52度 五粮液 1995专卖店酒（新） 500ml</p>
                      <div className="gjw_tabBox_price">￥206</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="52度 五粮国宾酒 珍藏 500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/98.jpg" />
                      </div>
                      <p>52度 五粮国宾酒 珍藏 500ml</p>
                      <div className="gjw_tabBox_price">￥198</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="50度 五粮液股份 绵柔尖庄（红标）500ml*6"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/99.jpg" />
                      </div>
                      <p>50度 五粮液股份 绵柔尖庄（红标）500ml*6</p>
                      <div className="gjw_tabBox_price">￥188</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="52度 五粮迎宾酒 陈酿 500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/100.jpg" />
                      </div>
                      <p>52度 五粮迎宾酒 陈酿 500ml</p>
                      <div className="gjw_tabBox_price">￥198</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="56度 五粮液 老酒 500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/101.jpg" title="." alt="." />
                      </div>
                      <p>56度 五粮液 老酒 500ml</p>
                      <div className="gjw_tabBox_price">￥878</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="52度 五粮液 1公斤 1000ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/102.jpg" title="." alt="." />
                      </div>
                      <p>52度 五粮液 1公斤 1000ml</p>
                      <div className="gjw_tabBox_price">￥1150</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="52度 五粮液 人民大会堂 国宴酒 水晶装（2011年产） 500ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/103.jpg" title="." alt="." />
                      </div>
                      <p>52度 五粮液 人民大会堂 国宴酒 水晶装（2011年产） 500ml</p>
                      <div className="gjw_tabBox_price">￥599</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="52度 五粮液 贵宾级酒品 500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/104.jpg" title="." alt="." />
                      </div>
                      <p>52度 五粮液 贵宾级酒品 500ml</p>
                      <div className="gjw_tabBox_price">￥139</div>
                    </LinkA>
                  </li>
                </ul>
              </div>

              <div className="gjw_tabBox fl qq">
                <ul className="gjw_tabBox_proList">
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="53度 茅台 珍品王子酒 双瓶礼盒装 375ml*2瓶"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/105.jpg" title="." alt="." />
                      </div>
                      <p>53度 茅台 珍品王子酒 双瓶礼盒装 375ml*2瓶</p>
                      <div className="gjw_tabBox_price">￥299</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="53度 珍酒（珍壹号15年陈）500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/106.jpg"
                          title="53度 珍酒（珍壹号15年陈）500ml"
                          alt="53度 珍酒（珍壹号15年陈）500ml"
                        />
                      </div>
                      <p>53度 珍酒（珍壹号15年陈）500ml</p>
                      <div className="gjw_tabBox_price">￥888</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="52度 洋河蓝色经典 天之蓝 480ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/107.jpg" title="." alt="." />
                      </div>
                      <p>52度 洋河蓝色经典 天之蓝 480ml</p>
                      <div className="gjw_tabBox_price">￥299</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="52度 文君酒 500ml*2瓶">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/108.jpg" title="." alt="." />
                      </div>
                      <p>52度 文君酒 500ml*2瓶</p>
                      <div className="gjw_tabBox_price">￥520</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="52度 中国福 金六福 经典08（2008年产）500ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/109.jpg" title="." alt="." />
                      </div>
                      <p>52度 中国福 金六福 经典08（2008年产）500ml</p>
                      <div className="gjw_tabBox_price">￥249</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="52度 酒鬼 红坛 15年酒 500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/110.jpg" title="." alt="." />
                      </div>
                      <p>52度 酒鬼 红坛 15年酒 500ml</p>
                      <div className="gjw_tabBox_price">￥299</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="53度 汾酒集团 一坛香 陈酿老酒 青花典藏 1500ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/111.jpg" title="." alt="." />
                      </div>
                      <p>53度 汾酒集团 一坛香 陈酿老酒 青花典藏 1500ml</p>
                      <div className="gjw_tabBox_price">￥199</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="53度 茅台 大木珍 礼盒装 500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/112.jpg" title="." alt="." />
                      </div>
                      <p>53度 茅台 大木珍 礼盒装 500ml</p>
                      <div className="gjw_tabBox_price">￥2980</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="52度 剑南春 鉴藏级 礼盒 680ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/113.jpg" title="." alt="." />
                      </div>
                      <p>52度 剑南春 鉴藏级 礼盒 680ml</p>
                      <div className="gjw_tabBox_price">￥399</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="53度 习酒 窖藏1998 500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/114.jpg" title="." alt="." />
                      </div>
                      <p>53度 习酒 窖藏1998 500ml</p>
                      <div className="gjw_tabBox_price">￥319</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="52度 泸州老窖 特曲 古法酿造 500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/115.jpg" title="." alt="." />
                      </div>
                      <p>52度 泸州老窖 特曲 古法酿造 500ml</p>
                      <div className="gjw_tabBox_price">￥159</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="52度 西凤 凤香经典 10年 500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/116.jpg" title="." alt="." />
                      </div>
                      <p>52度 西凤 凤香经典 10年 500ml</p>
                      <div className="gjw_tabBox_price">￥179</div>
                    </LinkA>
                  </li>
                </ul>
              </div>

              <div className="gjw_tabBox fl qq">
                <ul className="gjw_tabBox_proList">
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="50度 五粮液 红火爆 喜庆酒 475ml （2011年产）*6"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/117.jpg" title="." alt="." />
                      </div>
                      <p>50度 五粮液 红火爆 喜庆酒 475ml （2011年产）*6</p>
                      <div className="gjw_tabBox_price">￥249</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="53度 汾酒 陈酿老酒 一坛香 三星 475ml*6"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/118.jpg" title="." alt="." />
                      </div>
                      <p>53度 汾酒 陈酿老酒 一坛香 三星 475ml*6</p>
                      <div className="gjw_tabBox_price">￥219</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="52度 五粮迎宾酒 上品 500ml*6">
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/119.jpg"
                          title="52度 五粮 迎宾酒 上品 500ml*6"
                          alt="52度 五粮 迎宾酒 上品 500ml*6"
                        />
                      </div>
                      <p>52度 五粮迎宾酒 上品 500ml*6</p>
                      <div className="gjw_tabBox_price">￥498</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="52度 五粮液股份 金乔禧（红色） 500ml*6"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/120.jpg" title="." alt="." />
                      </div>
                      <p>52度 五粮液股份 金乔禧（红色） 500ml*6</p>
                      <div className="gjw_tabBox_price">￥948</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="53度 茅台 迎宾酒 500ml*6瓶">
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/121.jpg"
                          title="53度 茅台 迎宾酒 500ml*6瓶"
                          alt="53度 茅台 迎宾酒 500ml*6瓶"
                        />
                      </div>
                      <p>53度 茅台 迎宾酒 500ml*6瓶</p>
                      <div className="gjw_tabBox_price">￥343</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="52度 洋河 洋河大曲 青瓷 500ml*6">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/122.jpg" title="." alt="." />
                      </div>
                      <p>52度 洋河 洋河大曲 青瓷 500ml*6</p>
                      <div className="gjw_tabBox_price">￥349</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="52度 金六福 福星高照五星酒 450ml*6">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/123.jpg" title="." alt="." />
                      </div>
                      <p>52度 金六福 福星高照五星酒 450ml*6</p>
                      <div className="gjw_tabBox_price">￥1188</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="52度 泸州老窖 老窖陈酒 珍品 500ml*6"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/124.jpg" title="." alt="." />
                      </div>
                      <p>52度 泸州老窖 老窖陈酒 珍品 500ml*6</p>
                      <div className="gjw_tabBox_price">￥288</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="52度 小糊涂仙 500ml*6">
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/125.jpg"
                          title="52度 小糊涂仙 500ml*6"
                          alt="52度 小糊涂仙 500ml*6"
                        />
                      </div>
                      <p>52度 小糊涂仙 500ml*6</p>
                      <div className="gjw_tabBox_price">￥698</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="52度 泸州老窖 老窖真藏 精酿6号 500ml*6"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/126.jpg"
                          title="52度 泸州老窖 老窖真藏 精酿6号 500ml*6（首款电商特供酒）"
                          alt="52度 泸州老窖 老窖真藏 精酿6号 500ml*6（首款电商特供酒）"
                        />
                      </div>
                      <p>52度 泸州老窖 老窖真藏 精酿6号 500ml*6</p>
                      <div className="gjw_tabBox_price">￥328</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="53度 茅台 王子酒 500ml*6瓶">
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/127.jpg"
                          title="53度 茅台 王子酒 500ml*6瓶"
                          alt="53度 茅台 王子酒 500ml*6瓶"
                        />
                      </div>
                      <p>53度 茅台 王子酒 500ml*6瓶</p>
                      <div className="gjw_tabBox_price">￥529</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="52度 茅台集团 经典玉液V11 500ml*6">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/128.jpg" title="." alt="." />
                      </div>
                      <p>52度 茅台集团 经典玉液V11 500ml*6</p>
                      <div className="gjw_tabBox_price">￥469</div>
                    </LinkA>
                  </li>
                </ul>
              </div>

              <div className="gjw_tabBox fl qq">
                <ul className="gjw_tabBox_proList">
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="45度 茅台醇 福酒 小酒版 125ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/129.jpg" title="." alt="." />
                      </div>
                      <p>45度 茅台醇 福酒 小酒版 125ml</p>
                      <div className="gjw_tabBox_price">￥15</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="52度 水井坊 春夏秋冬 小酒版 75ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/151.jpg" title="." alt="." />
                      </div>
                      <p>52度 水井坊 春夏秋冬 小酒版 75ml</p>
                      <div className="gjw_tabBox_price">￥120</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="51度 汉酱 小酒版 125ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/130.jpg"
                          title="51度 茅台 汉酱 小酒版 125ml"
                          alt="51度 茅台 汉酱 小酒版 125ml"
                        />
                      </div>
                      <p>51度 汉酱 小酒版 125ml</p>
                      <div className="gjw_tabBox_price">￥68</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="53度 茅台 小酒版 50ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/131.jpg" title="." alt="." />
                      </div>
                      <p>53度 茅台 小酒版 50ml</p>
                      <div className="gjw_tabBox_price">￥139</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="35度 农垦大粮仓 淡雅酒 250ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/132.jpg" title="." alt="." />
                      </div>
                      <p>35度 农垦大粮仓 淡雅酒 250ml</p>
                      <div className="gjw_tabBox_price">￥19</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="52度 五粮液 小酒版（光瓶装） 100ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/133.jpg"
                          title="52度 五粮液 小酒版（光瓶装） 100ml"
                          alt="52度 五粮液 小酒版（光瓶装） 100ml"
                        />
                      </div>
                      <p>52度 五粮液 小酒版（光瓶装） 100ml</p>
                      <div className="gjw_tabBox_price">￥129</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="52度 舍得酒 小酒版（光瓶装） 100ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/134.jpg" title="." alt="." />
                      </div>
                      <p>52度 舍得酒 小酒版（光瓶装） 100ml</p>
                      <div className="gjw_tabBox_price">￥98</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="52度 舍得酒 （2010年产 ）250ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/135.jpg"
                          title="52度 舍得酒 （2010年产 ）250ml"
                          alt="52度 舍得酒 （2010年产 ）250ml"
                        />
                      </div>
                      <p>52度 舍得酒 （2010年产 ）250ml</p>
                      <div className="gjw_tabBox_price">￥219</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="52度 小酒人生 小酒版 100ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/136.jpg" title="." alt="." />
                      </div>
                      <p>52度 小酒人生 小酒版 100ml</p>
                      <div className="gjw_tabBox_price">￥0.1</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="53度 郎酒 108老郎酒 小酒版 108ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/137.jpg"
                          title="53度 郎酒 108老郎酒 小酒版 108ml"
                          alt="53度 郎酒 108老郎酒 小酒版 108ml"
                        />
                      </div>
                      <p>53度 郎酒 108老郎酒 小酒版 108ml</p>
                      <div className="gjw_tabBox_price">￥25</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="52度 小糊涂仙 精品 250ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/138.jpg"
                          title="52度 小糊涂仙 精品 250ml"
                          alt="52度 小糊涂仙 精品 250ml"
                        />
                      </div>
                      <p>52度 小糊涂仙 精品 250ml</p>
                      <div className="gjw_tabBox_price">￥137</div>
                    </LinkA>
                  </li>
                </ul>
              </div>

              <div className="gjw_tabBox fl qq">
                <ul className="gjw_tabBox_proList">
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="53度 茅台王子酒 珍品 （2008年产）1000ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/139.jpg" title="." alt="." />
                      </div>
                      <p>53度 茅台王子酒 珍品 （2008年产）1000ml</p>
                      <div className="gjw_tabBox_price">￥699</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="53度珍酒 十二年陈酿 500ml（2006年产）"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/140.jpg"
                          title="53度珍酒 十二年陈酿 500ml（2006年产）"
                          alt="53度珍酒 十二年陈酿 500ml（2006年产）"
                        />
                      </div>
                      <p>53度珍酒 十二年陈酿 500ml（2006年产）</p>
                      <div className="gjw_tabBox_price">￥189</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="52度 精制 文君酒 （2003年产） 500ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/141.jpg"
                          title="52度 精制 文君酒 （2003年产） 500ml"
                          alt="52度 精制 文君酒 （2003年产） 500ml"
                        />
                      </div>
                      <p>52度 精制 文君酒 （2003年产） 500ml</p>
                      <div className="gjw_tabBox_price">￥169</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="38度 西凤牌 西凤醇 （96年产）500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/142.jpg" title="." alt="." />
                      </div>
                      <p>38度 西凤牌 西凤醇 （96年产）500ml</p>
                      <div className="gjw_tabBox_price">￥99</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="50度 沱牌老窖 2007年产 500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/143.jpg" title="." alt="." />
                      </div>
                      <p>50度 沱牌老窖 2007年产 500ml</p>
                      <div className="gjw_tabBox_price">￥66</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="38度 董酒（2000年产）500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/144.jpg" title="." alt="." />
                      </div>
                      <p>38度 董酒（2000年产）500ml</p>
                      <div className="gjw_tabBox_price">￥109</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="52度 双沟大曲 百年古窖 2002年产 500ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/145.jpg"
                          title="52度 双沟大曲 百年古窖 2002年产 500ml"
                          alt="52度 双沟大曲 百年古窖 2002年产 500ml"
                        />
                      </div>
                      <p>52度 双沟大曲 百年古窖 2002年产 500ml</p>
                      <div className="gjw_tabBox_price">￥206</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="52度 泸州迎宾郎酒 99年产 500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/146.jpg" title="." alt="." />
                      </div>
                      <p>52度 泸州迎宾郎酒 99年产 500ml</p>
                      <div className="gjw_tabBox_price">￥181</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="52度 习酒 金牌国浆酒 一品（2005年） 500ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/147.jpg"
                          title="52度 习酒 金牌国浆酒 一品（2005年） 500ml"
                          alt="52度 习酒 金牌国浆酒 一品（2005年） 500ml"
                        />
                      </div>
                      <p>52度 习酒 金牌国浆酒 一品（2005年） 500ml</p>
                      <div className="gjw_tabBox_price">￥288</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="45度 竹叶青 2006年产 475ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/148.jpg" title="·" alt="·" />
                      </div>
                      <p>45度 竹叶青 2006年产 475ml</p>
                      <div className="gjw_tabBox_price">￥131</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="53度 汾酒 2006年产 475ml  45度 竹叶青 2006年产 475ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/149.jpg" title="." alt="." />
                      </div>
                      <p>53度 汾酒 2006年产 475ml 45度 竹叶青 2006年产 475ml</p>
                      <div className="gjw_tabBox_price">￥211</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="52度 全兴大曲 八年陈（2003年产）500ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/150.jpg" title="." alt="." />
                      </div>
                      <p>52度 全兴大曲 八年陈（2003年产）500ml</p>
                      <div className="gjw_tabBox_price">￥198</div>
                    </LinkA>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        {/* <!--二楼红酒--> */}
        <div className="floor2 gjw_tab floor" id="floor2">
          <div className="floor_t">
            <h2 className="fl">
              <i>2F</i>红酒馆
            </h2>
            <ul className="gjw_tabItemBox fr">
              <li className="gjw_tabItem mb active">
                <LinkA>热门红酒</LinkA>
                <span />
              </li>
              <li className="gjw_tabItem mb">
                <LinkA>整合装送礼</LinkA>
                <span />
              </li>
              <li className="gjw_tabItem mb">
                <LinkA>整箱套装优惠价</LinkA>
                <span />
              </li>
              <li className="gjw_tabItem mb">
                <LinkA>美女选择</LinkA>
                <span />
              </li>
              <li className="gjw_tabItem mb">
                <LinkA>原装进口</LinkA>
                <span />
              </li>
              <li className="gjw_tabItem mb">
                <LinkA>超值国货</LinkA>
                <span />
              </li>
              <li className="gjw_tabItem">
                <LinkA>更多</LinkA>
              </li>
            </ul>
          </div>
          <div className="floor_container">
            <div className="gjw_tabSide fl">
              <LinkA className="gjw_tabSideBanner shine">
                <Image src="images/152.jpg" style={{ display: 'block' }} />
              </LinkA>
              <ul className="gjw_tabSideNav">
                <li>
                  <LinkA>拉菲专区</LinkA>
                </li>
                <li>
                  <LinkA>红酒会馆</LinkA>
                </li>
                <li>
                  <LinkA>法国皇轩</LinkA>
                </li>
                <li>
                  <LinkA>加州乐事</LinkA>
                </li>
                <li>
                  <LinkA>酩悦香槟 </LinkA>
                </li>
                <li>
                  <LinkA>黄尾袋鼠</LinkA>
                </li>
                <li>
                  <LinkA>卡斯特</LinkA>
                </li>
                <li>
                  <LinkA>张裕专区</LinkA>
                </li>
              </ul>
              <LinkA className="tab_sideAdLink shine">
                <Image src="images/153.jpg" style={{ display: 'inline' }} className="tab_sideAd" />
              </LinkA>
              <LinkA className="tab_sideAdLink2 shine">
                <Image src="images/154.jpg" style={{ display: 'inline' }} className="tab_sideAd" />
              </LinkA>
            </div>

            <div className="gjw_tabBigBox fl">
              {/* <!--第一屏--> */}
              <div className="gjw_tabBox fl ww" style={{ display: 'block' }}>
                <div className="gjw_slideBox fl">
                  <div
                    className="tempWrap"
                    style={{ overflow: 'hidden', ' position': 'relative', ' width': '440px' }}
                  >
                    <ul
                      style={{
                        width: '2200px',
                        position: ' relative',
                        ' overflow': 'hidden',
                        padding: ' 0px',
                        ' margin': ' 0px',
                        ' left': '0px',
                      }}
                      className="gjw_slideBody"
                      id="gjw1"
                    >
                      <li style={{ float: 'left', width: '440px' }} className="clone">
                        <LinkA>
                          <Image
                            style={{ width: '440px', height: ' 237px' }}
                            src="images/155.jpg"
                            className="notloadlazy"
                            alt=""
                            height="237px"
                            width="435px"
                          />
                        </LinkA>
                      </li>
                      <li style={{ float: 'left', width: '440px' }}>
                        <LinkA>
                          <Image
                            style={{ width: '440px', height: ' 237px' }}
                            src="images/156.jpg"
                            className="notloadlazy"
                            alt=""
                            height="237px"
                            width="435px"
                          />
                        </LinkA>
                      </li>
                      <li style={{ float: 'left', width: '440px' }}>
                        <LinkA>
                          <Image
                            style={{ width: '440px', height: ' 237px' }}
                            src="images/157.jpg"
                            className="notloadlazy"
                            alt=""
                            height="237px"
                            width="435px"
                          />
                        </LinkA>
                      </li>
                    </ul>
                  </div>
                  <div className="gjw_slideBtn">
                    <LinkA className="gjw_slidePrev" id="prevBtn1">
                      &lt","
                    </LinkA>
                    <LinkA className="gjw_slideNext" id="nextBtn1">
                      &gt;
                    </LinkA>
                  </div>
                  <div className="gjw_slideNav" id="gjwList1">
                    <LinkA className="active" />
                    <LinkA className="" />
                    <LinkA className="" />
                  </div>
                </div>
                <div className="fr gjw_adBox_xs">
                  <LinkA>
                    <Image src="images/160.jpg" style={{ display: 'inline' }} />
                  </LinkA>
                  <LinkA>
                    <Image src="images/161.jpg" style={{ display: 'inline' }} />
                  </LinkA>
                </div>
                <ul className="gjw_adBox_big clear">
                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/162.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>
                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/163.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>
                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/164.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>
                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/165.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>
                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/166.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>
                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/167.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>
                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/168.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>
                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/169.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>
                </ul>
              </div>
              {/* <!--第二屏--> */}
              <div className="gjw_tabBox fl ww">
                <ul className="gjw_tabBox_proList">
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="法国 路易拉菲（两支皮盒装） 干红葡萄酒750ml*2"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/170.jpg"
                          title="法国 路易拉菲（两支皮盒装） 干红葡萄酒750ml*2"
                          alt="法国 路易拉菲（两支皮盒装） 干红葡萄酒750ml*2"
                        />
                      </div>
                      <p>法国 路易拉菲（两支皮盒装） 干红葡萄酒750ml*2</p>
                      <div className="gjw_tabBox_price">￥99</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="长城 华夏94 圆桶珍藏品葡萄酒 750ml*2"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/171.jpg"
                          title="中国 长城 华夏94 圆桶珍藏品葡萄酒 750ml*2"
                        />
                      </div>
                      <p>长城 华夏94 圆桶珍藏品葡萄酒 750ml*2</p>
                      <div className="gjw_tabBox_price">￥188</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="法国 拉菲传说（经典鳄鱼纹皮盒）750ml*2"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/172.jpg" />
                      </div>
                      <p>法国 拉菲传说（经典鳄鱼纹皮盒）750ml*2</p>
                      <div className="gjw_tabBox_price">￥199</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="法国 拉菲传奇（皮盒装） 750ml*2">
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/173.jpg"
                          title="法国 拉菲传奇（皮盒装） 750ml*2"
                          alt="法国 拉菲传奇（皮盒装） 750ml*2"
                        />
                      </div>
                      <p>法国 拉菲传奇（皮盒装） 750ml*2</p>
                      <div className="gjw_tabBox_price">￥158</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="拉菲 庄园2011城堡干红葡萄酒 750ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/174.jpg" title="拉菲 庄园2011城堡干红葡萄酒 750ml" />
                      </div>
                      <p>拉菲 庄园2011城堡干红葡萄酒 750ml</p>
                      <div className="gjw_tabBox_price">￥318</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="长城 华夏92 木盒珍藏级葡萄酒 750ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/175.jpg" title="长城 华夏92 木盒珍藏级葡萄酒 750ml" />
                      </div>
                      <p>长城 华夏92 木盒珍藏级葡萄酒 750ml</p>
                      <div className="gjw_tabBox_price">￥136</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="法国 卡斯特 邦塞 美露干红葡萄酒（2011） 750ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/176.jpg" />
                      </div>
                      <p>法国 卡斯特 邦塞 美露干红葡萄酒（2011） 750ml</p>
                      <div className="gjw_tabBox_price">￥198</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="美国 所罗门 珍藏赤霞珠干红葡萄酒（木盒） 750ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/177.jpg" />
                      </div>
                      <p>美国 所罗门 珍藏赤霞珠干红葡萄酒（木盒） 750ml</p>
                      <div className="gjw_tabBox_price">￥259</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="智利 红蔓庄园 赤霞珠干红葡萄酒 750ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/178.jpg" />
                      </div>
                      <p>智利 红蔓庄园 赤霞珠干红葡萄酒 750ml</p>
                      <div className="gjw_tabBox_price">￥65</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="法国 卡斯特 世家赤霞珠 干红葡萄酒 750ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/179.jpg" />
                      </div>
                      <p>法国 卡斯特 世家赤霞珠 干红葡萄酒 750ml</p>
                      <div className="gjw_tabBox_price">￥239</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="法国 卡斯特 世家干红葡萄酒 750ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/180.jpg" />
                      </div>
                      <p>法国 卡斯特 世家干红葡萄酒 750ml</p>
                      <div className="gjw_tabBox_price">￥109</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="张裕 品酒大师 解百纳干红葡萄酒 礼盒 750ml*2瓶"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/181.jpg"
                          title="张裕 品酒大师 解百纳干红葡萄酒 礼盒 750ml*2瓶"
                        />
                      </div>
                      <p>张裕 品酒大师 解百纳干红葡萄酒 礼盒 750ml*2瓶</p>
                      <div className="gjw_tabBox_price">￥199</div>
                    </LinkA>
                  </li>
                </ul>
              </div>
              {/* <!-- 第三屏 -->         */}
              <div className="gjw_tabBox fl ww">
                <ul className="gjw_tabBox_proList">
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="西班牙 贝雷斯 干红葡萄酒 750ml*6">
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/182.jpg"
                          title="西班牙 贝雷斯 干红葡萄酒 750ml*6"
                          alt="西班牙 贝雷斯 干红葡萄酒 750ml*6"
                        />
                      </div>
                      <p>西班牙 贝雷斯 干红葡萄酒 750ml*6</p>
                      <div className="gjw_tabBox_price">￥189</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="法国 木桐 夏嘉城堡 干红葡萄酒 750ml*6"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/183.jpg" title="." alt="." />
                      </div>
                      <p>法国 木桐 夏嘉城堡 干红葡萄酒 750ml*6</p>
                      <div className="gjw_tabBox_price">￥199</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="澳大利亚 奔富 洛神山庄 西拉干红葡萄酒 750ml*6"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/184.jpg" title="." alt="." />
                      </div>
                      <p>澳大利亚 奔富 洛神山庄 西拉干红葡萄酒 750ml*6</p>
                      <div className="gjw_tabBox_price">￥258</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="澳大利亚 黄尾袋鼠 梅洛红葡萄酒 750ml*6"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/185.jpg" title="." alt="." />
                      </div>
                      <p>澳大利亚 黄尾袋鼠 梅洛红葡萄酒 750ml*6</p>
                      <div className="gjw_tabBox_price">￥279</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="美国 加州乐事 干红葡萄酒 750ml *6">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/186.jpg" title="." alt="." />
                      </div>
                      <p>美国 加州乐事 干红葡萄酒 750ml *6</p>
                      <div className="gjw_tabBox_price">￥288</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="西班牙 唐卢卡 干红葡萄酒 750ml*6">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/187.jpg" title="." alt="." />
                      </div>
                      <p>西班牙 唐卢卡 干红葡萄酒 750ml*6</p>
                      <div className="gjw_tabBox_price">￥228</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="法国 拉菲 传奇波尔多AOC干红葡萄酒 正品行货 750ml*6 整箱装"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/188.jpg"
                          title="法国Lafite拉菲 传奇波尔多AOC 干红葡萄酒 正品行货 750ml*6"
                          alt="法国Lafite拉菲 传奇波尔多AOC 干红葡萄酒 正品行货 750ml*6"
                        />
                      </div>
                      <p>法国 拉菲 传奇波尔多AOC干红葡萄酒 正品行货 750ml*6 整箱装</p>
                      <div className="gjw_tabBox_price">￥388</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="法比纳古堡 皇族雄狮系列 鲁西荣 干红葡萄酒750ml*6 （木箱原装进口 ）"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/189.jpg" title="." alt="." />
                      </div>
                      <p>法比纳古堡 皇族雄狮系列 鲁西荣 干红葡萄酒750ml*6 （木箱原装进口 ）</p>
                      <div className="gjw_tabBox_price">￥999</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="南非 艾拉贝拉 西拉 干红葡萄酒 750ml*6"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/190.jpg" title="." alt="." />
                      </div>
                      <p>南非 艾拉贝拉 西拉 干红葡萄酒 750ml*6</p>
                      <div className="gjw_tabBox_price">￥309</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="智利 干露 红魔鬼 黑皮诺红葡萄酒2011 750ml*6"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/191.jpg" title="." alt="." />
                      </div>
                      <p>智利 干露 红魔鬼 黑皮诺红葡萄酒2011 750ml*6</p>
                      <div className="gjw_tabBox_price">￥408</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="法国 法比纳古堡 皇族雄狮系列 高卢雄鸡干红葡萄酒 750ml*6"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/192.jpg" title="." alt="." />
                      </div>
                      <p>法国 法比纳古堡 皇族雄狮系列 高卢雄鸡干红葡萄酒 750ml*6</p>
                      <div className="gjw_tabBox_price">￥229</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="法国 法比纳古堡 皇族雄狮系列 蓝羽干红葡萄酒 750ml*6"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/193.jpg" title="." alt="." />
                      </div>
                      <p>法国 法比纳古堡 皇族雄狮系列 蓝羽干红葡萄酒 750ml*6</p>
                      <div className="gjw_tabBox_price">￥299</div>
                    </LinkA>
                  </li>
                </ul>
              </div>
              {/* <!-- 第四屏 -->   */}
              <div className="gjw_tabBox fl ww">
                <ul className="gjw_tabBox_proList">
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="智利 干露 红魔鬼 梅洛红葡萄酒2014（螺旋盖） 750ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/194.jpg" title="1" alt="1" />
                      </div>
                      <p>智利 干露 红魔鬼 梅洛红葡萄酒2014（螺旋盖） 750ml</p>
                      <div className="gjw_tabBox_price">￥75</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="智利 晨曦之星 赤霞珠 干红葡萄酒 750ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/195.jpg" title="." alt="." />
                      </div>
                      <p>智利 晨曦之星 赤霞珠 干红葡萄酒 750ml</p>
                      <div className="gjw_tabBox_price">￥48</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="意大利  激情飞扬  甜型气泡葡萄酒750ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/196.jpg" title="." alt="." />
                      </div>
                      <p>意大利 激情飞扬 甜型气泡葡萄酒750ml</p>
                      <div className="gjw_tabBox_price">￥69</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="智利 干露 红魔鬼 卡本妮/赤霞珠红葡萄酒 2014  750ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/197.jpg" title="." alt="." />
                      </div>
                      <p>智利 干露 红魔鬼 卡本妮/赤霞珠红葡萄酒 2014 750ml</p>
                      <div className="gjw_tabBox_price">￥98</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="法国 木桐 夏嘉城堡 干红葡萄酒 750ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/198.jpg" title="." alt="." />
                      </div>
                      <p>法国 木桐 夏嘉城堡 干红葡萄酒 750ml</p>
                      <div className="gjw_tabBox_price">￥49</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="西班牙玛莉迪麦干红葡萄酒750ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/199.jpg" title="." alt="." />
                      </div>
                      <p>西班牙玛莉迪麦干红葡萄酒750ml</p>
                      <div className="gjw_tabBox_price">￥49</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="法国拉菲特干红葡萄酒 750ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/200.jpg" title="." alt="." />
                      </div>
                      <p>法国拉菲特干红葡萄酒 750ml</p>
                      <div className="gjw_tabBox_price">￥39</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="意大利玫瑰小屋桃红起泡葡萄酒750ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/201.jpg" title="意大利玫瑰小屋桃红起泡葡萄酒750ml" />
                      </div>
                      <p>意大利玫瑰小屋桃红起泡葡萄酒750ml</p>
                      <div className="gjw_tabBox_price">￥48</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="法国 路易拉菲 干红葡萄酒 750ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/202.jpg" title="." alt="." />
                      </div>
                      <p>法国 路易拉菲 干红葡萄酒 750ml</p>
                      <div className="gjw_tabBox_price">￥39.9</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="智利 拉丁神话 长相思白葡萄酒750ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/203.jpg" title="." alt="." />
                      </div>
                      <p>智利 拉丁神话 长相思白葡萄酒750ml</p>
                      <div className="gjw_tabBox_price">￥99</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="西班牙唐卢卡干红葡萄酒750ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/204.jpg" title="." alt="." />
                      </div>
                      <p>西班牙唐卢卡干红葡萄酒750ml</p>
                      <div className="gjw_tabBox_price">￥69</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="智利 神树珍藏 赤霞珠 干红葡萄酒 750ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/205.jpg" title="." alt="." />
                      </div>
                      <p>智利 神树珍藏 赤霞珠 干红葡萄酒 750ml</p>
                      <div className="gjw_tabBox_price">￥99</div>
                    </LinkA>
                  </li>
                </ul>
              </div>
              {/* <!-- 第五屏 --> */}
              <div className="gjw_tabBox fl ww">
                <ul className="gjw_tabBox_proList">
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="澳大利亚 奔富 洛神山庄 设拉子赤霞珠 红葡萄酒（螺纹盖） 750ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/206.jpg" title="." alt="." />
                      </div>
                      <p>澳大利亚 奔富 洛神山庄 设拉子赤霞珠 红葡萄酒（螺纹盖） 750ml</p>
                      <div className="gjw_tabBox_price">￥45</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="美国 加州乐事 白葡萄酒 750ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/207.jpg" title="." alt="." />
                      </div>
                      <p>美国 加州乐事 白葡萄酒 750ml</p>
                      <div className="gjw_tabBox_price">￥59</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="智利 晨曦之星 赤霞珠 干红葡萄酒 750ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/208.jpg" title="." alt="." />
                      </div>
                      <p>智利 晨曦之星 赤霞珠 干红葡萄酒 750ml</p>
                      <div className="gjw_tabBox_price">￥48</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="法国 法莱雅酒堡 西拉干红葡萄酒（金瓶） 750ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/209.jpg" title="." alt="." />
                      </div>
                      <p>法国 法莱雅酒堡 西拉干红葡萄酒（金瓶） 750ml</p>
                      <div className="gjw_tabBox_price">￥99</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="澳大利亚 奔富酒园蔻兰山 西拉子红葡萄酒 750ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/210.jpg" title="." alt="." />
                      </div>
                      <p>澳大利亚 奔富酒园蔻兰山 西拉子红葡萄酒 750ml</p>
                      <div className="gjw_tabBox_price">￥98</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="澳大利亚 杰卡斯 西拉加本纳干红葡萄酒 750ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/211.jpg" title="." alt="." />
                      </div>
                      <p>澳大利亚 杰卡斯 西拉加本纳干红葡萄酒 750ml</p>
                      <div className="gjw_tabBox_price">￥79</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="南非 艾拉贝拉 美乐 干红葡萄酒 750ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/212.jpg" title="." alt="." />
                      </div>
                      <p>南非 艾拉贝拉 美乐 干红葡萄酒 750ml</p>
                      <div className="gjw_tabBox_price">￥29</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="澳大利亚 红山溪 西拉干红葡萄酒 750ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/213.jpg" title="." alt="." />
                      </div>
                      <p>澳大利亚 红山溪 西拉干红葡萄酒 750ml</p>
                      <div className="gjw_tabBox_price">￥59</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="德国 菲波 金碧甜型起泡葡萄酒 750ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/214.jpg"
                          title="德国 菲波 金碧甜型起泡葡萄酒 750ml"
                          alt="德国 菲波 金碧甜型起泡葡萄酒 750ml"
                        />
                      </div>
                      <p>德国 菲波 金碧甜型起泡葡萄酒 750ml</p>
                      <div className="gjw_tabBox_price">￥55</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="澳大利亚 红山溪 美乐干红葡萄酒 750ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/215.jpg" title="." alt="." />
                      </div>
                      <p>澳大利亚 红山溪 美乐干红葡萄酒 750ml</p>
                      <div className="gjw_tabBox_price">￥99</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="法国 瑞德 干红葡萄酒 750ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/216.jpg" title="." alt="." />
                      </div>
                      <p>法国 瑞德 干红葡萄酒 750ml</p>
                      <div className="gjw_tabBox_price">￥58</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="法国 飞爵 起泡葡萄酒 750ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/217.jpg" title="." alt="." />
                      </div>
                      <p>法国 飞爵 起泡葡萄酒 750ml</p>
                      <div className="gjw_tabBox_price">￥39</div>
                    </LinkA>
                  </li>
                </ul>
              </div>
              {/* <!--  第六屏 --> */}
              <div className="gjw_tabBox fl ww">
                <ul className="gjw_tabBox_proList">
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="张裕 解百纳 特选级 干红葡萄酒 750ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/218.jpg"
                          title="张裕 解百纳 特选级 干红葡萄酒 750ml"
                          alt="张裕 解百纳 特选级 干红葡萄酒 750ml"
                        />
                      </div>
                      <p>张裕 解百纳 特选级 干红葡萄酒 750ml</p>
                      <div className="gjw_tabBox_price">￥118</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="长城 特选级 解百纳干红葡萄酒 750ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/219.jpg"
                          title="长城 特选级 解百纳干红葡萄酒 750ml"
                          alt="长城 特选级 解百纳干红葡萄酒 750ml"
                        />
                      </div>
                      <p>长城 特选级 解百纳干红葡萄酒 750ml</p>
                      <div className="gjw_tabBox_price">￥69</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="中国 新疆 尼雅臻爱永恒婚宴 干红葡萄酒 750ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/220.jpg" title="." alt="." />
                      </div>
                      <p>中国 新疆 尼雅臻爱永恒婚宴 干红葡萄酒 750ml</p>
                      <div className="gjw_tabBox_price">￥48</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="张裕 特选级 北京爱斐堡赤霞珠干红葡萄酒 750ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/221.jpg"
                          title="张裕 特选级 北京爱斐堡赤霞珠干红葡萄酒 750ml"
                          alt="张裕 特选级 北京爱斐堡赤霞珠干红葡萄酒 750ml"
                        />
                      </div>
                      <p>张裕 特选级 北京爱斐堡赤霞珠干红葡萄酒 750ml</p>
                      <div className="gjw_tabBox_price">￥398</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="王朝 半干白葡萄酒 750ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/222.jpg" title="." alt="." />
                      </div>
                      <p>王朝 半干白葡萄酒 750ml</p>
                      <div className="gjw_tabBox_price">￥48</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="王朝 精酿干红葡萄酒 750ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/223.jpg" title="." alt="." />
                      </div>
                      <p>王朝 精酿干红葡萄酒 750ml</p>
                      <div className="gjw_tabBox_price">￥58</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="中国 新疆 尼雅 金霞多丽甜白葡萄酒 375ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/224.jpg"
                          title="中国 新疆 尼雅 金霞多丽甜白葡萄酒 375ml"
                          alt="中国 新疆 尼雅 金霞多丽甜白葡萄酒 375ml"
                        />
                      </div>
                      <p>中国 新疆 尼雅 金霞多丽甜白葡萄酒 375ml</p>
                      <div className="gjw_tabBox_price">￥49</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="长城 华夏94 圆桶珍藏品葡萄酒 750ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/225.jpg"
                          title="长城 华夏94 圆桶珍藏品葡萄酒 750ml"
                          alt="长城 华夏94 圆桶珍藏品葡萄酒 750ml"
                        />
                      </div>
                      <p>长城 华夏94 圆桶珍藏品葡萄酒 750ml</p>
                      <div className="gjw_tabBox_price">￥109</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="王朝 大酒窖169赤霞珠 干红 葡萄酒 750ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/226.jpg" title="." alt="." />
                      </div>
                      <p>王朝 大酒窖169赤霞珠 干红 葡萄酒 750ml</p>
                      <div className="gjw_tabBox_price">￥48</div>
                    </LinkA>
                  </li>

                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="张裕 窖藏 特选级 解百纳干红葡萄酒 750ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/227.jpg" title="." alt="." />
                      </div>
                      <p>张裕 窖藏 特选级 解百纳干红葡萄酒 750ml</p>
                      <div className="gjw_tabBox_price">￥169</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="张裕 解百纳干红葡萄酒（珍藏级） 750ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/228.jpg"
                          title="张裕 解百纳干红葡萄酒（珍藏级） 750ml"
                          alt="张裕 解百纳干红葡萄酒（珍藏级） 750ml"
                        />
                      </div>
                      <p>张裕 解百纳干红葡萄酒（珍藏级） 750ml</p>
                      <div className="gjw_tabBox_price">￥179</div>
                    </LinkA>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        {/* <!--3洋酒馆s--> */}
        <div className="floor3 gjw_tab floor" id="floor3">
          <div className="floor_t">
            <h2 className="fl">
              <i>3F</i>洋酒馆
            </h2>
            <ul className="gjw_tabItemBox fr">
              <li className="gjw_tabItem db active">
                <LinkA>知名洋酒</LinkA>
                <span />
              </li>
              <li className="gjw_tabItem db">
                <LinkA>白兰地系列</LinkA>
                <span />
              </li>
              <li className="gjw_tabItem db">
                <LinkA>威士忌系列</LinkA>
                <span />
              </li>
              <li className="gjw_tabItem db">
                <LinkA>伏特加系列</LinkA>
                <span />
              </li>
              <li className="gjw_tabItem db">
                <LinkA>力娇酒系列</LinkA>
                <span />
              </li>
              <li className="gjw_tabItem">
                <LinkA>更多</LinkA>
              </li>
            </ul>
          </div>
          <div className="gjw_tab_c floor_container">
            <div className="gjw_tabSide fl">
              <LinkA className="gjw_tabSideBanner shine">
                <Image src="images/229.jpg" style={{ display: 'block' }} />
              </LinkA>
              <ul className="gjw_tabSideNav">
                <li>
                  <LinkA>芝华士 </LinkA>
                </li>
                <li>
                  <LinkA>格兰菲迪</LinkA>
                </li>
                <li>
                  <LinkA>尊尼获加</LinkA>
                </li>
                <li>
                  <LinkA>皇家礼炮</LinkA>
                </li>
                <li>
                  <LinkA>白兰地</LinkA>
                </li>
                <li>
                  <LinkA>伏特加</LinkA>
                </li>
                <li>
                  <LinkA>威士忌</LinkA>
                </li>
                <li>
                  <LinkA>皇家路易</LinkA>
                </li>
              </ul>
              <LinkA className="tab_sideAdLink shine">
                <Image src="images/230.jpg" style={{ display: 'inline' }} className="tab_sideAd" />
              </LinkA>
              <LinkA className="tab_sideAdLink2 shine">
                <Image src="images/231.jpg" style={{ display: 'inline' }} className="tab_sideAd" />
              </LinkA>
            </div>
            <div className="gjw_tabBigBox fl">
              {/* <!--第一屏s--> */}
              <div className="gjw_tabBox fl vv" style={{ display: 'block' }}>
                <div className="gjw_slideBox fl">
                  <div
                    className="tempWrap"
                    style={{ overflow: 'hidden', ' position': 'relative', ' width': '440px' }}
                  >
                    <ul
                      style={{
                        width: '2200px',
                        position: ' relative',
                        ' overflow': ' hidden',
                        ' padding': ' 0px',
                        ' margin': ' 0px',
                      }}
                      className="gjw_slideBody"
                      id="gjw"
                    >
                      <li style={{ float: 'left', width: '440px' }} className="clone">
                        <LinkA>
                          <Image
                            style={{ width: '440px', height: ' 237px' }}
                            src="images/232.jpg"
                            className="notloadlazy"
                            height="237px"
                            width="435px"
                          />
                        </LinkA>
                      </li>
                      <li style={{ float: 'left', width: '440px' }}>
                        <LinkA>
                          <Image
                            style={{ width: '440px', height: ' 237px' }}
                            src="images/234.jpg"
                            className="notloadlazy"
                            alt=""
                            height="237px"
                            width="435px"
                          />
                        </LinkA>
                      </li>
                      <li style={{ float: 'left', width: '440px' }}>
                        <LinkA>
                          <Image
                            style={{ width: '440px', height: ' 237px' }}
                            src="images/235.jpg"
                            className="notloadlazy"
                            alt=""
                            height="237px"
                            width="435px"
                          />
                        </LinkA>
                      </li>
                    </ul>
                  </div>
                  <div className="gjw_slideBtn">
                    <LinkA className="gjw_slidePrev" id="prevBtn">
                      &lt","
                    </LinkA>
                    <LinkA className="gjw_slideNext" id="nextBtn">
                      &gt;
                    </LinkA>
                  </div>
                  <div className="gjw_slideNav" id="gjwList">
                    <LinkA className="active" />
                    <LinkA className="" />
                    <LinkA className="" />
                  </div>
                </div>
                <div className="fr gjw_adBox_xs">
                  <LinkA>
                    <Image src="images/238.jpg" style={{ display: 'inline' }} />
                  </LinkA>
                  <LinkA>
                    <Image src="images/239.jpg" style={{ display: 'inline' }} />
                  </LinkA>
                </div>
                <ul className="gjw_adBox_big clear">
                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/233.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>
                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/240.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>
                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/241.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>
                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/242.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>
                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/243.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>
                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/244.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>
                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/245.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>
                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/246.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>
                </ul>
              </div>
              {/* <!--第二屏--> */}
              <div className="gjw_tabBox fl vv">
                <ul className="gjw_tabBox_proList">
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="皇家路易 XO 红色天地盖 700ml+50ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/247.jpg" title="皇家路易 XO 红色天地盖" />
                      </div>
                      <p>皇家路易 XO 红色天地盖 700ml+50ml</p>
                      <div className="gjw_tabBox_price">￥188</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="皇家路易 XO 舵手酒架 700ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/248.jpg" title="皇家路易 XO 舵手酒架 700ml" />
                      </div>
                      <p>皇家路易 XO 舵手酒架 700ml</p>
                      <div className="gjw_tabBox_price">￥399</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="马爹利 名士 干邑白兰地 700ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/249.jpg" title="马爹利 名士 干邑白兰地 700ml" />
                      </div>
                      <p>马爹利 名士 干邑白兰地 700ml</p>
                      <div className="gjw_tabBox_price">￥399</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="皇家路易 XO 至尊水晶架 1500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/250.jpg" title="皇家路易 XO 至尊水晶架 1500ml" />
                      </div>
                      <p>皇家路易 XO 至尊水晶架 1500ml</p>
                      <div className="gjw_tabBox_price">￥488</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="马爹利 XO 干邑白兰地 700ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/251.jpg"
                          title="马爹利 XO 干邑白兰地 700ml"
                          alt="马爹利 XO 干邑白兰地 700ml"
                        />
                      </div>
                      <p>马爹利 XO 干邑白兰地 700ml</p>
                      <div className="gjw_tabBox_price">￥1688</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="马爹利 蓝带 干邑白兰地 700ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/252.jpg" title="." alt="." />
                      </div>
                      <p>马爹利 蓝带 干邑白兰地 700ml</p>
                      <div className="gjw_tabBox_price">￥1099</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="阿尼斯果渣白兰地 750ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/253.jpg" />
                      </div>
                      <p>阿尼斯果渣白兰地 750ml</p>
                      <div className="gjw_tabBox_price">￥115</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="人头马 俱乐部 干邑白兰地 700ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/254.jpg" title="·" alt="·" />
                      </div>
                      <p>人头马 俱乐部 干邑白兰地 700ml</p>
                      <div className="gjw_tabBox_price">￥580</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="轩尼诗Hennessy XO 干邑白兰地 1500ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/255.jpg" title="." alt="." />
                      </div>
                      <p>轩尼诗Hennessy XO 干邑白兰地 1500ml</p>
                      <div className="gjw_tabBox_price">￥3489</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="马爹利 蓝带 干邑白兰地 1500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/256.jpg" />
                      </div>
                      <p>马爹利 蓝带 干邑白兰地 1500ml</p>
                      <div className="gjw_tabBox_price">￥2580</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="轩尼诗Hennessy XO 干邑白兰地 700ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/257.jpg" />
                      </div>
                      <p>轩尼诗Hennessy XO 干邑白兰地 700ml</p>
                      <div className="gjw_tabBox_price">￥1999</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="马爹利 名士 干邑白兰地 小酒版 50ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/258.jpg" title="." alt="." />
                      </div>
                      <p>马爹利 名士 干邑白兰地 小酒版 50ml</p>
                      <div className="gjw_tabBox_price">￥48</div>
                    </LinkA>
                  </li>
                </ul>
              </div>
              {/* <!-- 第三屏 -->      */}
              <div className="gjw_tabBox fl vv">
                <ul className="gjw_tabBox_proList">
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="兄弟（JHBROS） 铁塔12年典藏威士忌 375ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/259.jpg" />
                      </div>
                      <p>兄弟（JHBROS） 铁塔12年典藏威士忌 375ml</p>
                      <div className="gjw_tabBox_price">￥49</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="尊尼获加Johnnie Walker 黑牌威士忌 700ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/260.jpg" />
                      </div>
                      <p>尊尼获加Johnnie Walker 黑牌威士忌 700ml</p>
                      <div className="gjw_tabBox_price">￥199</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="格兰御鹿 调配苏格兰威士忌 700ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/261.jpg" title="格兰御鹿 调配苏格兰威士忌 700ml" />
                      </div>
                      <p>格兰御鹿 调配苏格兰威士忌 700ml</p>
                      <div className="gjw_tabBox_price">￥59</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="正品40度杰克丹尼威士忌700ml 进口洋酒威士忌 夜店鸡尾酒"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/262.jpg"
                          title="杰克丹尼威士忌700ml"
                          alt="杰克丹尼威士忌700ml"
                        />
                      </div>
                      <p>正品40度杰克丹尼威士忌700ml 进口洋酒威士忌 夜店鸡尾酒</p>
                      <div className="gjw_tabBox_price">￥138</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="百龄坛 12年威士忌 700ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/263.jpg" />
                      </div>
                      <p>百龄坛 12年威士忌 700ml</p>
                      <div className="gjw_tabBox_price">￥199</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="马谛氏 尊者 苏格兰威士忌 礼盒装 700ml+200ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/264.jpg" />
                      </div>
                      <p>马谛氏 尊者 苏格兰威士忌 礼盒装 700ml+200ml</p>
                      <div className="gjw_tabBox_price">￥259</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="麦卡伦 12年单一纯麦威士忌 700ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/265.jpg" />
                      </div>
                      <p>麦卡伦 12年单一纯麦威士忌 700ml</p>
                      <div className="gjw_tabBox_price">￥339</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="苏格兰 第林可10年单一麦芽 威士忌 700ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/266.jpg"
                          title="苏格兰 第林可10年单一麦芽 威士忌 700ml"
                        />
                      </div>
                      <p>苏格兰 第林可10年单一麦芽 威士忌 700ml</p>
                      <div className="gjw_tabBox_price">￥499</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="美国 哈拿 金酒 750ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/267.jpg" />
                      </div>
                      <p>美国 哈拿 金酒 750ml</p>
                      <div className="gjw_tabBox_price">￥238</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="古巴 哈瓦那俱乐部 7年朗姆酒 750ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/268.jpg" title="古巴 哈瓦那俱乐部 7年朗姆酒 750ml" />
                      </div>
                      <p>古巴 哈瓦那俱乐部 7年朗姆酒 750ml</p>
                      <div className="gjw_tabBox_price">￥208</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="格兰 苏格兰威士忌 700ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/269.jpg" />
                      </div>
                      <p>格兰 苏格兰威士忌 700ml</p>
                      <div className="gjw_tabBox_price">￥79</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="芝华士 18年威士忌 小酒版 50ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/270.jpg" />
                      </div>
                      <p>芝华士 18年威士忌 小酒版 50ml</p>
                      <div className="gjw_tabBox_price">￥46</div>
                    </LinkA>
                  </li>
                </ul>
              </div>
              {/* <!-- 第四屏 -->  */}
              <div className="gjw_tabBox fl vv">
                <ul className="gjw_tabBox_proList">
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="绝对 原味 伏特加 750ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/271.jpg" title="绝对 原味 伏特加 750ml" />
                      </div>
                      <p>绝对 原味 伏特加 750ml</p>
                      <div className="gjw_tabBox_price">￥109</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="绝对 柠檬味 伏特加 750ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/272.jpg" title="绝对 柠檬味 伏特加 750ml" />
                      </div>
                      <p>绝对 柠檬味 伏特加 750ml</p>
                      <div className="gjw_tabBox_price">￥89</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="绝对 覆盆莓味 伏特加 750ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/273.jpg" title="." alt="." />
                      </div>
                      <p>绝对 覆盆莓味 伏特加 750ml</p>
                      <div className="gjw_tabBox_price">￥89</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="维波罗瓦 伏特加 小酒版 50ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/274.jpg" title="维波罗瓦 伏特加 小酒版 50ml" />
                      </div>
                      <p>维波罗瓦 伏特加 小酒版 50ml</p>
                      <div className="gjw_tabBox_price">￥11</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="法国 皇太子 伏特加 700ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/275.jpg" />
                      </div>
                      <p>法国 皇太子 伏特加 700ml</p>
                      <div className="gjw_tabBox_price">￥59</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="雪树 伏特加 700ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/276.jpg" title="雪树 伏特加 700ml" />
                      </div>
                      <p>雪树 伏特加 700ml</p>
                      <div className="gjw_tabBox_price">￥338</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="美国 深蓝牌 伏特加 柑橘味 750ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/277.jpg" />
                      </div>
                      <p>美国 深蓝牌 伏特加 柑橘味 750ml</p>
                      <div className="gjw_tabBox_price">￥76</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="美国 深蓝牌 伏特加 莓子味 750ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/278.jpg" title="." alt="." />
                      </div>
                      <p>美国 深蓝牌 伏特加 莓子味 750ml</p>
                      <div className="gjw_tabBox_price">￥75</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="维波罗瓦 精致伏特加 700ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/279.jpg"
                          title="维波罗瓦 精致伏特加 700ml"
                          alt="维波罗瓦 精致伏特加 700ml"
                        />
                      </div>
                      <p>维波罗瓦 精致伏特加 700ml</p>
                      <div className="gjw_tabBox_price">￥359</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="绝对 苹果梨味 伏特加 700ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/280.jpg" title="." alt="." />
                      </div>
                      <p>绝对 苹果梨味 伏特加 700ml</p>
                      <div className="gjw_tabBox_price">￥137</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="绝对 伏特加 小酒版 50ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/281.jpg" title="绝对 伏特加 小酒版 50ml" />
                      </div>
                      <p>绝对 伏特加 小酒版 50ml</p>
                      <div className="gjw_tabBox_price">￥24</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="苏连 红牌 伏特加 750ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/282.jpg" />
                      </div>
                      <p>苏连 红牌 伏特加 750ml</p>
                      <div className="gjw_tabBox_price">￥58</div>
                    </LinkA>
                  </li>
                </ul>
              </div>
              {/* <!-- 第五屏 --> */}
              <div className="gjw_tabBox fl vv">
                <ul className="gjw_tabBox_proList">
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="墨西哥 甘露咖啡力娇酒 700ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/283.jpg" title="墨西哥 甘露咖啡力娇酒 700ml" />
                      </div>
                      <p>墨西哥 甘露咖啡力娇酒 700ml</p>
                      <div className="gjw_tabBox_price">￥79</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="必得利 石榴汁 力娇酒 700ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/284.jpg" title="必得利 石榴汁 力娇酒 700ml" />
                      </div>
                      <p>必得利 石榴汁 力娇酒 700ml</p>
                      <div className="gjw_tabBox_price">￥69</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="波士 白薄荷 力娇酒 700ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/285.jpg" title="波士 白薄荷 力娇酒 700ml" />
                      </div>
                      <p>波士 白薄荷 力娇酒 700ml</p>
                      <div className="gjw_tabBox_price">￥79</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="波士 草莓味 力娇酒 700ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/286.jpg" />
                      </div>
                      <p>波士 草莓味 力娇酒 700ml</p>
                      <div className="gjw_tabBox_price">￥119</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="波士 黄梅 力娇酒 700ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/287.jpg" />
                      </div>
                      <p>波士 黄梅 力娇酒 700ml</p>
                      <div className="gjw_tabBox_price">￥79</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="波士 荔枝 力娇酒 700ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/288.jpg" />
                      </div>
                      <p>波士 荔枝 力娇酒 700ml</p>
                      <div className="gjw_tabBox_price">￥79</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="美国 哈拿 金酒 750ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/289.jpg" title="·" alt="·" />
                      </div>
                      <p>美国 哈拿 金酒 750ml</p>
                      <div className="gjw_tabBox_price">￥238</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="波士 苹果 力娇酒 700ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/290.jpg" title="波士 苹果 力娇酒 700ml" />
                      </div>
                      <p>波士 苹果 力娇酒 700ml</p>
                      <div className="gjw_tabBox_price">￥79</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="波士 椰子 力娇酒 700ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/291.jpg" />
                      </div>
                      <p>波士 椰子 力娇酒 700ml</p>
                      <div className="gjw_tabBox_price">￥79</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="波士 樱桃白兰地 力娇酒 700ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/292.jpg" />
                      </div>
                      <p>波士 樱桃白兰地 力娇酒 700ml</p>
                      <div className="gjw_tabBox_price">￥79</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="荷兰 迪凯堡 黑莓 力娇酒 700ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/293.jpg" title="荷兰 迪凯堡 黑莓 力娇酒 700ml" />
                      </div>
                      <p>荷兰 迪凯堡 黑莓 力娇酒 700ml</p>
                      <div className="gjw_tabBox_price">￥79</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="葫芦 绿薄荷 力娇酒 700ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/294.jpg" title="." alt="." />
                      </div>
                      <p>葫芦 绿薄荷 力娇酒 700ml</p>
                      <div className="gjw_tabBox_price">￥79</div>
                    </LinkA>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        {/* <!--4黄酒养生酒-->        */}
        <div className="floor4 gjw_tab floor" id="floor4">
          <div className="floor_t">
            <h2 className="fl">
              <i>4F</i>黄酒·养生酒·啤酒·茶叶馆
            </h2>
            <ul className="gjw_tabItemBox fr">
              <li className="gjw_tabItem nb active">
                <LinkA>巨惠推荐</LinkA>
                <span />
              </li>
              <li className="gjw_tabItem nb">
                <LinkA>黄酒系列</LinkA>
                <span />
              </li>
              <li className="gjw_tabItem nb">
                <LinkA>养生酒啤酒</LinkA>
                <span />
              </li>
              <li className="gjw_tabItem nb">
                <LinkA>茶叶</LinkA>
                <span />
              </li>
              <li className="gjw_tabItem">
                <LinkA>更多</LinkA>
              </li>
            </ul>
          </div>
          <div className="gjw_tab_c floor_container">
            <div className="gjw_tabSide fl">
              <LinkA className="gjw_tabSideBanner shine">
                <Image src="images/295.jpg" style={{ display: 'block' }} />
              </LinkA>
              <ul className="gjw_tabSideNav">
                <li>
                  <LinkA>古越龙山</LinkA>
                </li>
                <li>
                  <LinkA>女儿红 </LinkA>
                </li>
                <li>
                  <LinkA>啤酒专区 </LinkA>
                </li>
                <li>
                  <LinkA>养生酒</LinkA>
                </li>
                <li>
                  <LinkA>黄酒专区</LinkA>
                </li>
                <li>
                  <LinkA>茶叶专区</LinkA>
                </li>
                <li>
                  <LinkA>酒杯专区</LinkA>
                </li>
                <li>
                  <LinkA>劲酒专区 </LinkA>
                </li>
              </ul>
              <LinkA className="tab_sideAdLink shine">
                <Image src="images/296.jpg" style={{ display: 'inline' }} className="tab_sideAd" />
              </LinkA>
              <LinkA className="tab_sideAdLink2 shine">
                <Image src="images/297.jpg" style={{ display: 'inline' }} className="tab_sideAd" />
              </LinkA>
            </div>
            <div className="gjw_tabBigBox fl">
              {/* <!--第一屏s--> */}
              <div className="gjw_tabBox fl xx" style={{ display: 'block' }}>
                <div className="gjw_slideBox fl">
                  <div
                    className="tempWrap"
                    style={{ overflow: 'hidden', ' position': 'relative', ' width': '440px' }}
                  >
                    <ul
                      style={{
                        width: ' 2200px',
                        ' position': ' relative',
                        ' overflow': ' hidden',
                        ' padding': ' 0px',
                        ' margin': ' 0px',
                      }}
                      className="gjw_slideBody"
                      id="gjw3"
                    >
                      <li style={{ float: 'left', width: '440px' }} className="clone">
                        <LinkA>
                          <Image
                            style={{ width: '440px', height: ' 237px' }}
                            src="images/298.jpg"
                            className="notloadlazy"
                            height="237px"
                            width="435px"
                          />
                        </LinkA>
                      </li>
                      <li style={{ float: 'left', width: '440px' }}>
                        <LinkA>
                          <Image
                            style={{ width: '440px', height: ' 237px' }}
                            src="images/299.jpg"
                            className="notloadlazy"
                            alt=""
                            height="237px"
                            width="435px"
                          />
                        </LinkA>
                      </li>
                      <li style={{ float: 'left', width: '440px' }}>
                        <LinkA>
                          <Image
                            style={{ width: '440px', height: ' 237px' }}
                            src="images/300.jpg"
                            className="notloadlazy"
                            height="237px"
                            width="435px"
                          />
                        </LinkA>
                      </li>
                    </ul>
                  </div>
                  <div className="gjw_slideBtn">
                    <LinkA className="gjw_slidePrev" id="prevBtn3">
                      &lt","
                    </LinkA>
                    <LinkA className="gjw_slideNext" id="nextBtn3">
                      &gt;
                    </LinkA>
                  </div>
                  <div className="gjw_slideNav" id="gjwList3">
                    <LinkA className="active" />
                    <LinkA className="" />
                    <LinkA className="" />
                  </div>
                </div>
                <div className="fr gjw_adBox_xs">
                  <LinkA>
                    <Image src="images/303.jpg" style={{ display: 'inline' }} />
                  </LinkA>
                  <LinkA>
                    <Image src="images/304.jpg" style={{ display: 'inline' }} />
                  </LinkA>
                </div>
                <ul className="gjw_adBox_big clear">
                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/305.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>
                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/306.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>

                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/307.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>

                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/308.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>

                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/309.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>

                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/310.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>

                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/311.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>
                  <li>
                    <div>
                      <LinkA>
                        <Image src="images/312.jpg" style={{ display: 'inline' }} />
                      </LinkA>
                    </div>
                  </li>
                </ul>
              </div>
              {/* <!--第二屏--> */}
              <div className="gjw_tabBox fl xx">
                <ul className="gjw_tabBox_proList">
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="会稽山 八年红瓷花雕 500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/313.jpg" title="会稽山 八年红瓷花雕 500ml" />
                      </div>
                      <p>会稽山 八年红瓷花雕 500ml</p>
                      <div className="gjw_tabBox_price">￥45</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="25度 那楼淮山 营养酒 500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/314.jpg" title="." alt="." />
                      </div>
                      <p>25度 那楼淮山 营养酒 500ml</p>
                      <div className="gjw_tabBox_price">￥88</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="13.5度 玖农 井冈山 红米酒（8年陈酿） 500ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/315.jpg" />
                      </div>
                      <p>13.5度 玖农 井冈山 红米酒（8年陈酿） 500ml</p>
                      <div className="gjw_tabBox_price">￥58</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="女儿红 十二年陈酿（牡丹瓶） 500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/316.jpg" />
                      </div>
                      <p>女儿红 十二年陈酿（牡丹瓶） 500ml</p>
                      <div className="gjw_tabBox_price">￥99</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="42度 竹叶青 精酿盒 500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/317.jpg" />
                      </div>
                      <p>42度 竹叶青 精酿盒 500ml</p>
                      <div className="gjw_tabBox_price">￥199</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="会稽山 18年 500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/318.jpg" />
                      </div>
                      <p>会稽山 18年 500ml</p>
                      <div className="gjw_tabBox_price">￥195</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="35度 中国 劲酒 礼盒装 600ml*2">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/319.jpg" />
                      </div>
                      <p>35度 中国 劲酒 礼盒装 600ml*2</p>
                      <div className="gjw_tabBox_price">￥108</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="石库门 上海老酒（新黑标）500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/320.jpg" />
                      </div>
                      <p>石库门 上海老酒（新黑标）500ml</p>
                      <div className="gjw_tabBox_price">￥29</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="石库门 红标 500ml *12">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/321.jpg" />
                      </div>
                      <p>石库门 红标 500ml *12</p>
                      <div className="gjw_tabBox_price">￥199</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="钱塘人家 六年陈酿 500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/322.jpg" title="钱塘人家 六年陈酿 500ml" />
                      </div>
                      <p>钱塘人家 六年陈酿 500ml</p>
                      <div className="gjw_tabBox_price">￥22</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="沙洲优黄 小红坛 480ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/323.jpg" />
                      </div>
                      <p>沙洲优黄 小红坛 480ml</p>
                      <div className="gjw_tabBox_price">￥39</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="绍兴师爷 五年陈 白色紫砂 500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/324.jpg" title="绍兴师爷 五年陈 白色紫砂 500ml" />
                      </div>
                      <p>绍兴师爷 五年陈 白色紫砂 500ml</p>
                      <div className="gjw_tabBox_price">￥49</div>
                    </LinkA>
                  </li>
                </ul>
              </div>
              {/* <!-- 第三屏 --> */}
              <div className="gjw_tabBox fl xx">
                <ul className="gjw_tabBox_proList">
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="古越龙山 喜愿黄酒 428ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/325.jpg" />
                      </div>
                      <p>古越龙山 喜愿黄酒 428ml</p>
                      <div className="gjw_tabBox_price">￥59</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="蓝带 领航者风范 纪念版 1L">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/326.jpg" title="蓝带 领航者风范 纪念版 1L" />
                      </div>
                      <p>蓝带 领航者风范 纪念版 1L</p>
                      <div className="gjw_tabBox_price">￥98</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="45度 竹叶青 牧童 475ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/327.jpg" />
                      </div>
                      <p>45度 竹叶青 牧童 475ml</p>
                      <div className="gjw_tabBox_price">￥49</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="古越龙山 库藏15年花雕 500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/328.jpg" />
                      </div>
                      <p>古越龙山 库藏15年花雕 500ml</p>
                      <div className="gjw_tabBox_price">￥188</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="13.5度 玖农 井冈山 红米酒（8年陈酿） 500ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/329.jpg" title="." alt="." />
                      </div>
                      <p>13.5度 玖农 井冈山 红米酒（8年陈酿） 500ml</p>
                      <div className="gjw_tabBox_price">￥58</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="钱塘人家 十八年陈酿 500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/330.jpg"
                          title="钱塘人家 十八年陈酿 500ml"
                          alt="钱塘人家 十八年陈酿 500ml"
                        />
                      </div>
                      <p>钱塘人家 十八年陈酿 500ml</p>
                      <div className="gjw_tabBox_price">￥109</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="乌毡帽 浦江之源 480ml*6">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/331.jpg" />
                      </div>
                      <p>乌毡帽 浦江之源 480ml*6</p>
                      <div className="gjw_tabBox_price">￥118</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="任性 黑金版（小米金酒） 300ml+任性 白金版（小米金酒） 300ml"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/332.jpg" />
                      </div>
                      <p>任性 黑金版（小米金酒） 300ml+任性 白金版（小米金酒） 300ml</p>
                      <div className="gjw_tabBox_price">￥128</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="江南 女儿红 一线牵（黄酒） 500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/333.jpg" />
                      </div>
                      <p>江南 女儿红 一线牵（黄酒） 500ml</p>
                      <div className="gjw_tabBox_price">￥29</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="25度 那楼淮山 营养酒 500ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/334.jpg" />
                      </div>
                      <p>25度 那楼淮山 营养酒 500ml</p>
                      <div className="gjw_tabBox_price">￥88</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="12度 汉糯 红标米酒 350ml">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/335.jpg" />
                      </div>
                      <p>12度 汉糯 红标米酒 350ml</p>
                      <div className="gjw_tabBox_price">￥19.8</div>
                    </LinkA>
                  </li>
                </ul>
              </div>
              {/* <!-- 第四屏 --> */}
              <div className="gjw_tabBox fl xx">
                <ul className="gjw_tabBox_proList">
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="天心峰 大红袍乌龙茶蓝岩蓝色礼盒150g*2盒"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/336.jpg"
                          title="天心峰 大红袍乌龙茶蓝岩蓝色礼盒150g*2盒"
                        />
                      </div>
                      <p>天心峰 大红袍乌龙茶蓝岩蓝色礼盒150g*2盒</p>
                      <div className="gjw_tabBox_price">￥199</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="天心峰儒禧圆罐正山小种红茶 100g">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/337.jpg" title="天心峰儒禧圆罐正山小种红茶 100g" />
                      </div>
                      <p>天心峰儒禧圆罐正山小种红茶 100g</p>
                      <div className="gjw_tabBox_price">￥39</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="金骏眉红茶 武夷山桐木关金俊眉175g*2罐"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/338.jpg" />
                      </div>
                      <p>金骏眉红茶 武夷山桐木关金俊眉175g*2罐</p>
                      <div className="gjw_tabBox_price">￥119</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="九龙亭 安溪铁观音茶叶 清香型茶礼礼盒250g*2盒"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/339.jpg" />
                      </div>
                      <p>九龙亭 安溪铁观音茶叶 清香型茶礼礼盒250g*2盒</p>
                      <div className="gjw_tabBox_price">￥99</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA className="gjw_tabBox_proBox" title="天心峰真格大红袍木质礼盒 200g">
                      <div className="gjw_tabBox_imgBox">
                        <Image src="images/340.jpg" title="天心峰真格大红袍木质礼盒 200g" />
                      </div>
                      <p>天心峰真格大红袍木质礼盒 200g</p>
                      <div className="gjw_tabBox_price">￥388</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="天心峰 正山小种武夷山红茶 浓香型金骏眉 100g"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/341.jpg"
                          title="天心峰 正山小种武夷山红茶 浓香型金骏眉 100g"
                        />
                      </div>
                      <p>天心峰 正山小种武夷山红茶 浓香型金骏眉 100g</p>
                      <div className="gjw_tabBox_price">￥59</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="九龙亭 安溪铁观音茶叶 清香型新茶 双罐装250g*2盒"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/342.jpg"
                          title="九龙亭 安溪铁观音茶叶 清香型新茶 双罐装250g*2盒"
                        />
                      </div>
                      <p>九龙亭 安溪铁观音茶叶 清香型新茶 双罐装250g*2盒</p>
                      <div className="gjw_tabBox_price">￥99</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="九龙亭 安溪铁观音茶叶 清香型乌龙茶250g*2盒"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/343.jpg"
                          title="九龙亭 安溪铁观音茶叶 清香型乌龙茶250g*2盒"
                        />
                      </div>
                      <p>九龙亭 安溪铁观音茶叶 清香型乌龙茶250g*2盒</p>
                      <div className="gjw_tabBox_price">￥128</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="天心峰 女王之选 正山小种 武夷山茶叶礼盒120g*2盒"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/344.jpg"
                          title="天心峰 女王之选 正山小种 武夷山茶叶礼盒120g*2盒"
                        />
                      </div>
                      <p>天心峰 女王之选 正山小种 武夷山茶叶礼盒120g*2盒</p>
                      <div className="gjw_tabBox_price">￥219</div>
                    </LinkA>
                  </li>
                  <li>
                    <LinkA
                      className="gjw_tabBox_proBox"
                      title="天心峰 金骏眉武夷山红茶 正山小种礼盒120g*2盒"
                    >
                      <div className="gjw_tabBox_imgBox">
                        <Image
                          src="images/345.jpg"
                          title="天心峰 金骏眉武夷山红茶 正山小种礼盒120g*2盒"
                        />
                      </div>
                      <p>天心峰 金骏眉武夷山红茶 正山小种礼盒120g*2盒</p>
                      <div className="gjw_tabBox_price">￥199</div>
                    </LinkA>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        {/* <!-- 5楼高端收藏--> */}
        <div className="floor5 floor" id="floor5">
          <div className="floor_t">
            <h2>
              <i>5F</i>高端收藏
            </h2>
          </div>
          <div className="floor_container floor_max">
            <LinkA style={{ display: 'inline-block' }}>
              <Image src="images/346.jpg" alt="" />
            </LinkA>
            <LinkA style={{ display: 'inline-block' }}>
              <Image src="images/347.jpg" alt="" />
            </LinkA>
            <LinkA style={{ display: 'inline-block' }}>
              <Image src="images/348.jpg" alt="" />
            </LinkA>
            <LinkA style={{ display: 'inline-block' }}>
              <Image src="images/349.jpg" alt="" />
            </LinkA>
            <LinkA style={{ display: 'inline-block' }}>
              <Image src="images/350.jpg" alt="" />
            </LinkA>
          </div>
        </div>
        {/* <!-- 三途广告栏 --> */}
        <div className="advert">
          <div className="advert_img">
            <LinkA style={{ display: 'inline-block' }}>
              <Image src="images/351.jpg" alt="" />
            </LinkA>
            <LinkA style={{ display: 'inline-block' }}>
              <Image src="images/352.jpg" alt="" />
            </LinkA>
            <LinkA style={{ display: 'inline-block' }}>
              <Image src="images/353.jpg" alt="" />
            </LinkA>
          </div>
        </div>
        {/* <!-- 6楼品牌馆 --> */}
        <div className=" floor6 floor" id="floor6">
          <div className="floor_t">
            <h2 className="fl">
              <i>6F</i>品牌馆
            </h2>
            <h2 className="fr" style={{ ' float': ' right' }}>
              <LinkA>查看全部品牌</LinkA>
            </h2>
          </div>
          <div className="floor_container" style={{ height: 'auto' }}>
            <div className="brandBox">
              <LinkA>
                <div className="v6_brandBox_imgBox">
                  <Image src="images/354.jpg" style={{ display: 'block' }} />
                </div>
              </LinkA>
              <LinkA>
                <div className="v6_brandBox_imgBox">
                  <Image src="images/355.jpg" style={{ display: 'block' }} />
                </div>
              </LinkA>
              <LinkA>
                <div className="v6_brandBox_imgBox">
                  <Image src="images/356.jpg" style={{ display: 'block' }} />
                </div>
              </LinkA>
              <LinkA>
                <div className="v6_brandBox_imgBox">
                  <Image src="images/357.jpg" style={{ display: 'block' }} />
                </div>
              </LinkA>
              <LinkA>
                <div className="v6_brandBox_imgBox">
                  <Image src="images/358.jpg" style={{ display: 'block' }} />
                </div>
              </LinkA>
              <LinkA>
                <div className="v6_brandBox_imgBox">
                  <Image src="images/359.jpg" style={{ display: 'block' }} />
                </div>
              </LinkA>
              <LinkA>
                <div className="v6_brandBox_imgBox">
                  <Image src="images/360.jpg" style={{ display: 'block' }} />
                </div>
              </LinkA>
              <LinkA>
                <div className="v6_brandBox_imgBox">
                  <Image src="images/361.jpg" style={{ display: 'block' }} />
                </div>
              </LinkA>
              <LinkA>
                <div className="v6_brandBox_imgBox">
                  <Image src="images/362.jpg" style={{ display: 'block' }} />
                </div>
              </LinkA>
              <LinkA>
                <div className="v6_brandBox_imgBox">
                  <Image src="images/363.jpg" style={{ display: 'block' }} />
                </div>
              </LinkA>
              <LinkA>
                <div className="v6_brandBox_imgBox">
                  <Image src="images/364.jpg" style={{ display: 'block' }} />
                </div>
              </LinkA>
              <LinkA>
                <div className="v6_brandBox_imgBox">
                  <Image src="images/365.jpg" style={{ display: 'block' }} />
                </div>
              </LinkA>
              <LinkA>
                <div className="v6_brandBox_imgBox">
                  <Image src="images/366.jpg" style={{ display: 'block' }} />
                </div>
              </LinkA>
              <LinkA>
                <div className="v6_brandBox_imgBox">
                  <Image src="images/367.jpg" style={{ display: 'block' }} />
                </div>
              </LinkA>
              <LinkA>
                <div className="v6_brandBox_imgBox">
                  <Image src="images/368.jpg" style={{ display: 'block' }} />
                </div>
              </LinkA>
            </div>
          </div>
        </div>
        {/* <!-- 楼层跳转 --> */}
        <ul style={{ display: 'none' }} className="floorNav" id="#floorNav">
          <li className="card_container">
            <LinkA href="#floor1" className="card active">
              <div className="front">1F</div>
              <div className="back">
                <span>
                  白酒
                  <br />
                  专馆
                </span>
              </div>
            </LinkA>
          </li>
          <li className="card_container">
            <LinkA href="#floor2" className="card">
              <div className="front">2F</div>
              <div className="back">
                <span>
                  葡萄
                  <br />
                  酒馆
                </span>
              </div>
            </LinkA>
          </li>
          <li className="card_container">
            <LinkA href="#floor3" className="card">
              <div className="front">3F</div>
              <div className="back">
                <span>洋酒</span>
              </div>
            </LinkA>
          </li>
          <li className="card_container">
            <LinkA href="#floor4" className="card">
              <div className="front">4F</div>
              <div className="back">
                <span>
                  养生
                  <br />
                  酒馆
                </span>
              </div>
            </LinkA>
          </li>
          <li className="card_container">
            <LinkA href="#floor5" className="card">
              <div className="front">5F</div>
              <div className="back">
                <span>
                  高端
                  <br />
                  收藏
                </span>
              </div>
            </LinkA>
          </li>
          <li className="card_container">
            <LinkA href="#floor6" className="card">
              <div className="front">6F</div>
              <div className="back">
                <span>
                  品牌
                  <br />
                  推荐
                </span>
              </div>
            </LinkA>
          </li>
          <li className="card_container">
            <LinkA href="#top" className="backTop" id="floatNav_backTop">
              <span>^</span>
            </LinkA>
          </li>
        </ul>
      </Fragment>
    );
  }
}

WHotel.propTypes = {};

export default WHotel;
