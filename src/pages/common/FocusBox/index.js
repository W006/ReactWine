import React, { PureComponent } from 'react';
import { Image, LinkA } from 'components';
import classNames from 'classnames';
import { random } from '../../../utils/util.num';

const datas = [
  { id: 1, title: '', img: 'images/21.jpg' },
  { id: 1, title: '', img: 'images/22.jpg' },
  { id: 1, title: '', img: 'images/23.jpg' },
  { id: 1, title: '', img: 'images/24.jpg' },
  { id: 1, title: '', img: 'images/25.jpg' },
  { id: 1, title: '', img: 'images/26.jpg' },
  { id: 1, title: '', img: 'images/27.jpg' },
];

class FocusBox extends PureComponent {
  timer;

  timeUnit = 1000;

  constructor(props) {
    super(props);
    this.state = {
      cur: 0,
      data: [],
    };
  }

  componentDidMount() {
    this.setState({ data: datas }, () => {
      this.autoPlay();
    });
  }

  handleClick = (_) => {
    if (this.timer) {
      clearTimeout(this.timer);
      this.timer = undefined;
    }

    this.setState({ cur: _ }, () => {
      this.autoPlay();
    });
  };

  // 自动播放
  autoPlay = () => {
    if (this.state.data.length > 1) {
      this.timer = setTimeout(() => {
        this.setState((state) => {
          const cur = state.cur + 1;
          if (cur > state.data.length - 1) {
            return { ...state, cur: 0 };
          }
          return { ...state, cur };
        });
        this.autoPlay();
      }, this.getRadomTime() * this.timeUnit);
    }
  };

  getRadomTime = () => {
    return random(3, 8);
  };

  render() {
    const { data, cur } = this.state;
    return (
      <div>
        {/* <!--大轮播图--> */}
        <div className="focusBox">
          <LinkA className="v6_floatAd">
            <div className="shine" style={{ width: '190px', height: '429px', overflow: 'hidden' }}>
              <Image src="images/20.jpg" />
            </div>
          </LinkA>
          <ul
            style={{ position: 'relative', ' width': ' 100%', height: '453px' }}
            className="focusBox_inner"
            id="focusBoxInner"
          >
            {data.map((i, idex) => {
              return (
                <li
                  className={classNames({ hidden: cur !== idex })}
                  style={{
                    backGround: ' rgb(34, 41, 158) none repeat scroll 0% 0%',
                    ' zIndex': '8',
                    ' position': ' absolute',
                    ' width': ' 100%',
                    ' left': '0px',
                    ' top': ' 0px',
                  }}
                >
                  <div className="mid_box">
                    <LinkA
                      href="javascript"
                      style={{
                        background: `url(${i.img}) no-repeat center center`,
                        height: '100%',
                      }}
                    />
                  </div>
                </li>
              );
            })}
          </ul>
          <div
            className="focusBox_titCell"
            style={{ zIndex: ' 101', ' marginLeft': '-119px' }}
            id="focusBoxTitCell"
          >
            {data.map((i, idex) => {
              return (
                <span
                  onClick={() => this.handleClick(idex)}
                  className={classNames('tst', { active: cur == idex })}
                >
                  {idex + 1}
                </span>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}

FocusBox.propTypes = {};

export default FocusBox;
