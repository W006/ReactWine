import React, { PureComponent } from 'react';
import { Image, LinkA } from 'components';

class Hot extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    return (
      <div className="pag_img">
        <h3>热门品牌</h3>
        <div className="brandGroup">
          <LinkA>
            <span>洋河</span>
            <Image src="images/471.jpg" style={{ display: 'inline' }} height="150" width="230" />
          </LinkA>
          <LinkA>
            <span>澳洲奔富</span>
            <Image src="images/472.jpg" style={{ display: 'inline' }} height="150" width="230" />
          </LinkA>
          <LinkA>
            <span>汾酒</span>
            <Image src="images/473.jpg" style={{ display: 'inline' }} height="150" width="230" />
          </LinkA>
          <LinkA>
            <span>郎酒</span>
            <Image src="images/474.jpg" style={{ display: 'inline' }} height="150" width="230" />
          </LinkA>
          <LinkA style={{ marginRight: '0px' }}>
            <span>皇家路易</span>
            <Image src="images/475.jpg" style={{ display: 'inline' }} height="150" width="230" />
          </LinkA>
        </div>
        <div id="brandsDiv">
          <ul className="img_box_main">
            <li style={{ display: 'none' }}>
              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/525.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>茅台</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/526.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>五粮液</span>
                    </LinkA>
                  </div>
                </div>
              </div>
              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/527.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>剑南春</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div style={{ top: '-1px' }} className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/528.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>水井坊</span>
                    </LinkA>
                  </div>
                </div>
              </div>
              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/529.gif"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>汾酒</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/530.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>洋河</span>
                    </LinkA>
                  </div>
                </div>
              </div>
              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/531.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>珍酒</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/532.gif"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>泸州老窖</span>
                    </LinkA>
                  </div>
                </div>
              </div>

              <div style={{ marginRight: '0px' }} className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/533.gif"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>郎酒</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/534.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>古井贡酒</span>
                    </LinkA>
                  </div>
                </div>
              </div>

              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/535.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>西凤</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/536.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>董酒</span>
                    </LinkA>
                  </div>
                </div>
              </div>

              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div style={{ top: '-1px' }} className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/537.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>酒鬼</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div style={{ top: '-1px' }} className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/538.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>文君酒</span>
                    </LinkA>
                  </div>
                </div>
              </div>
              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div style={{ top: '-1px' }} className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/539.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>红星</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/540.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>牛栏山</span>
                    </LinkA>
                  </div>
                </div>
              </div>

              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/541.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>四特</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/542.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>沱牌</span>
                    </LinkA>
                  </div>
                </div>
              </div>
              <div style={{ marginRight: '0px' }} className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/543.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>口子窖</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/544.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>国台</span>
                    </LinkA>
                  </div>
                </div>
              </div>

              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div style={{ top: '-1px' }} className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/545.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>习酒</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/546.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>衡水老白干</span>
                    </LinkA>
                  </div>
                </div>
              </div>
              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div style={{ top: '-1px' }} className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/547.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>舍得</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div style={{ top: '-1px' }} className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/548.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>种子酒</span>
                    </LinkA>
                  </div>
                </div>
              </div>

              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div style={{ top: '-1px' }} className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/549.png"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>伊力牌</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/550.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>杜康</span>
                    </LinkA>
                  </div>
                </div>
              </div>

              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/551.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>枝江</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/552.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>白云边</span>
                    </LinkA>
                  </div>
                </div>
              </div>

              <div style={{ marginRight: '0px' }} className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/553.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>孔府家</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/554.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>扳倒井</span>
                    </LinkA>
                  </div>
                </div>
              </div>
              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div style={{ top: '-1px' }} className="imgDiv">
                    <LinkA style={{ borderBottom: ' medium none' }}>
                      <Image
                        src="images/555.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA style={{ borderBottom: ' medium none' }} href="">
                      <span>今世缘</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div style={{ top: '-1px' }} className="imgDiv">
                    <LinkA style={{ borderBottom: ' medium none' }}>
                      <Image
                        src="images/556.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA style={{ borderBottom: ' medium none' }} href="">
                      <span>金六福</span>
                    </LinkA>
                  </div>
                </div>
              </div>

              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div style={{ top: '-1px' }} className="imgDiv">
                    <LinkA style={{ borderBottom: ' medium none' }}>
                      <Image
                        src="images/557.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA style={{ borderBottom: ' medium none' }} href="">
                      <span>小糊涂仙</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div style={{ top: '-1px' }} className="imgDiv">
                    <LinkA style={{ borderBottom: ' medium none' }}>
                      <Image
                        src="images/557.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA style={{ borderBottom: ' medium none' }} href="">
                      <span>迎驾</span>
                    </LinkA>
                  </div>
                </div>
              </div>

              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div className="imgDiv">
                    <LinkA style={{ borderBottom: ' medium none' }}>
                      <Image
                        src="images/559.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA style={{ borderBottom: ' medium none' }} href="">
                      <span>稻花香</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div style={{ top: '-1px' }} className="imgDiv">
                    <LinkA style={{ borderBottom: ' medium none' }}>
                      <Image
                        src="images/560.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA style={{ borderBottom: ' medium none' }} href="">
                      <span>皖酒</span>
                    </LinkA>
                  </div>
                </div>
              </div>

              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div className="imgDiv">
                    <LinkA style={{ borderBottom: ' medium none' }}>
                      <Image
                        src="images/561.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA style={{ borderBottom: ' medium none' }} href="">
                      <span>金门高粱酒</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div className="imgDiv">
                    <LinkA style={{ borderBottom: ' medium none' }}>
                      <Image
                        src="images/562.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA style={{ borderBottom: ' medium none' }} href="">
                      <span>丰谷</span>
                    </LinkA>
                  </div>
                </div>
              </div>

              <div style={{ marginRight: '0px' }} className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div className="imgDiv">
                    <LinkA style={{ borderBottom: ' medium none' }}>
                      <Image
                        src="images/563.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA style={{ borderBottom: ' medium none' }} href="">
                      <span>黔台牌</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div className="imgDiv">
                    <LinkA style={{ borderBottom: ' medium none' }}>
                      <Image
                        src="images/564.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA style={{ borderBottom: ' medium none' }} href="">
                      <span>双沟</span>
                    </LinkA>
                  </div>
                </div>
              </div>
            </li>
            <li style={{ display: ' list-item', opacity: '0.924375' }}>
              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div className="imgDiv">
                    <LinkA>
                      <Image src="images/477.jpg" border="0" height="50" width="110" />
                    </LinkA>
                    <LinkA>
                      <span>黄鹤楼</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/476.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>浏阳河</span>
                    </LinkA>
                  </div>
                </div>
              </div>

              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/477.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>太白酒</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/478.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>高炉家</span>
                    </LinkA>
                  </div>
                </div>
              </div>
              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/479.png"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>苏酒</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div style={{ top: '-1px' }} className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/480.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>五星</span>
                    </LinkA>
                  </div>
                </div>
              </div>
              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/481.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>青酒</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/482.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>河套</span>
                    </LinkA>
                  </div>
                </div>
              </div>

              <div style={{ marginRight: '0px' }} className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/483.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA href="images">
                      <span>红楼梦</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/484.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>全兴</span>
                    </LinkA>
                  </div>
                </div>
              </div>
              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div style={{ top: '-1px' }} className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/485.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>人头马</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/486.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>芝华士</span>
                    </LinkA>
                  </div>
                </div>
              </div>

              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/487.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>轩尼诗</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/488.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>军酒</span>
                    </LinkA>
                  </div>
                </div>
              </div>

              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/489.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA href="images">
                      <span>酒中酒</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/500.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>高参</span>
                    </LinkA>
                  </div>
                </div>
              </div>

              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/501.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>景芝</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/524.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>百加得</span>
                    </LinkA>
                  </div>
                </div>
              </div>
              <div style={{ marginRight: '0px' }} className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/502.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>百龄坛</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/503.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>任意门</span>
                    </LinkA>
                  </div>
                </div>
              </div>
              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div style={{ top: '-1px' }} className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/504.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>尊尼获加</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/505.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>JHBROS</span>
                    </LinkA>
                  </div>
                </div>
              </div>

              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/506.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>深蓝</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/507.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>古越龙山</span>
                    </LinkA>
                  </div>
                </div>
              </div>
              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/508.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>塔牌</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div style={{ top: '-1px' }} className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/509.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>绍兴师爷</span>
                    </LinkA>
                  </div>
                </div>
              </div>
              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/510.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>金泸老窖</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/511.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>劲酒</span>
                    </LinkA>
                  </div>
                </div>
              </div>

              <div style={{ marginRight: '0px' }} className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/512.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>江津酒</span>{' '}
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div className="imgDiv">
                    <LinkA>
                      <Image
                        src="images/513.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA>
                      <span>四大美女</span>
                    </LinkA>
                  </div>
                </div>
              </div>

              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div style={{ top: '-1px' }} className="imgDiv">
                    <LinkA style={{ borderBottom: ' medium none' }}>
                      <Image
                        src="images/514.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA style={{ borderBottom: ' medium none' }} href="">
                      <span>小酒人生</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div style={{ top: '-1px' }} className="imgDiv">
                    <LinkA style={{ borderBottom: ' medium none' }}>
                      <Image
                        src="images/515.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA style={{ borderBottom: ' medium none' }} href="">
                      <span>八大怪</span>
                    </LinkA>
                  </div>
                </div>
              </div>

              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div style={{ top: '-1px' }} className="imgDiv">
                    <LinkA style={{ borderBottom: ' medium none' }}>
                      <Image
                        src="images/516.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA style={{ borderBottom: ' medium none' }} href="">
                      <span>燃點</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div style={{ top: '-1px' }} className="imgDiv">
                    <LinkA style={{ borderBottom: ' medium none' }}>
                      <Image
                        src="images/517.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA style={{ borderBottom: ' medium none' }} href="">
                      <span>皇家礼炮</span>
                    </LinkA>
                  </div>
                </div>
              </div>

              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div className="imgDiv">
                    <LinkA style={{ borderBottom: ' medium none' }}>
                      <Image
                        src="images/518.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA style={{ borderBottom: ' medium none' }} href="">
                      <span>马利宝</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div className="imgDiv">
                    <LinkA style={{ borderBottom: ' medium none' }}>
                      <Image
                        src="images/519.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA style={{ borderBottom: ' medium none' }} href="">
                      <span>必富达</span>
                    </LinkA>
                  </div>
                </div>
              </div>
              <div className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div style={{ top: '-1px' }} className="imgDiv">
                    <LinkA style={{ borderBottom: ' medium none' }}>
                      <Image
                        src="images/520.png"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA style={{ borderBottom: ' medium none' }} href="">
                      <span>奔富</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div className="imgDiv">
                    <LinkA style={{ borderBottom: ' medium none' }}>
                      <Image
                        src="images/521.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA style={{ borderBottom: ' medium none' }} href="">
                      <span>拉菲系列</span>
                    </LinkA>
                  </div>
                </div>
              </div>
              <div style={{ marginRight: '0px' }} className="twoBrand">
                <div className="imgDivP rightDashed">
                  <div className="imgDiv">
                    <LinkA style={{ borderBottom: ' medium none' }}>
                      <Image
                        src="images/522.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA style={{ borderBottom: ' medium none' }}>
                      <span>许愿树</span>
                    </LinkA>
                  </div>
                </div>
                <div className="imgDivP">
                  <div className="imgDiv">
                    <LinkA style={{ borderBottom: ' medium none' }}>
                      <Image
                        src="images/523.jpg"
                        style={{ display: 'inline' }}
                        border="0"
                        height="50"
                        width="110"
                      />
                    </LinkA>
                    <LinkA style={{ borderBottom: ' medium none' }}>
                      <span>天下福</span>
                    </LinkA>
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

Hot.propTypes = {};

export default Hot;
