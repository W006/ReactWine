import React, { PureComponent } from 'react';
import MItem from './MItem';
import './index.scss';

class SMenu extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        {
          title: '购酒直通车',
          isHot: true,
          more: true,
          children: [
            { id: 1, title: '大坛美酒', isHot: false },
            { id: 1, title: '高端收藏', isHot: false },
            { id: 1, title: '送礼推荐', isHot: false },
            { id: 1, title: '整箱促销', isHot: true },
            { id: 1, title: '封顶49元', isHot: false },
            { id: 1, title: '婚宴用酒', isHot: false },
          ],
        },
        {
          title: '白酒',
          isHot: true,
          more: false,
          children: [
            { id: 1, title: '茅台', isHot: false },
            { id: 1, title: '五粮液', isHot: false },
            { id: 1, title: '郎酒', isHot: false },
            { id: 1, title: '剑南春', isHot: true },
            { id: 1, title: '封顶49元', isHot: false },
            { id: 1, title: '婚宴用酒', isHot: false },
          ],
        },
      ],
    };
  }

  componentDidMount() {}

  render() {
    return (
      <div className="v6_navBar_cate dropBox fl" id="navBar_cate">
        <div className="navBar_dt">全部商品分类</div>
        {/* <!--分类导航s--> */}
        <div className="navBar_navTab cateMenu " style={{ display: 'block' }}>
          <ul className="navBar_navTabInner">
            {this.state.data.map((da) => {
              return <MItem record={da} />;
            })}
          </ul>
        </div>
      </div>
    );
  }
}

SMenu.propTypes = {};

export default SMenu;
