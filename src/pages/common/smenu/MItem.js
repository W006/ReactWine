import React, { PureComponent, Fragment } from 'react';
import { Image, LinkA } from 'components';
import { Icon } from 'antd';

const record = {
  title: '购酒直通车',
  isHot: true,
  more: true,
  children: [
    { id: 1, title: '大坛美酒', isHot: false },
    { id: 1, title: '高端收藏', isHot: false },
    { id: 1, title: '送礼推荐', isHot: false },
    { id: 1, title: '整箱促销', isHot: true },
    { id: 1, title: '封顶49元', isHot: false },
    { id: 1, title: '婚宴用酒', isHot: false },
  ],
};

const panelData = {
  title: '品牌',
  breakMenu: ['亏本清仓专区', '高端收藏酒', '陈年老酒专区', '特卖会'],
  hotBrands: [
    { id: 1, img: 'images/7.jpg', title: '' },
    { id: 2, img: 'images/7.jpg', title: '' },
  ],
  children: [
    {
      id: 1,
      title: '品牌',
      children: [
        { id: 1, title: '茅台' },
        { id: 2, title: '五粮液' },
        { id: 3, title: '剑南春' },
        { id: 4, title: '泸州老窖国窖1573' },
        { id: 5, title: '郎酒' },
        { id: 6, title: '洋河汾酒酒鬼' },
      ],
    },
    {
      id: 2,
      title: '香型',
      children: [
        { id: 1, title: '董香型' },
        { id: 2, title: '凤香型' },
        { id: 3, title: '馥郁香兼香型' },
        { id: 4, title: '酱香型金门香型' },
      ],
    },
    {
      id: 3,
      title: '产地',
      children: [
        { id: 1, title: '四川' },
        { id: 2, title: '山西' },
        { id: 3, title: '贵州' },
        { id: 4, title: '安徽' },
        { id: 5, title: '江苏' },
      ],
    },
  ],
};

const MMPanel = ({ record }) => {
  return (
    <Fragment>
      <div className="navBar_bTitle">
        <strong>{record.title}</strong>
      </div>
      <ul className="navBar_brandList">
        {record.children.map((data) => {
          return (
            <LinkA key={data.id}>
              <font color="#3f3f3f">{data.title}</font>
            </LinkA>
          );
        })}
      </ul>
    </Fragment>
  );
};

const MPanel = ({ showMore }) => {
  return (
    <div className={`navBar_subNav ${showMore ? 'show' : 'hidden'}`}>
      <div className="navBar_leftSubNav fl">
        <div className="navBar_leftInner">
          <div className="navBar_chns">
            {panelData.breakMenu.map((ti) => {
              return (
                <LinkA>
                  {ti}
                  <i className="navBar_chnArrow">&gt;</i>
                </LinkA>
              );
            })}
          </div>
          {panelData.children.map((i) => {
            return (
              <div className="navBar_brands v6_clear">
                <MMPanel record={i} />
              </div>
            );
          })}
        </div>
      </div>
      <div className="navBar_rightSubNav fr">
        {panelData.hotBrands.map((i) => {
          return (
            <LinkA>
              <Image src={i.img} />
            </LinkA>
          );
        })}
      </div>
    </div>
  );
};

class SMenu extends PureComponent {
  timer;

  constructor(props) {
    super(props);
    this.state = {
      record: {},
      showMore: false,
    };
  }

  componentDidMount() {
    this.setState({ record: this.props.record });
    this.more = record.more;
  }

  handleMouseEnter = (_) => {
    if (this.more) {
      if (this.timer) {
        clearTimeout(this.timer);
        this.timer = undefined;
      }
      this.setState({ showMore: true });
    }
  };

  handleMouseLeave = (_) => {
    this.timer = setTimeout(_ => this.setState({ showMore: false }), 1000);
  };

  render() {
    const { record, showMore } = this.state;

    return (
      <li className="navBar_cateItem noSubNav">
        <div className="navBar_title">
          <strong className="fl cateItem_title">
            {record.title}
            {record.isHot ? <span className="navBar_tip">HOT</span> : null}
          </strong>
          {record.more ? (
            <span
              className="arrow fr"
              onMouseEnter={this.handleMouseEnter}
              onMouseLeave={this.handleMouseLeave}
            >
              <Icon type={showMore ? 'right' : 'down'} />
              <MPanel showMore={showMore} />
            </span>
          ) : null}
        </div>
        <div className="navBar_linkBox v6_clear">
          {record.children
            && record.children.map((i) => {
              return <LinkA className={i.isHot ? 'v6_highLight' : ''}>{i.title}</LinkA>;
            })}
        </div>
      </li>
    );
  }
}

SMenu.propTypes = {};

export default SMenu;
