/**
 * 三图广告组成
 */

import React from 'react';
import { Image, LinkA } from 'components';

export default () => {
  return (
    <div className="section">
      <div className="section_news">
        <LinkA>
          <Image src="images/52.jpg" style={{ display: 'inline' }} />
        </LinkA>
        <LinkA>
          <Image src="images/53.jpg" style={{ display: 'inline' }} />
        </LinkA>
        <LinkA>
          <Image src="images/54.jpg" style={{ display: 'inline' }} />
        </LinkA>
      </div>
    </div>
  );
};
