import React, { PureComponent, Fragment } from 'react';
import AdvertOne from './AdvertOne';
import AdvertTwo from './AdvertTwo';
import AdvertThree from './AdvertThree';
import AdvertFour from './AdvertFour';

export default class Advert extends PureComponent {
  render() {
    return (
      <Fragment>
        {/* <!--滑动广告--> */}
        <AdvertOne />
        {/* <!--购酒闪购--> */}
        <AdvertTwo />
        {/* <!--三图广告组成--> */}
        <AdvertThree />
        {/* <!--购酒推荐--> */}
        <AdvertFour />
      </Fragment>
    );
  }
}
