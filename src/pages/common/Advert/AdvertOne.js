/**
 * 滑动广告
 */

import React from 'react';
import { Image, LinkA } from 'components';

export default () => {
  return (
    <div className="slideAd">
      <div className="leftAd fl shine">
        <LinkA>
          <Image src="images/29.jpg" alt="" />
        </LinkA>
      </div>
      <div className="slideAd_banner fl">
        <div style={{ overflow: 'hidden', ' position': 'relative', width: '990px' }}>
          <input
            type="button"
            value="<"
            className="slideAd_prevBtn"
            id="btn1"
            style={{ border: 'none', outline: 'none' }}
          />
          <input
            type="button"
            value=">"
            className="slideAd_nextBtn"
            id="btn2"
            style={{ border: 'none', outline: 'none' }}
          />
          <ul
            style={{
              width: ' 3960px',
              position: 'relative',
              overflow: 'hidden',
              ' padding': '0px',
              margin: ' 0px',
              left: '-990px',
            }}
            id="ul1"
          >
            <li style={{ float: ' left', ' width': ' 990px' }} className="clone">
              <div className="slideAd_imgBox">
                <LinkA>
                  <Image src="images/30.gif" />
                </LinkA>
              </div>
              <div className="slideAd_imgBox">
                <LinkA>
                  <Image src="images/31.gif" />
                </LinkA>
              </div>
              <div className="slideAd_imgBox">
                <LinkA>
                  <Image src="images/32.gif" />
                </LinkA>
              </div>
              <div className="slideAd_imgBox">
                <LinkA>
                  <Image src="images/33.gif" />
                </LinkA>
              </div>
            </li>
            <li style={{ float: ' left', ' width': ' 990px' }}>
              <div className="slideAd_imgBox">
                <LinkA>
                  <Image src="images/34.gif" />
                </LinkA>
              </div>
              <div className="slideAd_imgBox">
                <LinkA>
                  <Image src="images/35.gif" />
                </LinkA>
              </div>
              <div className="slideAd_imgBox">
                <LinkA>
                  <Image src="images/36.gif" />
                </LinkA>
              </div>
              <div className="slideAd_imgBox">
                <LinkA>
                  <Image src="images/37.gif" />
                </LinkA>
              </div>
            </li>
            <li style={{ float: ' left', ' width': ' 990px' }}>
              <div className="slideAd_imgBox">
                <LinkA>
                  <Image src="images/38.gif" />
                </LinkA>
              </div>
              <div className="slideAd_imgBox">
                <LinkA>
                  <Image src="images/39.gif" />
                </LinkA>
              </div>
              <div className="slideAd_imgBox">
                <LinkA>
                  <Image src="images/40.gif" />
                </LinkA>
              </div>
              <div className="slideAd_imgBox">
                <LinkA>
                  <Image src="images/41.gif" />
                </LinkA>
              </div>
            </li>
            <li style={{ float: ' left', ' width': ' 990px' }} className="clone">
              <div className="slideAd_imgBox">
                <LinkA>
                  <Image src="images/42.gif" />
                </LinkA>
              </div>
              <div className="slideAd_imgBox">
                <LinkA>
                  <Image src="images/43.gif" />
                </LinkA>
              </div>
              <div className="slideAd_imgBox">
                <LinkA>
                  <Image src="images/44.gif" />
                </LinkA>
              </div>
              <div className="slideAd_imgBox">
                <LinkA>
                  <Image src="images/45.gif" />
                </LinkA>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};
