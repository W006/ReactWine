/**
 * 购酒推荐
 */

import React from 'react';
import { Image, LinkA } from 'components';

export default () => {
  return (
    <div className="buy_recommend">
      <div className="buy_recommend-box">
        <div className="buy_section_tit">
          <span>购酒推荐&nbsp;|&nbsp;</span>限时特惠
        </div>
        <div className="recommendBox">
          <div className="recommendBox_left shine">
            <LinkA>
              <Image src="images/55.jpg" style={{ display: 'inline' }} />
            </LinkA>
          </div>
          <ul className="recommendBox_center" id="oUl">
            <li className="aLi aLi1 move">
              <LinkA>
                <div className="rcBox_cate">
                  <h2 title="经典好酒 经典品质">经典好酒经典品质</h2>
                </div>
                <div className="rcBox_title mgt10 rcBox_cate1">
                  52度 五粮液 1995专卖店 经典...
                  <br />
                  <span className="rcBox_price">259元</span>
                </div>
                <Image
                  src="images/56.jpg"
                  style={{ display: 'inline' }}
                  title="52度 五粮液 1995专卖店 经典装 500ml"
                />
              </LinkA>
            </li>
            <li className="oLi aLi1 move">
              <LinkA>
                <div className="rcBox_cate">
                  <h2 title="高端酒质 绵甜爽净">高端酒质绵甜爽净</h2>
                </div>
                <div className="rcBox_title mgt10 rcBox_cate1">
                  52度 百年泸州老窖 窖龄30年 500ml
                  <br />
                  <span className="rcBox_price">169元</span>
                </div>
                <Image src="images/57.jpg" style={{ display: 'inline' }} />
              </LinkA>
            </li>
            <li className="aLi aLi1 move">
              <LinkA>
                <div className="rcBox_cate">
                  <h2 title="智慧人生 品味舍得">智慧人生品味舍得</h2>
                </div>
                <div className="rcBox_title mgt10 rcBox_cate1">
                  52度 舍得酒（新） 500ml
                  <br />
                  <span className="rcBox_price">366元</span>
                </div>
                <Image
                  src="images/58.jpg"
                  style={{ display: 'inline' }}
                  title="52度 舍得酒（新） 500ml"
                />
              </LinkA>
            </li>
            <li className="aLi move">
              <LinkA>
                <div className="rcBox_cate">
                  <h2 title="国酒品质 仁者智慧">国酒品质仁者智慧</h2>
                </div>
                <div className="rcBox_title mgt10 rcBox_cate1">
                  53度 茅台 仁酒 500ml
                  <br />
                  <span className="rcBox_price">119元</span>
                </div>
                <Image
                  src="images/59.jpg"
                  style={{ display: 'inline' }}
                  title="53度 茅台 仁酒 500ml"
                />
              </LinkA>
            </li>
            <li className="oLi move">
              <LinkA>
                <div className="rcBox_cate">
                  <h2 title="经典品质 热卖爆款">经典品质热卖爆款</h2>
                </div>
                <div className="rcBox_title mgt10 rcBox_cate1">
                  石库门 锦绣12年 500ml
                  <br />
                  <span className="rcBox_price">89元</span>
                </div>
                <Image src="images/60.jpg" style={{ display: 'inline' }} />
              </LinkA>
            </li>
            <li className="aLi move">
              <LinkA>
                <div className="rcBox_cate">
                  <h2 title="限时特卖 欲购从速">限时特卖欲购从速</h2>
                </div>
                <div className="rcBox_title mgt10 rcBox_cate1">
                  42度 白云边 15年 陈酿酒 500ml
                  <br />
                  <span className="rcBox_price">179元</span>
                </div>
                <Image
                  src="images/61.jpg"
                  style={{ display: 'inline' }}
                  title="42度 白云边 15年 陈酿酒 500ml"
                />
              </LinkA>
            </li>
          </ul>
          <div className="recommendBox_right shine">
            <LinkA>
              <Image src="images/62.jpg" style={{ display: 'inline' }} />
            </LinkA>
          </div>
        </div>
      </div>
    </div>
  );
};
