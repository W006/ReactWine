/**
 * <!--购酒闪购-->
 */

import React from 'react';
import { Image, LinkA } from 'components';

export default () => {
  return (
    <div className="buy_flash">
      <div className="buy_main">
        <div className="buy_left">
          <div className="buy_title_top">
            <span>购酒闪购&nbsp;|&nbsp;</span>每天10点/15点更新
          </div>
          <div className="buy_title_bottom">
            <ul>
              <li>
                <div>
                  <LinkA className="flash">
                    <Image src="images/46.jpg" alt="" />
                  </LinkA>
                </div>
                <div className="div1">
                  <LinkA className="a1">52度 金六福 特曲 (红铁盒)500ml*2</LinkA>
                  <strong>￥99</strong>
                  <div>
                    <span className="span1">剩余:</span>
                    <span className="span2">02</span>
                    <span className="span2">02</span>
                  </div>
                </div>
              </li>
              <li>
                <div>
                  <LinkA className="flash">
                    <Image src="images/47.jpg" alt="" />
                  </LinkA>
                </div>
                <div className="div1">
                  <LinkA className="a1"> 绝对 辣椒味 伏特加 750ml</LinkA>
                  <strong>￥89</strong>
                  <div>
                    <span className="span1">剩余:</span>
                    <span className="span2">02</span>
                    <span className="span2">02</span>
                  </div>
                </div>
              </li>
              <li>
                <div className="flash">
                  <LinkA className="flash">
                    <Image src="images/48.jpg" alt="" />
                  </LinkA>
                </div>
                <div className="div1">
                  <LinkA className="a1"> 德国 华尔兹蓝牌 白葡萄酒 750ml</LinkA>
                  <strong>￥39</strong>
                  <div>
                    <span className="span1">剩余：</span>
                    <span className="span2">02</span>
                    <span className="span2">02</span>
                  </div>
                </div>
              </li>
              <li>
                <div>
                  <LinkA className="flash">
                    <Image src="images/49.jpg" alt="" />
                  </LinkA>
                </div>
                <div className="div1">
                  <LinkA className="a1"> 52度 丰谷醇 红尊 500ml</LinkA>
                  <strong>￥29</strong>
                  <div>
                    <span className="span1">剩余：</span>
                    <span className="span2">02</span>
                    <span className="span2">02</span>
                  </div>
                </div>
              </li>
              <li className="last">
                <div>
                  <LinkA className="flash">
                    <Image src="images/50.jpg" alt="" />
                  </LinkA>
                </div>
                <div>
                  <LinkA className="a1"> 46度 红星 古钟二锅头 珍品（2008年产） 500ml</LinkA>
                  <strong>￥79</strong>
                  <div>
                    <span className="span1">剩余：</span>
                    <span className="span2">02</span>
                    <span className="span2">02</span>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
        <div className="buy_right">
          <div className="buy_right_top">
            <span>购酒快讯&nbsp;|&nbsp;</span>
            <LinkA>更多</LinkA>
          </div>
          <div className="buy_right_bottom">
            <LinkA>
              <b>【公告】</b>即日起在线支付立享99折{' '}
            </LinkA>
            <LinkA>
              <b>【公告】</b>重磅惊喜又一弹！年中促160...
            </LinkA>
            <LinkA>
              <b>【公告】</b>购酒网全球特惠 满599减100...
            </LinkA>
            <LinkA>
              <b>【公告】</b>会员福利大放送 好酒加倍...
            </LinkA>{' '}
            <LinkA>
              <b>【公告】</b>购酒网五月重磅推荐 红包任性...
            </LinkA>
            <LinkA className="Image">
              <Image src="images/51.jpg" />
            </LinkA>
          </div>
        </div>
      </div>
    </div>
  );
};
