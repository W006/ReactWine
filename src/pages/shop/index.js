import React, { PureComponent, Fragment } from 'react';
import { LinkA, Image } from 'components';
import { Link } from 'react-router-dom';
import styles from './index.less';

class Shop extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    return (
      <Fragment>
        <div className={styles.footer}>
          <div className={styles.footer_nav}>
            <ul className={styles.left}>
              <li>
                <LinkA href="user_login.html">登录</LinkA>&nbsp;
                <LinkA href="registration.html">注册</LinkA>
              </li>
            </ul>
            <ul className={styles.right}>
              <li>
                <LinkA>购物车2件</LinkA>
              </li>
              <li>
                <LinkA>我的订单</LinkA>
              </li>
              <li style={{ width: '79px', padding: '0', textAlign: 'center' }} className="li1">
                <LinkA>用户中心</LinkA>
                <div className={styles.right_main}>
                  <p className="active">
                    <LinkA>商品评论</LinkA>
                  </p>
                  <p>
                    <LinkA>收藏夹</LinkA>
                  </p>
                  <p>
                    <LinkA>我的电子卷</LinkA>
                  </p>
                </div>
              </li>
              <li>
                <LinkA>帮助</LinkA>
              </li>
              <li>
                <LinkA style={{ color: '#c40000' }}>提建议</LinkA>
              </li>
              <li>
                <LinkA style={{ color: '#c40000' }}>微博</LinkA>
              </li>
            </ul>
          </div>
        </div>

        <div className={styles.main}>
          <div className={styles.main_title}>
            <h1>我的购物车</h1>
            <div>
              <b>单笔订单满100元免运费（香港、澳门、台湾、钓鱼岛地区和拆分订单除外）</b>
            </div>
            <LinkA>
              <Image src="images/xb13.jpg" alt="" />
            </LinkA>
          </div>
          <dl className={styles.cart_box}>
            <dt>
              <span className={styles.cb_r1} style={{ textAlign: 'center' }}>
                商品
              </span>
              <span className={styles.cb_r2}>单价</span>
              <span className={styles.cb_r3}>优惠</span>
              <span className={styles.cb_r4}>数量</span>
              <span className={styles.cb_r5}>小计</span>
              <span className={styles.cb_r6}>操作</span>
              <div className="clear-both" />
            </dt>
            <dd id={styles.cart_goods}>
              <div id={styles.cart_goods_area}>
                <table className={styles.cart_goods_area}>
                  <tbody>
                    <tr>
                      <td className="cb_r11 zp_td">
                        <LinkA>
                          <Image src="images/456.jpg" width="50" height="50" />
                        </LinkA>
                      </td>
                      <td className="cb_r12 zp_td">
                        <LinkA className="c12">法国 拉菲传说（经典鳄鱼纹皮盒）750ml*2</LinkA>
                        <br />
                      </td>
                      <td className="cb_r2 zp_td" align="center">
                        ¥199
                      </td>
                      <td className="cb_r3 zp_td cf04" align="center">
                        -
                      </td>
                      <td className="cb_r4 zp_td" align="center">
                        <p className="num_line">
                          <span className="num_down" index="6515">
                            -
                          </span>
                          <input value="1" className="num_input" index="6515" type="text" />
                          <span className="num_up" index="6515">
                            +
                          </span>
                        </p>
                      </td>
                      <td className="cb_r5 zp_td" align="center">
                        <span>
                          ¥<b className="xp">199</b>
                        </span>
                      </td>
                      <td className="cb_r6 zp_td" align="center">
                        <span className="coll_opt">
                          <LinkA>收藏</LinkA>
                        </span>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <span className="del_opt">
                          <LinkA href="javascript:Del(6515)">删除</LinkA>
                        </span>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table className={styles.cart_goods_area}>
                  <tbody>
                    <tr>
                      <td className="cb_r11 zp_td">
                        <LinkA>
                          <Image src="images/457.jpg" width="50" height="50" />
                        </LinkA>
                      </td>
                      <td className="cb_r12 zp_td">
                        <LinkA className="c12">52度 五粮液 丙申猴年纪念酒（金瓶）375ml</LinkA>
                        <br />
                      </td>
                      <td className="cb_r2 zp_td" align="center">
                        ¥680
                      </td>
                      <td className="cb_r3 zp_td cf04" align="center">
                        -
                      </td>
                      <td className="cb_r4 zp_td" align="center">
                        <p className="num_line">
                          <span className="num_down" index="6899">
                            -
                          </span>
                          <input value="1" index="6899" className="num_input" type="text" />
                          <span className="num_up" index="6899">
                            +
                          </span>
                        </p>
                      </td>
                      <td className="cb_r5 zp_td" align="center">
                        <span>
                          ¥<b className="xp1">680</b>
                        </span>
                      </td>
                      <td className="cb_r6 zp_td" align="center">
                        <span className="coll_opt">
                          <LinkA href="javascript:Collect(6899)">收藏</LinkA>
                        </span>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <span className="del_opt">
                          <LinkA href="javascript:Del(6899)">删除</LinkA>
                        </span>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </dd>
            <div className={styles.p1}>
              <p>电子券在“去结算”页面中使用</p>
            </div>
            <div className={styles.p2}>
              <p className={styles.p3}>商品价格和库存请以订单提交时为准</p>
              <p className={styles.p4}>
                应付商品金额：
                <span>
                  ¥<b>879</b>
                </span>
              </p>
            </div>
          </dl>
        </div>
        <div className={styles.p5}>
          <p className={styles.p6}>
            <b className="b1" />
            <Link to="/home/a">继续购物</Link>
          </p>
          <LinkA className={styles.b2}>
            <Image src="images/xb13.jpg" alt="" />
          </LinkA>
        </div>
        <div className={styles.b3}>
          <h1>&nbsp;你可能还需要</h1>
          <div className={styles.qv}>
            <ul>
              <li className={styles.goods_pic}>
                <LinkA>
                  <Image src="images/458.jpg" width="100" height="100" />
                </LinkA>
              </li>
              <li className={styles.goods_name}>
                <LinkA>53度 茅台 飞天带杯（2016年产） 500ml</LinkA>&nbsp;&nbsp;
                <span className="cf04">下单立减50元 多买多减</span>
              </li>
              <li className={styles.goods_price}>¥926</li>
              <li className={styles.goods_btn}>
                <LinkA className="">加入购物车</LinkA>{' '}
              </li>
            </ul>
            <ul>
              <li className={styles.goods_pic}>
                <LinkA>
                  <Image src="images/459.jpg" width="100" height="100" />
                </LinkA>
              </li>
              <li className={styles.goods_name}>
                <LinkA>52度 五粮液 500ml</LinkA>&nbsp;&nbsp;
                <span className="cf04">下单立减50元 多买多减</span>
              </li>
              <li className={styles.goods_price}>¥728</li>
              <li className={styles.goods_btn}>
                <LinkA className="">加入购物车</LinkA>{' '}
              </li>
            </ul>
            <ul>
              <li className={styles.goods_pic}>
                <LinkA>
                  <Image src="images/460.jpg" width="100" height="100" />
                </LinkA>
              </li>
              <li className={styles.goods_name}>
                <LinkA>52度 剑南春 500ml</LinkA>&nbsp;&nbsp;
                <span className="cf04">剑南古道 历史名酒</span>
              </li>
              <li className={styles.goods_price}>¥338</li>
              <li className={styles.goods_btn}>
                <LinkA className="" index="35">
                  加入购物车
                </LinkA>{' '}
              </li>
            </ul>
            <ul>
              <li className={styles.goods_pic}>
                <LinkA>
                  <Image src="images/461.jpg" width="100" height="100" />
                </LinkA>
              </li>
              <li className={styles.goods_name}>
                <LinkA>52度 五粮液 1618 500ml</LinkA>&nbsp;&nbsp;
                <span className="cf04">下单立减60元 多买多减</span>
              </li>
              <li className={styles.goods_price}>¥728</li>
              <li className={styles.goods_btn}>
                <LinkA className="">加入购物车</LinkA>
              </li>
            </ul>
            <ul>
              <li className={styles.goods_pic}>
                <LinkA>
                  <Image src="images/462.jpg" width="100" height="100" />
                </LinkA>
              </li>
              <li className={styles.goods_name}>
                <LinkA>53度 茅台 迎宾酒 500ml*6瓶</LinkA>&nbsp;&nbsp;
                <span className="cf04">限时抢购 下手要快</span>
              </li>
              <li className={styles.goods_price}>¥343</li>
              <li className={styles.goods_btn}>
                <LinkA className="">加入购物车</LinkA>{' '}
              </li>
            </ul>
            <ul>
              <li className={styles.goods_pic}>
                <LinkA>
                  <Image src="images/463.jpg" width="100" height="100" />
                </LinkA>
              </li>
              <li className={styles.goods_name}>
                <LinkA>52度 五粮迎宾酒 精品 500ml*6</LinkA>&nbsp;&nbsp;
                <span className="cf04" />
              </li>
              <li className={styles.goods_price}>¥348</li>
              <li className={styles.goods_btn}>
                <LinkA className="">加入购物车</LinkA>{' '}
              </li>
            </ul>
            <ul>
              <li className={styles.goods_pic}>
                <LinkA>
                  <Image src="images/464.jpg" width="100" height="100" />
                </LinkA>
              </li>
              <li className={styles.goods_name}>
                <LinkA>52度 泸州老窖 金泸州 A6 750ml*2</LinkA>&nbsp;&nbsp;
                <span className="cf04" />
              </li>
              <li className={styles.goods_price}>¥99</li>
              <li className={styles.goods_btn}>
                <LinkA className="">加入购物车</LinkA>
              </li>
            </ul>
            <ul>
              <li className={styles.goods_pic}>
                <LinkA>
                  <Image src="images/465.jpg" width="100" height="100" />
                </LinkA>
              </li>
              <li className={styles.goods_name}>
                <LinkA>52度 剑南春 天益老号珍藏级 500ml</LinkA>&nbsp;&nbsp;
                <span className="cf04">好酒 值得品尝</span>
              </li>
              <li className={styles.goods_price}>¥379</li>
              <li className={styles.goods_btn}>
                <LinkA className="">加入购物车</LinkA>
              </li>
            </ul>
            <ul>
              <li className={styles.goods_pic}>
                <LinkA>
                  <Image src="images/466.jpg" width="100" height="100" />
                </LinkA>
              </li>
              <li className={styles.goods_name}>
                <LinkA>52度 国窖1573 500ml</LinkA>&nbsp;&nbsp;<span className="cf04">买一送一</span>
              </li>
              <li className={styles.goods_price}>¥649</li>
              <li className={styles.goods_btn}>
                <LinkA className="" index="37">
                  加入购物车
                </LinkA>{' '}
              </li>
            </ul>
            <ul>
              <li className={styles.goods_pic}>
                <LinkA>
                  <Image src="images/467.jpg" width="100" height="100" />
                </LinkA>
              </li>
              <li className={styles.goods_name}>
                <LinkA>52度 茅台集团经典原浆V15 1000ml</LinkA>&nbsp;&nbsp;
                <span className="cf04" />
              </li>
              <li className={styles.goods_price}>¥139</li>
              <li className={styles.goods_btn}>
                <LinkA className="" index="6129">
                  加入购物车
                </LinkA>{' '}
              </li>
            </ul>
            <ul>
              <li className={styles.goods_pic}>
                <LinkA>
                  <Image src="images/468.jpg" width="100" height="100" />
                </LinkA>
              </li>
              <li className={styles.goods_name}>
                <LinkA>52度 五粮液 五星级 金装版 500ml</LinkA>&nbsp;&nbsp;
                <span className="cf04" />
              </li>
              <li className={styles.goods_price}>¥126</li>
              <li className={styles.goods_btn}>
                <LinkA className="">加入购物车</LinkA>{' '}
              </li>
            </ul>
            <ul>
              <li className={styles.goods_pic}>
                <LinkA>
                  <Image src="images/469.jpg" width="100" height="100" />
                </LinkA>
              </li>
              <li className={styles.goods_name}>
                <LinkA>38度 茅台 五星（2011年） 500ml</LinkA>&nbsp;&nbsp;
                <span className="cf04" />
              </li>
              <li className={styles.goods_price}>¥599</li>
              <li className={styles.goods_btn}>
                <LinkA className="">加入购物车</LinkA>
              </li>
            </ul>
            <div className="clearboth" />
          </div>
        </div>
        <div className={styles.come}>
          <Image src="images/470.gif" alt="" />
          <div className={styles.come1}>
            <ul>
              <li>
                <LinkA>关于我们</LinkA>
              </li>
              <li className={styles.li0}>|</li>
              <li>
                <LinkA>联系我们</LinkA>
              </li>
              <li className={styles.li0}>|</li>
              <li>
                <LinkA>友情链接</LinkA>
              </li>
              <li className={styles.li0}>|</li>
              <li>
                <LinkA>隐私声明</LinkA>
              </li>
            </ul>
          </div>
          <p>
            Copyright® 2009-2016 gjw.com, All Rights Reserved 上海线加下电子商务有限公司 版权所有
          </p>
        </div>
      </Fragment>
    );
  }
}

Shop.propTypes = {};

export default Shop;
