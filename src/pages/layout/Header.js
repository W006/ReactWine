/* eslint-disable react/no-array-index-key */
import React, { PureComponent, Fragment } from 'react';
import { LinkA } from 'components';
import { Link } from 'react-router-dom';
import './Header.scss';

export default class Header extends PureComponent {
  constructor() {
    super();
    this.searchTag = ['茅台', '郎酒', '五粮液', '剑南春', '泸州老窖', '洋河'];
  }

  render() {
    return (
      <Fragment>
        <div className="header">
          <div className="head_box">
            <div className="head_main">
              <a className="focus_us fl">
                <div style={{ cursor: 'pointer' }}>
                  <span>关注购酒网</span>
                </div>
                <span className="hoverImg">
                  <img src="images/1.jpg" alt="" />
                </span>
              </a>
              <ul className="fr head_nav">
                <li className="hello z">
                  Hi,请
                  <Link to="/user/login" className="head_link">
                    登录
                  </Link>
                  /
                  <Link to="/user/registr" className="head_link">
                    注册
                  </Link>
                </li>
                <li className="line" />
                <li className="head_minNav z">
                  <Link to="/user/shop" className="head_link">
                    我的订单
                  </Link>
                </li>
                <li className="line" />
                <li className="head_minNav head_minNav1">
                  <ul className="head_minList">
                    <li>
                      <LinkA>用户中心</LinkA>
                    </li>
                    <li className="head_hideList">
                      <div>
                        <LinkA>商品评论</LinkA>
                      </div>
                      <div>
                        <LinkA>收藏夹</LinkA>
                      </div>
                      <div>
                        <LinkA>我的电子券</LinkA>
                      </div>
                    </li>
                  </ul>
                </li>
                <li className="line" />
                <li className="head_minNav z">
                  <LinkA className="head_link">在线客服</LinkA>
                </li>
                <li className="line" />
                <li className="head_minNav z">
                  <LinkA className="head_link">帮助</LinkA>
                </li>
                <li className="line" />
                <li className="head_minNav z">
                  <LinkA className="head_link active">提意见</LinkA>
                </li>
                <li className="line" />
                <li className="head_minNav z">
                  <LinkA className="head_link active telNum">400-722-1919</LinkA>
                </li>
              </ul>
            </div>
          </div>
        </div>
        {/* <!--悬浮搜索--> */}
        <div style={{ top: '-100px' }} className="fixed_box">
          <div className="mid_box">
            <LinkA className="fixed_logo">
              <img src="images/5.png" alt="" className="fl" />
            </LinkA>
            <div className="fixedInput_box fl">
              <input
                id="fixSch"
                className="fixed_txt fl"
                placeholder="请输入搜索关键词"
                onKeyDown={() => {}}
                type="text"
              />
              <input value="搜索" className="fixed_btn fr" id="fixSearchBtn" type="button" />
            </div>
          </div>
        </div>
        {/* <!--顶部大广告--> */}
        <div className="news_img mid_box v6_clear" style={{ height: '80px', overflow: 'hidden' }}>
          <LinkA>
            <img src="images/371.jpg" alt="" />
          </LinkA>
          <Link to="/home/c">
            <img src="images/372.jpg" alt="" style={{ bottom: '0px' }} className="secondImg" />
          </Link>

          <span className="news_btn fl">展开</span>
        </div>
        {/* <!--顶部公告条--> */}
        <div className="text mid">
          <LinkA className="fl">即日起在线支付立享99折</LinkA>
          <span className="fr text_btn">关闭</span>
        </div>
        {/* <!--logo,搜索，购物车--> */}
        <div className=" mid_box product-search">
          <Link to="/home/a" className="fl main_nav_logo">
            <img src="images/2.jpg" alt="购酒网" />
          </Link>
          <div className="fl main_nav_gif" />
          <div className="main_nav_box fl search-main">
            <div>
              <input id="sch" className="main_nav_txt fl" type="text" />
              <button id="btnsch" className="main_nav_btn fl">
                搜索
              </button>
            </div>
            <div className="main_nav_tags">
              {this.searchTag.map((it, index) => (
                <LinkA key={`_${index}`} title={it} className="v6_hot">
                  {it}
                </LinkA>
              ))}
            </div>
          </div>
          {/* <!--购物车s--> */}
          <div className="main_nav_cart fr dropBox">
            <div className="main_nav_cartBtn" id="myCart">
              <Link to="/user/shop">我的购物车</Link>&nbsp;&nbsp;<span className="arrow">&gt;</span>
              <div className="main_nav_count" id="head_cart_no">
                2
              </div>
            </div>
            <div className="main_nav_dropBox dropBox_hideList" id="head_cart" />
          </div>
        </div>
      </Fragment>
    );
  }
}
