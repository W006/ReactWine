/* eslint-disable jsx-a11y/alt-text */
import React from 'react';
import { Image } from 'components';
import styles from './footer.less';

export default class Footer extends React.PureComponent {
  constructor() {
    super();
    this.footMenu = [
      {
        id: '1',
        title: '新手指南',
        className: 'firstDl',
        children: [
          { id: '11', title: '用户协议' },
          { id: '12', title: '常见问题' },
          { id: '13', title: '网站购物流程' },
          { id: '14', title: '会员制度' },
        ],
      },
      {
        id: '2',
        title: '如何付款',
        children: [
          { id: '21', title: '如何付款' },
          { id: '22', title: '发票制度说明' },
          { id: '23', title: '电子券说明' },
          { id: '24', title: '虚拟账户支付' },
          { id: '25', title: '商品优惠代码说明' },
        ],
      },
      {
        id: '3',
        title: '配送方式',
        children: [
          { id: '31', title: '配送收费标准' },
          { id: '32', title: '配送时间' },
          { id: '33', title: '货到付款支持城市' },
          { id: '34', title: '实体加盟服务商' },
        ],
      },
      {
        id: '4',
        title: '售后服务',
        children: [
          { id: '41', title: '如何办理退换货' },
          { id: '42', title: '如何退换货' },
          { id: '43', title: '联系客服' },
        ],
      },
      {
        id: '5',
        title: '关于我们',
        children: [
          { id: '51', title: '了解购酒网' },
          { id: '52', title: '招兵买马' },
          { id: '53', title: '联系我们' },
          { id: '54', title: '隐私声明' },
          { id: '55', title: '友情链接' },
        ],
      },
      {
        id: '6',
        title: '合作共赢',
        children: [
          { id: '61', title: '品牌馆' },
          { id: '62', title: '大客户采购 new' },
          { id: '63', title: '网站推广 new' },
          { id: '64', title: '供应商供货 new' },
        ],
      },
    ];
  }

  render() {
    return (
      <div className={styles.footer}>
        <div className={styles.footer_top}>
          <div className={styles.footer_top_top}>
            <div className="www fl" />
            <a className="footer_serv fl">
              <Image src="images/369.png" />
            </a>
            <div className="fr footer_box">
              <p className="fl">
                超万平方米仓储体验店
                <br />
                上海松江区茂盛路202弄18号
                <br />
                客服邮箱：kf@goujiuwang.com
                <br />
                <strong>扫描二维码，掌握一手优惠资讯</strong>
                <br />
              </p>
              <Image src="images/370.jpg" className="fl" />
            </div>
          </div>
          <div className={styles.footer_top_bottom}>
            {this.footMenu.map(({ title, children = [], ...other }) => {
              return (
                <dl {...other} key={other.id}>
                  <dt>{title}</dt>
                  {children.map(({ title, ...ot }) => (
                    <dd {...ot} key={ot.id}>
                      <a href="javscript:;">{title}</a>
                    </dd>
                  ))}
                </dl>
              );
            })}
          </div>
        </div>
        <div className={styles.footer_bottom}>
          <p>
            Copyright® 2009-2016 gjw.com, All Rights Reserved 上海线加下电子商务有限公司 版权所有
          </p>
          <div className="footer_Image" />
        </div>
      </div>
    );
  }
}
