import React, { PureComponent } from 'react';
import { Image, LinkA } from 'components';

export default class NavFilter extends PureComponent {
  render() {
    return (
      <div style={{ borderBottom: '2px solid #c40000' }}>
        <div className="v6_navBar mid_box">
          <div className="v6_navBar_cate dropBox fl" id="navBar_cate">
            <div className="navBar_dt">全部商品分类</div>
            {/* <!--分类导航s--> */}
            <div className="navBar_navTab cateMenu ">
              <ul className="navBar_navTabInner">
                <li className="navBar_cateItem noSubNav">
                  <div className="navBar_title">
                    <strong className="fl cateItem_title">
                      购酒直通车<span className="navBar_tip">HOT</span>{' '}
                    </strong>
                  </div>
                  <div className="navBar_linkBox v6_clear">
                    <LinkA className="">大坛美酒</LinkA>{' '}
                    <LinkA className="v6_highLight">高端收藏</LinkA>
                    <LinkA className="">送礼推荐</LinkA>
                    <LinkA className="">整箱促销</LinkA> <LinkA className="">封顶49元</LinkA>{' '}
                    <LinkA className="">婚宴用酒</LinkA>
                  </div>
                </li>
                <li className="navBar_cateItem itemTab">
                  <div className="navBar_title">
                    <strong className="fl cateItem_title">
                      <LinkA>白酒</LinkA>&nbsp;
                    </strong>{' '}
                    <span className="arrow fr">&gt;</span>
                  </div>
                  <div className="navBar_linkBox  v6_clear">
                    <LinkA className="v6_highLight">茅台</LinkA>

                    <LinkA className="v6_highLight">五粮液</LinkA>

                    <LinkA className="">郎酒</LinkA>

                    <LinkA className="">剑南春</LinkA>

                    <LinkA className="">国窖1573</LinkA>

                    <LinkA className="">泸州老窖</LinkA>

                    <LinkA className="v6_highLight">年份老酒</LinkA>
                  </div>
                </li>

                <li className="navBar_cateItem itemTab">
                  <div className="navBar_title">
                    <strong className="fl cateItem_title">
                      <LinkA>葡萄酒</LinkA>&nbsp;
                    </strong>{' '}
                    <span className="arrow fr">&gt;</span>
                  </div>
                  <div className="navBar_linkBox  v6_clear">
                    <LinkA className="v6_highLight">拉菲</LinkA>

                    <LinkA className="">黄尾袋鼠</LinkA>

                    <LinkA className="">奔富</LinkA>

                    <LinkA className="v6_highLight">皇族雄狮</LinkA>

                    <LinkA className="">张裕</LinkA>

                    <LinkA className="">杰卡斯</LinkA>
                  </div>
                </li>

                <li className="navBar_cateItem itemTab">
                  <div className="navBar_title">
                    <strong className="fl cateItem_title">
                      <LinkA>洋酒</LinkA>&nbsp;
                    </strong>{' '}
                    <span className="arrow fr">&gt;</span>
                  </div>
                  <div className="navBar_linkBox  v6_clear">
                    <LinkA className="v6_highLight">人头马</LinkA>

                    <LinkA className="">芝华士</LinkA>

                    <LinkA className="v6_highLight">皇家路易</LinkA>

                    <LinkA className="">尊尼获加</LinkA>

                    <LinkA className="">格兰菲迪</LinkA>

                    <LinkA className="">百龄坛</LinkA>
                  </div>
                </li>

                <li className="navBar_cateItem itemTab">
                  <div className="navBar_title">
                    <strong className="fl cateItem_title">
                      <LinkA>黄酒</LinkA>&nbsp;
                      <LinkA>养生酒</LinkA>&nbsp;
                      <LinkA>啤酒</LinkA>&nbsp;
                    </strong>
                    <span className="arrow fr">&gt;</span>
                  </div>
                  <div className="navBar_linkBox  v6_clear">
                    <LinkA className="v6_highLight">古越龙山</LinkA>

                    <LinkA className="">女儿红</LinkA>

                    <LinkA className="">唐宋</LinkA>

                    <LinkA className="">劲酒</LinkA>

                    <LinkA className="">竹叶青</LinkA>
                  </div>
                </li>

                <li className="navBar_cateItem noSubNav">
                  <div className="navBar_linkBox  v6_clear">
                    <div className="navBar_title">
                      <LinkA className="v6_highLight">茶叶</LinkA>

                      <LinkA className="">酒具</LinkA>

                      <LinkA className="">新品</LinkA>

                      <LinkA className="">清仓专区</LinkA>
                    </div>
                  </div>
                </li>
              </ul>
              {/* <!--白酒s--> */}
              <div style={{ display: 'none' }} className="navBar_subNav">
                <div className="navBar_leftSubNav fl">
                  <div className="navBar_leftInner">
                    <div className="navBar_chns">
                      <LinkA>
                        亏本清仓专区<i className="navBar_chnArrow">&gt;</i>
                      </LinkA>

                      <LinkA>
                        高端收藏酒<i className="navBar_chnArrow">&gt;</i>
                      </LinkA>

                      <LinkA>
                        陈年老酒专区<i className="navBar_chnArrow">&gt;</i>
                      </LinkA>

                      <LinkA>
                        特卖会<i className="navBar_chnArrow">&gt;</i>
                      </LinkA>
                    </div>
                    <div className="navBar_brands v6_clear">
                      <div className="navBar_bTitle">
                        <strong>品牌</strong>
                      </div>
                      <ul className="navBar_brandList">
                        <li>
                          <LinkA>
                            <font color="#c81623">茅台</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="#c81623">五粮液</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="#c81623">剑南春</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA href="http://www.gjw.com/baijiu-luzhoulaojiao">
                            <font color="#c81623">泸州老窖</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">国窖1573</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="#c81623">郎酒</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">洋河</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">汾酒</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="#c81623">酒鬼</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">习酒</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">水井坊</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="#c81623">西凤</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">珍酒</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">古井贡酒</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">董酒</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">白云边</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">景芝</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="#c81623">舍得</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">沱牌</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">金六福</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">口子窖</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">伊力牌</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">小糊涂仙</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">今世缘</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">牛栏山</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">四特</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">国台</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">衡水老白干</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">枝江</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">文君酒</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">迎驾</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="#FF0000">舒心</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">星河湾</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">黄鹤楼</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">太白酒</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">天下福</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">苏酒</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">红星</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">杜康</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">孔府家</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">稻花香</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">皖酒</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">扳倒井</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">鸭溪窖</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">金门高粱酒</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">极道</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">双沟</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">全兴</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">高参</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">青花瓷</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">百年传奇</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">金泸老窖</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">福成酒</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">泸泉</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA href="http://www.gjw.com/baijiu-randian">
                            <font color="">燃點</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">醉安逸</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">一道泓</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">八大怪</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">凤凰情</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">开门红</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">壹湖</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">舒韵</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">御甄</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">常酒</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">毛铺</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">四大美女</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">赖贵恒</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">湘泉</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA target="_blank" className="brands_more">
                            更多&gt;&gt;
                          </LinkA>
                        </li>
                      </ul>
                    </div>
                    <div>
                      <div className="navBar_brandsL fl">
                        <div className="navBar_bTitle" style={{ width: ' 423px' }}>
                          <strong>香型</strong>
                        </div>
                        <ul className="navBar_brandList">
                          <li>
                            <LinkA>董香型</LinkA>
                          </li>

                          <li>
                            <LinkA>凤香型</LinkA>
                          </li>

                          <li>
                            <LinkA>馥郁香</LinkA>
                          </li>

                          <li>
                            <LinkA>兼香型</LinkA>
                          </li>

                          <li>
                            <LinkA>酱香型</LinkA>
                          </li>

                          <li>
                            <LinkA>金门香型</LinkA>
                          </li>

                          <li>
                            <LinkA>老干白</LinkA>
                          </li>

                          <li>
                            <LinkA>绵柔型</LinkA>
                          </li>

                          <li>
                            <LinkA>浓香型</LinkA>
                          </li>

                          <li>
                            <LinkA>清香型</LinkA>
                          </li>

                          <li>
                            <LinkA>柔和型</LinkA>
                          </li>

                          <li>
                            <LinkA>柔雅型</LinkA>
                          </li>

                          <li>
                            <LinkA>儒雅型</LinkA>
                          </li>

                          <li>
                            <LinkA>特香型</LinkA>
                          </li>

                          <li>
                            <LinkA>芝麻香</LinkA>
                          </li>

                          <li>
                            <LinkA>小曲型</LinkA>
                          </li>

                          <li>
                            <LinkA>幽雅醇厚</LinkA>
                          </li>

                          <li>
                            <LinkA>生态竹香型</LinkA>
                          </li>

                          <li>
                            <LinkA>露酒</LinkA>
                          </li>

                          <li>
                            <LinkA>大曲酒</LinkA>
                          </li>

                          <li>
                            <LinkA target="_blank" className="brands_more">
                              更多&gt;&gt;
                            </LinkA>
                          </li>
                        </ul>
                      </div>
                      <div className="navbar_brandR fr">
                        <div className="navBar_bTitle" style={{ width: ' 230px' }}>
                          <strong>产地</strong>
                        </div>
                        <ul className="navBar_brandList">
                          <li>
                            <LinkA>四川</LinkA>
                          </li>

                          <li>
                            <LinkA>山西</LinkA>
                          </li>

                          <li>
                            <LinkA>贵州</LinkA>
                          </li>

                          <li>
                            <LinkA>江苏</LinkA>
                          </li>

                          <li>
                            <LinkA>安徽</LinkA>
                          </li>

                          <li>
                            <LinkA>北京</LinkA>
                          </li>

                          <li>
                            <LinkA>内蒙古</LinkA>
                          </li>

                          <li>
                            <LinkA>山东</LinkA>
                          </li>

                          <li>
                            <LinkA>河北</LinkA>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="navBar_rightSubNav fr">
                  <LinkA>
                    <Image src="images/7.jpg" />
                  </LinkA>
                  <LinkA>
                    <Image src="images/8.jpg" />
                  </LinkA>
                </div>
              </div>
              {/* <!--白酒e--> */}
              {/* <!--红酒s--> */}
              <div style={{ display: 'none' }} className="navBar_subNav">
                <div className="navBar_leftSubNav fl">
                  <div className="navBar_leftInner">
                    <div className="navBar_chns">
                      <LinkA>
                        法国原装进口<i className="navBar_chnArrow">&gt;</i>
                      </LinkA>
                      <LinkA>
                        澳大利亚原装进口<i className="navBar_chnArrow">&gt;</i>
                      </LinkA>

                      <LinkA>
                        起泡酒香槟酒<i className="navBar_chnArrow">&gt;</i>
                      </LinkA>
                    </div>
                    <div className="navBar_brands v6_clear navBar_row">
                      <div className="col-4">
                        <div className="col-4-inner">
                          <div className="navBar_bTitle">
                            <strong>产国</strong>
                          </div>
                          <ul className="navBar_brandList col-110">
                            <li className="col-3">
                              <LinkA>法国</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>澳大利亚</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>中国</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>智利</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>西班牙</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>阿根廷</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>南非</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>意大利</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>加拿大</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>美国</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>希腊</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>德国</LinkA>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div className="col-4">
                        <div className="col-4-inner">
                          <div className="navBar_bTitle">
                            <strong>品种</strong>
                          </div>
                          <ul className="navBar_brandList col-110">
                            <li className="col-3">
                              <LinkA>芭贝拉</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>波尔多</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>长相思</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>赤霞珠</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>红麝香</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>解百纳</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>卡曼尼</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>蔻狄丝</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>玛尔维萨</LinkA>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div className="col-4">
                        <div className="col-4-inner">
                          <div className="navBar_bTitle">
                            <strong>类型</strong>
                          </div>
                          <ul className="navBar_brandList col-110">
                            <li className="col-3">
                              <LinkA>红葡萄酒</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>白葡萄酒</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>桃红葡萄酒</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>起泡酒/香槟</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>冰酒/贵腐/甜酒</LinkA>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div className="navBar_brands v6_clear navBar_row ">
                      <div className="navBar_bTitle">
                        <strong>品牌</strong>
                      </div>
                      <ul className="navBar_brandList col-106">
                        <li>
                          <LinkA>
                            <font color="">拉菲系列</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="#c81623">奔富</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="#c81623">黄尾袋鼠</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">法比纳</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">红魔鬼</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="#c81623">张裕</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">长城</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">加州乐事</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">皇轩</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">嘉诚庄园</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="#c81623">卡斯特</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">法国路易拉菲</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">爱之湾</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">王朝</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">贝雷斯</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">尼雅</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">香格里拉</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">神树</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">晨曦之星</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">路易斯伯爵</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">瑞格堡</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">红山溪</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">智利 拉丁神话</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">菲波</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">西班牙唐卢卡</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">阿隆索</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">玛莉迪麦</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">加利帕索</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">意大利玫瑰小屋</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">法国木桐夏嘉城堡</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">意大利春之物语</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">杰卡斯</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="#c81623">酩悦</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">拉菲庄园</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">艾拉贝拉</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">皇族雄狮</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">洛诗敦莱福</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">活灵魂</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">卡莫莎</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">云南红</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">圣美都</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">威龙</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">绿雾</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">玫瑰时代</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">小企鹅</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">凯斯勒</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">红蔓庄园</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">普里米</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">法莱雅酒堡</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="#c81623">木桐庄园</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">红山谷</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">激情飞扬</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">牡丹公爵</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="#c81623">维图城堡</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">康堤</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">卡尔德</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">喜来</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">卡斯莫尼</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">玛利妮康帝</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">丹赫</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">索维纳</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">马标</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">斯麦特</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">拉图庄园</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">奥比昂庄园</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">白马庄园</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">玛歌庄园</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">宝马庄园</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA className="brands_more">更多&gt;&gt;</LinkA>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div className="navBar_rightSubNav fr">
                  <LinkA>
                    <Image src="images/9.jpg" />
                  </LinkA>

                  <LinkA>
                    <Image src="images/10.jpg" />
                  </LinkA>
                </div>
              </div>
              {/* <!--红酒酒e--> */}
              {/* <!--洋酒s--> */}
              <div style={{ display: 'none' }} className="navBar_subNav">
                <div className="navBar_leftSubNav fl">
                  <div className="navBar_leftInner">
                    <div className="navBar_chns">
                      <LinkA>
                        格兰菲迪<i className="navBar_chnArrow">&gt;</i>
                      </LinkA>
                    </div>
                    <div className="navBar_brands v6_clear navBar_row" style={{ height: '168px' }}>
                      <div className="navBar_bTitle">
                        <strong>品牌</strong>
                      </div>
                      <ul className="navBar_brandList">
                        <li>
                          <LinkA>
                            <font color="">百龄坛</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="#c81623">芝华士</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="#c81623">格兰菲迪</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="#c81623">尊尼获加</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">占边</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">杰克丹尼</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">麦卡伦</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">帝王</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="#c81623">皇家礼炮</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">珍宝威</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">加拿大cc</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">顺风</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">老伯威</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">三得利</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">添宝</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">温莎</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">马谛氏</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">第林可</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">本尼维斯</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">爱德华</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">格兰御鹿</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">尊美醇</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">剑威</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">御鹿</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">爱布船</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="#c81623">皇家路易</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="#c81623">人头马</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="#c81623">轩尼诗</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">马爹利</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">帕夫利季斯</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">萨隆公爵</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">蒙特伯爵</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">斯米诺</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">深蓝</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">瑞典绝对</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">苏连</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">灰雁</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">芬兰</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">雪树</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">维波罗瓦</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">皇太子</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA>
                            <font color="">魔冰</font>
                          </LinkA>
                        </li>

                        <li>
                          <LinkA className="brands_more">更多&gt;&gt;</LinkA>
                        </li>
                      </ul>
                    </div>
                    <div className="navBar_brands v6_clear navBar_row">
                      <div className="col-4">
                        <div className="col-4-inner">
                          <div className="navBar_bTitle">
                            <strong>品种</strong>
                          </div>
                          <ul className="navBar_brandList col-110">
                            <li className="col-3">
                              <LinkA>白兰地</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>伏特加</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>朗姆酒</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>力娇酒</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>墨西哥烈酒</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>起泡酒</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>甜酒</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>威士忌</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>香槟</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>金酒</LinkA>
                            </li>
                            <li className="col-3">
                              <LinkA>利口酒</LinkA>
                            </li>
                            <li className="col-3">
                              <LinkA className="brands_more">更多&gt;&gt;</LinkA>
                            </li>
                          </ul>
                          <div className="navBar_bTitle ">
                            <strong>价格区间</strong>
                          </div>
                          <ul className="navBar_brandList col-110">
                            <li className="col-3">
                              <LinkA>1-99</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>100-199</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>200-599</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>600-999</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>1000-1999</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>2000-5999</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>6000及以上</LinkA>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div className="navBar_leftAdBox">
                        <div className="navBar_leftAdBox_inner">
                          <LinkA>
                            <Image src="images/11.jpg" />
                          </LinkA>

                          <LinkA>
                            <Image src="images/12.jpg" />
                          </LinkA>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="navBar_rightSubNav fr">
                  <LinkA>
                    <Image src="images/13.jpg" />
                  </LinkA>

                  <LinkA>
                    <Image src="images/14.jpg" />
                  </LinkA>
                </div>
              </div>
              {/* <!--洋酒e--> */}
              <div style={{ display: 'none' }} className="navBar_subNav">
                <div className="navBar_leftSubNav fl">
                  <div className="navBar_leftInner">
                    <div className="navBar_chns" />
                    <div className="navBar_brands v6_clear navBar_row mgt10">
                      <div className="col-4">
                        <div className="col-4-inner">
                          <div className="navBar_bTitle">
                            <strong>黄酒</strong>
                          </div>
                          <ul className="navBar_brandList col-110">
                            <li className="col-3">
                              <LinkA>
                                <font color="">石库门</font>
                              </LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>
                                <font color="">和酒</font>
                              </LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>
                                <font color="#c81623">古越龙山</font>
                              </LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>
                                <font color="#c81623">女儿红</font>
                              </LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>
                                <font color="">沙洲优黄</font>
                              </LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>
                                <font color="">会稽山</font>
                              </LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>
                                <font color="">塔牌</font>
                              </LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>
                                <font color="">乌毡帽</font>
                              </LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>
                                <font color="">唐宋</font>
                              </LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>
                                <font color="">钱塘人家</font>
                              </LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>
                                <font color="">咸亨</font>
                              </LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA className="brands_more">更多&gt;&gt;</LinkA>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div className="col-4">
                        <div className="col-4-inner">
                          <div className="navBar_bTitle">
                            <strong>养生酒</strong>
                          </div>
                          <ul className="navBar_brandList col-110">
                            <li className="col-3">
                              <LinkA>
                                <font color="#c81623">劲酒</font>
                              </LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>
                                <font color="#c81623">竹叶青</font>
                              </LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>
                                <font color="">红姑娘</font>
                              </LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>
                                <font color="">彤康庄园</font>
                              </LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA className="brands_more">更多&gt;&gt;</LinkA>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div className="col-4">
                        <div className="col-4-inner">
                          <div className="navBar_bTitle">
                            <strong>啤酒</strong>
                          </div>
                          <ul className="navBar_brandList col-110">
                            <li className="col-3">
                              <LinkA>
                                <font color="">蓝带</font>
                              </LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA className="brands_more">更多&gt;&gt;</LinkA>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div className="navBar_brands v6_clear navBar_row mgt40">
                      <div className="col-4">
                        <div className="col-4-inner">
                          <div className="navBar_bTitle">
                            <strong>黄酒价格区间</strong>
                          </div>
                          <ul className="navBar_brandList col-110">
                            <li className="col-3">
                              <LinkA>1-99</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>100-199</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>200-599</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>600-999</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>1000-1999</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>2000-5999</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>6000及以上</LinkA>
                            </li>
                          </ul>

                          <LinkA>
                            <Image src="images/15.jpg" />
                          </LinkA>
                        </div>
                      </div>
                      <div className="col-4">
                        <div className="col-4-inner">
                          <div className="navBar_bTitle">
                            <strong>养生酒价格区间</strong>
                          </div>
                          <ul className="navBar_brandList col-110">
                            <li className="col-3">
                              <LinkA>1-99</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>100-199</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>200-599</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>600-999</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>1000-1999</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>2000-5999</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>6000及以上</LinkA>
                            </li>
                          </ul>

                          <LinkA>
                            <Image src="images/16.jpg" />
                          </LinkA>
                        </div>
                      </div>
                      <div className="col-4">
                        <div className="col-4-inner">
                          <div className="navBar_bTitle">
                            <strong>啤酒价格区间</strong>
                          </div>
                          <ul className="navBar_brandList col-110">
                            <li className="col-3">
                              <LinkA>1-99</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>100-199</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>200-599</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>600-999</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>1000-1999</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>2000-5999</LinkA>
                            </li>

                            <li className="col-3">
                              <LinkA>6000及以上</LinkA>
                            </li>
                          </ul>

                          <LinkA className="navBar_adBox-sm">
                            <Image src="images/17.jpg" />
                          </LinkA>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="navBar_rightSubNav fr">
                  <LinkA>
                    <Image src="images/18.jpg" />
                  </LinkA>
                  <LinkA>
                    <Image src="images/19.jpg" />
                  </LinkA>
                </div>
              </div>
            </div>
          </div>
          <div className="navBar_items fl">
            <ul>
              <li>
                <LinkA className="active">首页</LinkA>
              </li>
              <li>
                <LinkA>特卖会</LinkA>
                <span className="navBar_tip cur">HOT</span>
              </li>
              <li>
                <LinkA>新品尝鲜</LinkA>
                <span className="navBar_tip cur">NEW</span>
              </li>
              <li>
                <LinkA>进口馆</LinkA>
              </li>
              <li>
                <LinkA>陈年老酒</LinkA>
              </li>
              <li>
                <LinkA>醉爱小酒版</LinkA>
              </li>
              <li>
                <LinkA>高端收藏酒</LinkA>
              </li>
            </ul>
          </div>
          <div className="fr navBar_act" style={{ height: '37px' }}>
            <LinkA>
              <Image src="images/6.gif" />
            </LinkA>
          </div>
        </div>
      </div>
    );
  }
}
