import React from 'react';

import { LinkA, Pager } from 'components';
import Content from 'pages/common/content';
import Hot from 'pages/common/hot';

export default class Main extends React.PureComponent {
  render() {
    return (
      <div className="list_main">
        <Content />
        <div style={{ clear: 'both' }} />
        <Pager />
        <Hot />
        <div style={{ clear: 'both' }} />
        <div className="last">
          <div className="last_top">
            <p />
            <span>商品分类</span>
            <p />
          </div>
          <div style={{ clear: 'both' }} />
          <dl>
            <dt>
              <span className="span11" />
              <b>白酒</b>
            </dt>
            <dd>
              <li>
                <LinkA>酱香型</LinkA>
              </li>
              <li>
                <LinkA>浓香型</LinkA>
              </li>
              <li>
                <LinkA>清香型</LinkA>
              </li>
              <li>
                <LinkA>凤香型</LinkA>
              </li>
              <li>
                <LinkA>馥郁香</LinkA>
              </li>
              <li>
                <LinkA>董香型</LinkA>
              </li>
              <li>
                <LinkA>特香型</LinkA>
              </li>
              <li>
                <LinkA>芝麻香</LinkA>
              </li>
              <li>
                <LinkA>兼香型</LinkA>
              </li>
              <li>
                <LinkA>大曲酒</LinkA>
              </li>
              <li>
                <LinkA>金门香型</LinkA>
              </li>
              <li>
                <LinkA>老干白</LinkA>
              </li>
              <li>
                <LinkA>绵柔型</LinkA>
              </li>
              <li>
                <LinkA>柔和型</LinkA>
              </li>
              <li>
                <LinkA>小曲型</LinkA>
              </li>
              <li>
                <LinkA>生态竹香</LinkA>
              </li>
            </dd>
          </dl>
          <dl>
            <dt>
              <span className="span21" />
              <b>葡萄酒</b>
            </dt>
            <dd>
              <li>
                <LinkA href="">红葡萄酒</LinkA>
              </li>
              <li>
                <LinkA href="">白葡萄酒</LinkA>
              </li>
              <li>
                <LinkA href="">桃红葡.</LinkA>
              </li>
              <li>
                <LinkA href="">加强...</LinkA>
              </li>
              <li>
                <LinkA href="">起泡..</LinkA>
              </li>
              <li>
                <LinkA href="">冰酒..</LinkA>
              </li>
            </dd>
          </dl>
          <dl>
            <dt>
              <span className="span31" />
              <b>洋酒</b>
            </dt>
            <dd>
              <li>
                <LinkA href="">白兰地</LinkA>
              </li>
              <li>
                <LinkA href="">伏特加</LinkA>
              </li>
              <li>
                <LinkA href="">朗姆酒</LinkA>
              </li>
              <li>
                <LinkA href="">力娇酒</LinkA>
              </li>
              <li>
                <LinkA href="">墨西哥</LinkA>
              </li>
              <li>
                <LinkA href="">起泡酒</LinkA>
              </li>
              <li>
                <LinkA href="">甜酒</LinkA>
              </li>
              <li>
                <LinkA href="">威士忌</LinkA>
              </li>
              <li>
                <LinkA href="">香槟</LinkA>
              </li>
              <li>
                <LinkA href="">金酒</LinkA>
              </li>
              <li>
                <LinkA href="">利口酒</LinkA>
              </li>
              <li>
                <LinkA href="">其他</LinkA>
              </li>
              <li>
                <LinkA href="">清酒</LinkA>
              </li>
              <li>
                <LinkA href="">龙舌兰</LinkA>
              </li>
            </dd>
          </dl>
          <dl>
            <dt>
              <span className="span41" />
              <b>保健酒</b>
            </dt>
            <dd>
              <li>
                <LinkA href="">劲酒</LinkA>
              </li>
              <li>
                <LinkA href="">竹叶青</LinkA>
              </li>
              <li>
                <LinkA href="">红姑娘</LinkA>
              </li>
              <li>
                <LinkA href="">彤康庄园</LinkA>
              </li>
            </dd>
          </dl>
          <dl>
            <dt>
              <span className="span51" />
              <b>黄酒</b>
            </dt>
            <dd style={{ ' borderRight': 'none' }}>
              <li>
                <LinkA href="">石库门</LinkA>
              </li>
              <li>
                <LinkA href="">和酒</LinkA>
              </li>
              <li>
                <LinkA href="">古越龙山</LinkA>
              </li>
              <li>
                <LinkA href="">女儿红</LinkA>
              </li>
              <li>
                <LinkA href="">沙洲优黄</LinkA>
              </li>
              <li>
                <LinkA href="">会稽山</LinkA>
              </li>
              <li>
                <LinkA href="">塔牌</LinkA>
              </li>
              <li>
                <LinkA href="">乌毡帽</LinkA>
              </li>
              <li>
                <LinkA href="">唐宋</LinkA>
              </li>
              <li>
                <LinkA href="">钱塘人家</LinkA>
              </li>
              <li>
                <LinkA href="">咸亨</LinkA>
              </li>
              <li>
                <LinkA href="">绍兴师爷</LinkA>
              </li>
              <li>
                <LinkA href="">梁祝</LinkA>
              </li>
              <li>
                <LinkA href="">汉糯</LinkA>
              </li>
              <li>
                <LinkA href="">红米酒</LinkA>
              </li>
              <li>
                <LinkA href="">马头墙</LinkA>
              </li>
            </dd>
          </dl>
        </div>
      </div>
    );
  }
}
