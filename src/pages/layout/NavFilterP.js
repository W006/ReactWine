import React from 'react';
import { Image, LinkA } from 'components';
import SMenu from '../common/smenu';

const horMenus = [
  { id: 1, title: '首页', isHot: false },
  { id: 1, title: '特卖会', isHot: false },
  { id: 1, title: '进口馆', isHot: false },
  { id: 1, title: '新品尝鲜', isHot: true },
  { id: 1, title: '陈年老酒', isHot: false },
  { id: 1, title: '醉爱小酒版', isHot: false },
  { id: 1, title: '高端收藏酒', isHot: false },
];

export default class NavFilterP extends React.Component {
  render() {
    return (
      <div style={{ borderBottom: '2px solid #c40000' }}>
        <div className="v6_navBar mid_box">
          <SMenu />
          <div className="navBar_items fl">
            <ul>
              {horMenus.map((i) => {
                return (
                  <li>
                    {/* <LinkA className="active">{i.title}</LinkA> */}
                    <LinkA>{i.title}</LinkA>
                    {i.isHot ? <span className="navBar_tip cur">NEW</span> : null}
                  </li>
                );
              })}
            </ul>
          </div>
          <div className="fr navBar_act" style={{ height: '37px' }}>
            <LinkA>
              <Image src="images/6.gif" />
            </LinkA>
          </div>
        </div>
      </div>
    );
  }
}
