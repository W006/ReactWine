import React, { PureComponent } from 'react';
import { Image } from 'components';

export default class SliderBar extends PureComponent {
  render() {
    return (
      <div id="sliderBar">
        <div className="sliderBar_l">
          <div id="slider" className="sliderBar_l1">
            <Image src="images/57442b91N9a2ef376.png" />
          </div>
          <div className="sliderBar_l2">
            <i className="sliderBar_l21 sliderBar_l211" />
            <span className="sliderBar_l22">会员中心</span>
          </div>
          <div className="sliderBar_l2">
            <i className="sliderBar_l21 sliderBar_l212" />
            <span className=" sliderBar_l22">购物车</span>
          </div>
          <div className="sliderBar_l2">
            <i className="sliderBar_l21 sliderBar_l213" />
            <span className=" sliderBar_l22">我的关注</span>
          </div>
          <div className="sliderBar_l2">
            <i className="sliderBar_l21 sliderBar_l214" />
            <span className=" sliderBar_l22">我的足迹</span>
          </div>
          <div className="sliderBar_l2">
            <i className="sliderBar_l21 sliderBar_l215" />
            <span className=" sliderBar_l22">我的消息</span>
          </div>
          <div className="sliderBar_l2">
            <i className="sliderBar_l21 sliderBar_l216" />
            <span className=" sliderBar_l22">咨询JIMI</span>
          </div>
          <div className="sliderBar_l2">
            <i className="sliderBar_l21 sliderBar_l217" />
            <span className=" sliderBar_l22">顶部</span>
          </div>
          <div className="sliderBar_l2">
            <i className="sliderBar_l21 sliderBar_l218" />
            <span className=" sliderBar_l22">反馈</span>
          </div>
        </div>
      </div>
    );
  }
}
