import React, { PureComponent } from 'react';
import createHistory from 'history/createHashHistory';
import { Router, Route, Redirect, Switch } from 'react-router';
import HomeC from './main/HomeC';
import HomeB from './main/HomeB';
import HomeA from './main/HomeA';
import Login from './user/login/Login';
import Reginstration from './user/regist/Registration';
import Shop from './shop';
import './app.scss';

class App extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    return (
      <div>
        <Router history={createHistory()}>
          <Switch>
            <Route path="/home/a" component={HomeA} />
            <Route path="/home/b" component={HomeB} />
            <Route path="/home/c" component={HomeC} />
            <Route path="/user/login" component={Login} />
            <Route path="/user/registr" component={Reginstration} />
            <Route path="/user/shop" component={Shop} />
            <Redirect pat="/" to="/home/a" />
          </Switch>
        </Router>
        {/* <Main /> */}
        {/* <HomeA /> */}
      </div>
    );
  }
}

App.propTypes = {};

export default App;
