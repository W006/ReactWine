/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable no-script-url */
import React, { PureComponent } from 'react';
import SiderBar from '../layout/SiderBar';
import Header from '../layout/Header';
import Main from '../layout/Main';
import Footer from '../layout/Footer';
import NavFilter from '../layout/NavFilter';

import '../../css/index.css';
import '../../css/list.css';
// import '../../css/details.css'
import '../../css/proShow.css';
// import '../../css/shop.css'
import '../../css/special.css';

class MainList extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    return (
      <div>
        {/* <!--侧边 s--> */}
        <SiderBar />
        {/* <!--顶部导航s--> */}
        <Header />
        {/* <!--顶部导航查询条件s--> */}
        <NavFilter />
        <div style={{ clear: 'both' }} />
        {/* <!-- 内容区 --> */}
        <Main />
        {/* <!-- 底部推荐和版权 --> */}
        <Footer />
      </div>
    );
  }
}

MainList.propTypes = {};

export default MainList;
