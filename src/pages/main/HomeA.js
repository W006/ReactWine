/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable no-script-url */
import React, { PureComponent, Fragment } from 'react';
import Header from '../layout/Header';
import Footer from '../layout/Footer';
import NavFilterP from '../layout/NavFilterP';
import '../../css/index.css';
import '../../css/list.css';
import '../../css/proShow.css';
import '../../css/special.css';
import WHotel from '../common/whotel';
import SliderBar from '../common/SliderBar';
import FocusBox from '../common/FocusBox';
import Advert from '../common/Advert';

class HomeA extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    // return <Header />
    // return <NavFilterP />
    return (
      <Fragment>
        {/* <!--顶部导航s--> */}
        <Header />
        <NavFilterP />
        <div style={{ clear: 'both' }} />
        <FocusBox />
        {/* 广告 */}
        <Advert />
        {/* 酒楼 */}
        <WHotel />
        {/* <!-- footer --> */}
        <Footer />
        {/* <!-- 右边控制栏 --> */}
        <SliderBar />
      </Fragment>
    );
  }
}

HomeA.propTypes = {};
export default HomeA;
