import React, { PureComponent } from 'react';
import { Image, LinkA } from 'components';
import Header from '../layout/Header';
import Main from '../layout/Main';
import Footer from '../layout/Footer';
import NavFilter from '../layout/NavFilter';

class User extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    return (
      <div>
        <Header />
        <NavFilter />
        <div style={{ clear: 'both' }} />
        {/* <!-- 大图 --> */}
        <div className="Image">
          <LinkA>
            <Image src="images/373.jpg" alt="" />
          </LinkA>
        </div>
        <div className="img1">
          <LinkA>
            <Image src="images/374.jpg" alt="" />
          </LinkA>
        </div>
        {/* <!-- 内容图 --> */}
        <div className="img2">
          <div className="img3">
            <div className="lp" />
            <div className="lp1">
              <LinkA />
            </div>
            <div className="lp2">
              <LinkA />
            </div>
            <div className="lp3">
              <LinkA />
            </div>
          </div>
        </div>

        <div className="img4">
          <div className="img5">
            <LinkA />
          </div>
        </div>

        <div className="img7">
          <div className="img6">
            <div className="cp" />
            <div>
              <LinkA class="cp1" />
            </div>
            <div>
              <LinkA class="cp2" />
            </div>
            <div>
              <LinkA class="cp3" />
            </div>
            <div>
              <LinkA class="cp4" />
            </div>
          </div>
          <div className="img8">
            <LinkA class="a1" />
            <LinkA />
            <LinkA />
            <LinkA />
            <LinkA />
          </div>
          <div className="img9">
            <LinkA style={{ marginRight: '8px' }}>
              <Image
                src="images/379.jpg"
                name="picture"
                onMouseOver="this.src='images/383.jpg'"
                onMouseOut="this.src='images/379.jpg'"
                align="middle"
                border="0"
                height="328"
                width="251"
              />
            </LinkA>
            <LinkA style={{ marginRight: '8px' }}>
              <Image
                src="images/380.jpg"
                name="picture"
                onMouseOver="this.src='images/384.jpg'"
                onMouseOut="this.src='images/380.jpg'"
                align="middle"
                border="0"
                height="328"
                width="251"
              />
            </LinkA>
            <LinkA style={{ marginRight: '8px' }}>
              <Image
                src="images/381.jpg"
                name="picture"
                onMouseOver="this.src='images/385.jpg'"
                onMouseOut="this.src='images/381.jpg'"
                align="middle"
                border="0"
                height="328"
                width="251"
              />
            </LinkA>
            <LinkA>
              <Image
                src="images/382.jpg"
                name="picture"
                onMouseOver="this.src='images/386.jpg'"
                onMouseOut="this.src='images/382.jpg'"
                align="middle"
                border="0"
                height="328"
                width="251"
              />
            </LinkA>
          </div>
          <div className="img01">
            <div>
              <LinkA class="cp1" />
            </div>
            <div>
              <LinkA class="cp2" />
            </div>
            <div>
              <LinkA class="cp3" />
            </div>
            <div>
              <LinkA class="cp4" />
            </div>
          </div>
          <div className="img02">
            <LinkA>
              <Image src="images/389.jpg" alt="" />
            </LinkA>
            <LinkA>
              <Image src="images/390.jpg" alt="" />
            </LinkA>
            <LinkA>
              <Image src="images/391.jpg" alt="" />
            </LinkA>
            <LinkA>
              <Image src="images/392.jpg" alt="" />
            </LinkA>
            <LinkA>
              <Image src="images/393.jpg" alt="" />
            </LinkA>
            <LinkA>
              <Image src="images/394.jpg" alt="" />
            </LinkA>
            <LinkA>
              <Image src="images/395.jpg" alt="" />
            </LinkA>
            <LinkA>
              <Image src="images/396.jpg" alt="" />
            </LinkA>
          </div>
        </div>
        <div className="img03">
          <LinkA>
            <Image src="images/397.jpg" alt="" />
          </LinkA>
        </div>

        <div className="img04">
          <LinkA>
            <Image src="images/398.jpg" alt="" />
          </LinkA>
        </div>
        <div id="sliderBar">
          <div className="sliderBar_l">
            <div id="slider" className="sliderBar_l1">
              <Image src="images/57442b91N9a2ef376.png" />
            </div>
            <div className="sliderBar_l2">
              <i className="sliderBar_l21 sliderBar_l211" />
              <span className="sliderBar_l22">会员中心</span>
            </div>
            <div className="sliderBar_l2">
              <i className="sliderBar_l21 sliderBar_l212" />
              <span className=" sliderBar_l22">购物车</span>
            </div>
            <div className="sliderBar_l2">
              <i className="sliderBar_l21 sliderBar_l213" />
              <span className=" sliderBar_l22">我的关注</span>
            </div>
            <div className="sliderBar_l2">
              <i className="sliderBar_l21 sliderBar_l214" />
              <span className=" sliderBar_l22">我的足迹</span>
            </div>
            <div className="sliderBar_l2">
              <i className="sliderBar_l21 sliderBar_l215" />
              <span className=" sliderBar_l22">我的消息</span>
            </div>
            <div className="sliderBar_l2">
              <i className="sliderBar_l21 sliderBar_l216" />
              <span className=" sliderBar_l22">咨询JIMI</span>
            </div>
            <div className="sliderBar_l2">
              <i className="sliderBar_l21 sliderBar_l217" />
              <span className=" sliderBar_l22">顶部</span>
            </div>
            <div className="sliderBar_l2">
              <i className="sliderBar_l21 sliderBar_l218" />
              <span className=" sliderBar_l22">反馈</span>
            </div>
          </div>
        </div>
        {/* <!-- footer --> */}
        <Footer />
      </div>
    );
  }
}

User.propTypes = {};

export default User;
