import React, { PureComponent } from 'react';
import LinkA from '../LinkA';

class Pager extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    return (
      <div className="page_box">
        <div className="page_box1">
          <span style={{ width: '43px' }}>
            <font>
              <LinkA>«首页</LinkA>
            </font>
          </span>
          <span style={{ width: '48px' }}>
            <font>
              <LinkA>上一页</LinkA>
            </font>
          </span>
          <span style={{ width: '27px', color: '#fff', background: 'red' }} className="page_on">
            <LinkA>1</LinkA>
          </span>
          <span style={{ width: '27px' }}>
            <LinkA href="list2.html">2</LinkA>
          </span>
          <span style={{ width: '54px' }}>
            <LinkA href="list2.html">下一页</LinkA>
          </span>
          <span style={{ width: '49px' }}>
            <LinkA href="list2.html">尾页»</LinkA>
          </span>
        </div>
      </div>
    );
  }
}

Pager.propTypes = {};

export default Pager;
