/**
 *
 */
import React from 'react';
import { InputNumber } from 'antd';
import WrapInput from './WrapInput';
import ViewRender from './ViewRender';
import { getIntl } from '@/utils/utils';
// 没有值定义其他的  要把 不属于Input的属性 给过滤出去

const WrapProps = (Ele) => {
  return class InputFunc extends React.Component {
    valueFormatter = (initialValue) => {
      return initialValue;
    };

    render() {
      const { initialValue, readOnly, type, onBlur, isRead, ...otherProps } = this.props;
      const moneyObj = {};

      if (type == 'money') {
        const rex = /\d{1,3}(?=(\d{3})+$)/g;
        moneyObj.formatter = value => `${value}`.replace(/^(-?)(\d+)((\.\d+)?)$/, (s, s1, s2, s3) => {
          return s1 + s2.replace(rex, '$&,') + s3;
        });
        // moneyObj.formatter = value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        // moneyObj.parser = value => value.replace(/\$\s?|(,*)/g, '');
      }
      if (readOnly) return <ViewRender>{this.valueFormatter(initialValue || otherProps.value)}</ViewRender>;
      if (isRead) {
        return <Ele {...otherProps} {...moneyObj} onBlur={onBlur} />;
      }
      return (
        <Ele {...otherProps} {...moneyObj} onBlur={onBlur} placeholder={getIntl('onlynumbers')} />
      );
    }
  };
};

const InputNumberH = WrapInput(WrapProps(InputNumber), 'InputNumber');

export default InputNumberH;
