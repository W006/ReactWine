/**
 *
 */
import React from 'react';
import { Input } from 'antd';
import WrapInput from './WrapInput';
import ViewRender from './ViewRender';

const { TextArea } = Input;

// 没有值定义其他的  要把 不属于Input的属性 给过滤出去

const WrapProps = (Ele) => {
  return class InputFunc extends React.Component {
    // valueFormatter = (initialValue) => {
    //   return initialValue;
    // };

    render() {
      const { initialValue, view, ...otherProps } = this.props;
      if (view) {
        return (
          <ViewRender>
            {/* {this.valueFormatter(initialValue || otherProps.value)} */}
            {initialValue}
          </ViewRender>
        );
      }
      return <Ele {...otherProps} />;
    }
  };
};

const NInput = WrapInput(WrapProps(Input), 'Input');
const NTextArea = WrapInput(WrapProps(TextArea), 'TextArea');
const NPassword = WrapInput(WrapProps(Input.Password), 'Password');
NInput.NTextArea = NTextArea;
NInput.NPassword = NPassword;
export default NInput;
