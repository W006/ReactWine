import React, { Fragment } from 'react';
import { Form } from 'antd';

/**
 * 布局item
 */
export default ({ children, labelOptions = {}, label, extra }) => {
  return extra ? (
    <Fragment>
      <div className="ant-input-wrapper ant-input-group">{children}</div>
      <span className="ant-input-group-addon">{extra}</span>
    </Fragment>
  ) : (
    <Form.Item label={label} {...labelOptions}>
      {children}{' '}
    </Form.Item>
  );
};
