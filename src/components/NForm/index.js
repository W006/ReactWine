/* eslint-disable react/destructuring-assignment */
import React, { Fragment } from 'react';
import PropType from 'prop-types';
import { Skeleton, Checkbox, Form } from 'antd';

import NInput from './Item/NInput';
import WrapInput from './Item/WrapInput';
// import CheckboxH from './Item/CheckboxH';
// import SelectH from './Item/SelectH';
// import RadioH from './Item/RadioH';
// import DatePickerH from './Item/DatePickerH';
// import RangePickerH from './Item/RangePickerH';
// import InputNumberH from './Item/InputNumberH';
// import MonthPickerH from './Item/MonthPicker';

class NForm extends React.PureComponent {
  state = {
    loading: true,
  };

  // 声明Context对象属性
  static childContextTypes = {
    form: PropType.object,
    params: PropType.object,
  };

  // 返回Context对象，方法名是约定好的
  getChildContext() {
    return {
      form: this.props.form,
    };
  }

  componentDidMount() {
    this.setState({ loading: false });
  }

  render() {
    const { children, form, ...other } = this.props;
    const { loading } = this.state;
    return (
      <Form {...other}>
        <Skeleton loading={loading} active avatar>
          {children}
        </Skeleton>
      </Form>
    );
  }
}

NForm.childContextTypes = {
  form: PropType.object,
};

const NCheckbox = WrapInput(Checkbox, 'Checkbox');

export default NForm;
export { NInput, NCheckbox };
