/* eslint-disable no-script-url */
import React from 'react';

export default ({ children, ...other }) => (
  <a href="" {...other}>
    {children}
  </a>
);
