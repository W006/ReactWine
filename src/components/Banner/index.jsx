/* eslint-disable react/react-in-jsx-scope */
/* eslint-disable react/jsx-filename-extension */
/**
 * 轮播
 */
import { Component } from 'react';
import classNames from 'classnames';
import { random } from '../utils/util.num';

const Item = ({ title, children, opacity, className, ...other }) => (
  <li className={classNames('slide banner-item', className)} style={{ zIndex: 3, opacity }}>
    <a title={title} {...other}>
      {children}
    </a>
  </li>
);

const CryLink = ({ index, className, ...other }) => <li className={`page ${className}`} {...other}>{index}</li>;

class Banner extends Component {

	timer;

	timeUnit = 1000; // 1k毫秒

	constructor(p) {
	  super(p);
	  this.childLength = p.children.length;
	  this.state = {
	    active: 1,
	  };
	}

	// 自动播放
	autoPlay = () => {
	  if (this.childLength > 1) {
	    this.timer = setTimeout((i) => {
	      this.onSelect((this.state.active + 1));
	      this.autoPlay();
	    }, this.getRadomTime() * this.timeUnit);
	  }
	}

	getRadomTime = () => {
	  return random(3, 9);
	};

	componentWillUnMount() {
	  if (this.timer) {
	    clearTimeout(this.timer);
	  }
	}

	onSelect = (_) => {
	  if (_ < 0) {
	    return this.setState({ active: this.childLength - 1 });
	  }
	  if (_ >= this.childLength) {
	    return this.setState({ active: 0 });
	  }
	  this.setState({ active: _ });
	}

	componentDidMount() {
	  this.autoPlay();
	}

	render() {
	  const { props } = this;
	  const { state } = this;
	  return (
  <div className="banner">
    <div className="fader">
      {props.children.map((child, index) => {
			  const childProps = { ...child.props };
			  return <Item {...childProps} opacity={(state.active === index) ? 1 : 0} />;
      })}
	  <div 
		  className="focusBox_titCell"
		  style={{ zIndex: ' 101', ' marginLeft': '-119px' }}
		>
        {/* <div className="page prev" onClick={_ => this.onSelect(state.active - 1)} data-target="prev">&#8249;</div>
        <div className="page next" onClick={_ => this.onSelect(state.active + 1)} data-target="next">&#8250;</div> */}
        <ul className="pager_list">
          {props.children.map((child, index) => (
            <CryLink
              onClick={_ => this.onSelect(index)}
              className={(state.active === index ? 'active' : '')}
            >
              {index}
            </CryLink>
          ))}
        </ul>
      </div>
    </div>
  </div>
	  );
	}
}

Banner.Item = Item;

export default Banner;
