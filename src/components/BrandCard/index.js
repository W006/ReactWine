/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
import React, { PureComponent } from 'react';
import LinkA from '../LinkA';
import Image from '../Image';

const modal = {
  id: 1,
  detail: '西班牙 菲兰特利 骑士干红葡萄酒750ml', // 详情
  imgAddr: 'images/01.jpg', // 图片
  price: 136, // 价格
  focusCount: 5014, // 关注度
  evaluateCount: 22, // 评价度
  specialTip: '限时抢购 欲购从速',
};

class BrandCard extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    return (
      <div className="min_in">
        <div className="productImg-wrap">
          <LinkA>
            <Image src={modal.imgAddr} alt={modal.detail} height="230" width="230" />
          </LinkA>
        </div>
        <p className="productPrice">
          <em id="G_2" title="136">
            <strong>￥{modal.price}</strong>
          </em>
          <span className="guanzhu fr">
            关注度<b>{modal.focusCount}</b>
          </span>
        </p>
        <p className="productTitle">
          <LinkA>{modal.detail}</LinkA>
          <font color="#ffaa71">{modal.specialTip}</font>
          <label id="A_2" title="" />
        </p>
        <table className="productBtn" cellPadding="0" cellSpacing="0">
          <tbody>
            <tr>
              <td className="pingjiaBox">
                <LinkA>{modal.evaluateCount}</LinkA>
              </td>
              <td width="26%">
                <div className="favBox">
                  <LinkA onClick={() => {}} rel="nofollow">
                    收藏
                  </LinkA>
                </div>
              </td>
              <td className="shopCarBox" width="48%">
                <LinkA rel="nofollow" onClick={() => {}}>
                  加入购物车
                </LinkA>
              </td>
            </tr>
          </tbody>
          <tbody />
        </table>
      </div>
    );
  }
}

BrandCard.propTypes = {};

export default BrandCard;
