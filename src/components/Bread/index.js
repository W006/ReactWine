import React, { PureComponent } from 'react';
import LinkA from '../LinkA';

class Bread extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    return (
      <div className="list_main_top">
        <LinkA href="../index.html">首页</LinkA>
        <span>&nbsp;>&nbsp;</span>
        <LinkA>葡萄酒</LinkA>
        <span>&nbsp;>&nbsp;</span>
        <LinkA>您已经选择</LinkA>
      </div>
    );
  }
}

export default Bread;
