/**
 * util.num.js 数组工具类
 * @author wennn
 * @time 2019-12-16
 */

/**
 * 随机生产 min - max 之前整数数
 * @param {最大随机数} max
 * @param {最小随机数} min
 */
export function random(max, min) {
  return Math.floor(Math.random() * (max - min) + min);
}
