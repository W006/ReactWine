/* eslint-disable no-restricted-syntax */
/* eslint-disable no-unused-vars */
/**
 *  这里配置所有服务restapi 模版配置
 *  如果与重复的key 则会覆盖（请确保配置都key都重复）
 *  "KEY请大写唯一(服务模块_具体表_功能)":{
 *      "url":"/system/user/listByDto", //后台对应restapi url
 *      "method":"POST",                //请求方式 formpost，delete,post,get
 *      "header":['Accept': 'application/json'], // 可以设置请求的头
 *      "auth":"wennn",                 // 作者
 *      “desc":"用户列表查询"             // 方法描述
 *  }
 *
 * @time 20180731
 * @auth wennn
 */

const sys = require('./sys');

// 合并服务配置 ， ⚠️这里后面会覆盖前面的配置
const api = { ...sys };

const env = process.env.NODE_ENV === 'development';

// 检查是否有重复的url
function checkSameUrl(configs) {
  const tApi = {};
  Object.keys(configs).forEach((k) => {
    const { url, auth, desc, mocktable, mockhandler } = configs[k];
    if (mocktable) {
      // 针对mock 生成的
      Object.assign(configs[k], {
        mocktable: mocktable.toUpperCase(),
        mockhandler,
      });
    }
    if (url && url != '') {
      if (tApi[url]) {
        tApi[url].count += 1;
        tApi[url].apis.push(`${auth} => ${k}`);
      } else {
        tApi[url] = {
          count: 1,
          apis: [`${auth} => ${k}`],
        };
      }
    }
  });
  // logger.log('疑似重复 Url====Start===============================');
  // for (const k in tApi) {
  //   if (tApi[k].count > 1) logger.log('重复次数：', tApi[k].count, ` 》【${k}】${tApi[k].apis}`);
  // }
  // logger.log('疑似重复 Url====End=================================');
}

if (env) {
  checkSameUrl(api);
}

// export default api;
module.exports = {
  api,
};
