import ReactDOM from 'react-dom';
import React from 'react';
// import './index.scss';

import App from './pages/index';
// const App = () =><div>hello</div>

ReactDOM.render(<App />, document.getElementById('root'));

// 还需要在主要的js文件里写入下面这段代码
if (module.hot) {
  // 实现热更新
  module.hot.accept();
}
