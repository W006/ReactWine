const path = require('path');
const webpack = require('webpack');
const argv = require('yargs-parser')(process.argv.slice(2));
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const pro = argv.mode == 'production'; //  区别是生产环境和开发环境
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const apiMocker = require('webpack-api-mocker');

const plu = [];
if (pro) {
  //  线上环境
  plu.push(
    new HtmlWebpackPlugin({
      template: './src/index.html',
      hash: true, // 会在打包好的bundle.js后面加上hash串
      chunks: ['vendor', 'index', 'utils'], //  引入需要的chunk
    }),
    // 拆分后会把css文件放到dist目录下的css/style.css
    new ExtractTextWebpackPlugin('css/style.[chunkhash].css'),
    new ExtractTextWebpackPlugin('css/reset.[chunkhash].css'),
    new ExtractTextWebpackPlugin('css/index.[chunkhash].css'),
    new CleanWebpackPlugin('dist'),
  );
} else {
  //  开发环境
  plu.push(
    new HtmlWebpackPlugin({
      template: './src/index.html',
      chunks: ['vendor', 'index', 'utils'], //  引入需要的chunk
    }),
    // 拆分后会把css文件放到dist目录下的css/style.css
    new ExtractTextWebpackPlugin('css/style.css'),
    new ExtractTextWebpackPlugin('css/reset.css'),
    new ExtractTextWebpackPlugin('css/index.css'),
    new webpack.HotModuleReplacementPlugin(), // 热更新，热更新不是刷新
  );
}


module.exports = {
  entry: {
    index: './src/index.js',
  }, // 入口文件
  output: {
    filename: pro ? '[name].[chunkhash].js' : '[name].js', // 打包后的文件名称
    path: path.resolve('dist'), // 打包后的目录，必须是绝对路径
    publicPath: '/',
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: {
          loader: 'babel-loader',
          options: {
            plugins: [
              ['import', { libraryName: 'antd', libraryDirectory: 'es', style: 'css' }],
            ],
          },
        },
        include: /src/, // 只转化src目录下的js
        exclude: /node_modules/, // 排除掉node_modules，优化打包速度
      },
      {
        test: /\.(jpe?g|png|gif|jpg)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192, // 小于8k的图片自动转成base64格式，并且不会存在实体图片
              outputPath: 'images/', // 图片打包后存放的目录
            },
          },
        ],
      },
      {
        test: /\.css$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
        ],
      },
      {
        test: /\.less$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader', options: { modules: true } },
          { loader: 'less-loader' },
        ],
      },
      {
        test: /\.scss$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          { loader: 'sass-loader' },
        ],
      },
      {
        test: /\.(htm|html)$/,
        use: 'html-withimg-loader', // 打包页面img引用图片
      },
      {
        test: /\.(eot|ttf|woff|svg|jpg|png)$/, //  打包字体图片和svg图片
        use: 'file-loader',
      },
      {
        test: /\.(png|jpg|gif)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 1,
              name: 'images/[name].[ext]',
            },
          },
          { loader: 'image-webpack-loader' },
        ],
      },
    ],
  },
  plugins: plu,
  devServer: {
    host: '127.0.0.1',
    port: 6061, // 端口
    open: false, // 自动打开浏览器
    hot: true, // 开启热更新
    overlay: true, // 浏览器页面上显示错误
    historyApiFallback: true,
    /** **************此处星星多，注意看此处*************************** */
    // 利用webpack-dev-server 的before 方法调用webpack-api-mocker
    // path.resolve('./mocker/index.js') 中的'./mocker/index.js'为mock文件的相对路径
    /** *************以上只是个人的浅显理解罢了（有不同理解请交流）***************** */
    before(app) {
      apiMocker(app, path.resolve('./mock/index.js'), {
        proxy: {
          '/abc/*': 'https://www.baidu.com',
        },
        changeHost: true,
      });
    },
  },
  resolve: {
    // 别名
    alias: {
      '@': path.join(__dirname, '/src'),
      pages: path.join(__dirname, '/src/pages'),
      components: path.join(__dirname, './src/components'),
      images: path.join(__dirname, '/src/images'),
    },
    // 省略后缀
    extensions: ['.js', '.jsx', '.json', '.css', '.scss', '.less'],
  },
  //  提取公共代码
  optimization: {
    splitChunks: {
      cacheGroups: {
        vendor: { // 抽离第三方插件
          test: /node_modules/, // 指定是node_modules下的第三方包
          chunks: 'initial',
          name: 'vendor', // 打包后的文件名，任意命名
          // 设置优先级，防止和自定义的公共代码提取时被覆盖，不进行打包
          priority: 10,
        },
        utils: {
          // 抽离自己写的公共代码，utils这个名字可以随意起
          chunks: 'initial',
          name: 'utils', //  任意命名
          minSize: 0, // 只要超出0字节就生成一个新包
        },
      },
    },
  },
  devtool: pro ? '' : 'inline-source-map',
};
